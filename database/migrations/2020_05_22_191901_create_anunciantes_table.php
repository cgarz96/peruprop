<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnunciantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anunciantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->string('razon_social');
            $table->string('nombre_anunciante')->nullable();
            $table->string('apellido_anunciante')->nullable(); 
            $table->string('sexo_anunciante')->nullable();
            $table->string('correo_anunciante')->nullable();
            $table->string('correo_anunciante_fact')->nullable();
            $table->string('telefono_anunciante_fact')->nullable();
            $table->string('telefono_anunciante')->nullable();
            $table->string('domicilio_anunciante')->nullable();
            $table->string('movil_anunciante')->nullable();
            $table->string('movil_anunciante_fact')->nullable();
            $table->string('nacionalidad_anunciante')->nullable();
            $table->string('pais_anunciante')->nullable();
            $table->string('departamento_anunciante')->nullable();
            $table->string('direccion_anunciante')->nullable();
            $table->string('provincia_anunciante')->nullable();
            $table->string('districto_anunciante')->nullable();
            $table->string('calle_anunciante')->nullable();
            $table->string('codigo_postal_anunciante')->nullable();
            $table->date('fecha_nacimiento_anunciante')->nullable();
            $table->string('tipo_documento_anunciante')->nullable();
            $table->string('documento_anunciante')->nullable();
            $table->string('logo_anunciante')->nullable();
            $table->integer('cant_publicaciones')->nullable();
            $table->integer('role_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anunciantes');
    }
}
