<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeruDistrictosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peru_districtos', function (Blueprint $table) {
            $table->id();
            $table->integer('peru_departamento_id');
            $table->string('peru_provincia_id');
            $table->string('nombre_dist');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peru_districtos');
    }
}
