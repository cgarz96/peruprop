<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropiedadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propiedads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pago_id')->nullable();
            $table->string("tipo_propiedad")->nullable();
            $table->string("codigo_publicacion")->unique()->nullable();
            $table->string("tipo_propietario")->nullable();
            $table->integer("user_id")->unsigned();
            $table->string('titulo');
            $table->string('tipo_operacion');
            $table->longText('descripcion');
            $table->float('precio',25, 2);
            $table->string('latitud');
            $table->string('longitud');
            $table->string('departamento');
            $table->string('provincia')->nullable();
            $table->string('districto')->nullable();
            $table->string('ubicacion_completa')->nullable();
            $table->string('direccion');
            $table->integer('paquete_id')->nullable();
            $table->string('visibilidad')->nullable();
            $table->string('interes')->nullable();
            $table->date('fecha_alta')->nullable();
            $table->date('fecha_baja')->nullable();
            $table->integer('prioridad_visibilidad')->nullable();
            $table->string('estado_publicacion')->nullable();
            $table->string('correo_publicacion')->nullable();
            $table->string('numero_contacto_publicacion')->nullable();
            $table->timestamps();


            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propiedads');
    }
}
