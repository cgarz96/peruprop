<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agentes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->string('razon_social');
            $table->string('correo_agente')->nullable();
            $table->string('telefono_agente')->nullable();
            $table->string('domicilio_agente')->nullable();
            $table->string('movil_agente')->nullable();
            $table->string('departamento_agente')->nullable();
            $table->string('provincia_agente')->nullable();
            $table->string('districto_agente')->nullable();
            $table->string('dni')->nullable();
            $table->string('logo')->nullable();
            $table->integer('cant_publicaciones')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agentes');
    }
}
