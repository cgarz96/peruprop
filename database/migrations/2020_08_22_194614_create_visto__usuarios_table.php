<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVistoUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visto__usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsiged();
            $table->integer('propiedad_id')->unsiged();
            $table->string('codigo_publicacion_propiedad')->nullable();
            $table->date('fecha_visto')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visto__usuarios');
    }
}
