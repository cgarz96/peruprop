<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactadoUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactado__usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsiged(); 
            $table->integer('propiedad_id')->unsiged()->nullable();
            $table->string('codigo_publicacion_propiedad')->nullable();
            $table->integer('user_contactado_id')->unsiged()->nullable();
            $table->longText('mensaje')->nullable();
            $table->date('fecha_contactado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactado__usuarios');
    }
}
