<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaqueteUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paquete__users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('paquete_id')->unsigned();
            $table->integer('pago_id')->unsigned()->nullable();
            $table->integer('publicaciones_restantes')->nullable();
            $table->date('fecha_compra_paquete')->nullable();
            $table->date('fecha_alta_paquete')->nullable();
            $table->date('fecha_expiracion_paquete')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paquete__users');
    }
}
