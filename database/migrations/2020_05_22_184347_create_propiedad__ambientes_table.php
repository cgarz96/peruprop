<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropiedadAmbientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propiedad__ambientes', function (Blueprint $table) {
            $table->id();
            $table->integer('propiedad_id')->unsigned();
            $table->integer('ambiente_id');
            $table->timestamps();

            $table->foreign('propiedad_id')->references('id')->on('propiedads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propiedad__ambientes');
    }
}
