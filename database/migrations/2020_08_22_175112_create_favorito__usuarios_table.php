<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFavoritoUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorito__usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsiged();
            $table->integer('propiedad_id')->unsiged();
            $table->date('fecha_favorito')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorito__usuarios');
    }
}
