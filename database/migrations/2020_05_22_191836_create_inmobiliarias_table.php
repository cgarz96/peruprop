<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInmobiliariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inmobiliarias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->string('razon_social');
            $table->string('correo_inmobiliaria')->nullable();
            $table->string('telefono_inmobiliaria')->nullable();
            $table->string('domicilio_inmobiliaria')->nullable();
            $table->string('movil_inmobiliaria')->nullable();
            $table->string('departamento_inmobiliaria')->nullable();
            $table->string('provincia_inmobiliaria')->nullable();
            $table->string('districto_inmobiliaria')->nullable();
            $table->string('dni')->nullable();
            $table->string('logo')->nullable();
            $table->integer('cant_publicaciones')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inmobiliarias');
    }
}
