<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaquetesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paquetes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_paquete')->nullable();
            $table->string('tipo_paquete')->nullable();
            $table->string('tipo_publicante')->nullable();
            $table->float('precio')->nullable();
            $table->float('precio_igv')->nullable();
            $table->string('tipo_periodo')->nullable();
            $table->string('visibilidad')->nullable();
            $table->string('interes')->nullable();
            $table->integer('nro_avisos')->nullable();
            $table->integer('anuncio')->nullable();
            $table->integer('prioridad')->nullable();
            $table->string('slug')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paquetes');
    }
}
