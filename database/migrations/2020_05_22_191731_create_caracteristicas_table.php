<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaracteristicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caracteristicas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('propiedad_id')->unsigned();
            //Casa
            $table->float('superficie')->nullable();
            $table->integer('antiguedad')->nullable();
            $table->integer('cant_baños')->nullable();
            $table->integer('cant_dormitorios')->nullable();
            $table->string('tipo_techo')->nullable();
            $table->integer('cant_chocheras')->nullable();
            //Departamento-Hotel
            $table->string('tipo_balcon')->nullable();
            $table->float('expensas')->nullable();
            $table->string('estado')->nullable();
            //Terreno
            $table->string('tipo_de_pendiente')->nullable();
            $table->float('long_frente')->nullable();
            $table->float('long_fondo')->nullable();
            $table->float('sup_construible')->nullable();
            $table->string('zonificacion')->nullable();
            //Local
            $table->string('ultima_actividad')->nullable();
            $table->string('tipo_local')->nullable();
            $table->float('sup_cubierta')->nullable(); 
            $table->integer('cant_plantas')->nullable();
            //Cochera
            $table->string('tipo_cochera')->nullable();
            $table->string('tipo_coche')->nullable();
            $table->string('tipo_de_acceso')->nullable();
            //Oficina
            $table->float('sup_planta')->nullable();
            $table->float('sup_total')->nullable();
            $table->string('ambientes')->nullable();
            //Ph
            $table->float('sup_descubierta')->nullable();
            $table->string('disposicion')->nullable();
            //Quinta
            $table->string('tipo_piso')->nullable();
            
            $table->string('foto_principal')->nullable();

            $table->timestamps();

            $table->foreign('propiedad_id')->references('id')->on('propiedads')->onDelete('cascade');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caracteristicas');
    }
}
