<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('role_id')->nullable();
            $table->string('tipo_propietario')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('document')->nullable();
            $table->string('nacionalidad_user')->nullable();
            $table->string('pais_user')->nullable();
            $table->string('provincia_user')->nullable();
            $table->string('districto_user')->nullable();
            $table->string('departamento_user')->nullable();
            $table->string('telephone')->nullable();
            $table->string('cellphone')->nullable();
            $table->string('avatar')->nullable();
            $table->string('sexo_user')->nullable();
            $table->string('address_user')->nullable();
            $table->date('birth_date_user')->nullable();
            $table->datetime('last_connection')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
