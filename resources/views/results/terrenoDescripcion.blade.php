
<div class=" col-md-12">
<div class="card text-center elevation-3">
  <div class="card-header">
    <blockquote class="blockquote mb-0">
    Características principales
    </blockquote>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-md-3">
      <i class="fas fa-bed"></i>  <b>Longitud Frente: </b>{{$descripcionPropiedad[0]->long_frente}}
      </div>
       <div class="col-md-3">
        <i class="fas fa-bath"></i>  <b>Longitud Fondo: </b>{{$descripcionPropiedad[0]->long_fondo}}
      </div>
       <div class="col-md-3">
        <i class="fas fa-warehouse"></i> <b>Superficie Construible: </b>{{$descripcionPropiedad[0]->sup_construible}}
      </div>
       <div class="col-md-3">
        <i class="fas fa-calendar-times"></i> <b>Zonificación: </b>{{$descripcionPropiedad[0]->zonificacion}}
      </div>
    </div>
  </div>
</div>
</div>
