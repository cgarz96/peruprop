@extends('layouts.app')

@section('content')

<style type="text/css">
	
	 .caption {
   position: absolute;
   top: 45%;
   left: 0;
   width: 100%;
   background: green;
   
   opacity: 0.8;
  }

  .trunc{
text-overflow: ellipsis;
overflow: hidden;
white-space: nowrap;

  }

.pagination > li > a,
.pagination > li > span {
    color: black ; // use your own color here
}

.pagination > .active > a,
.pagination > .active > a:focus,
.pagination > .active > a:hover,
.pagination > .active > span,
.pagination > .active > span:focus,
.pagination > .active > span:hover {
    background-color: #ff9800 !important;
    border-color: #ff9800 !important; 
}

	/*background:#ff9800 !important;*/

</style>

<div>
<div class="row ml-2 mr-2 mb-2">
@foreach($resultados as $propiedad)
    

    
<div class="card ml-2 mt-2" style="width: 23em; "> 
  <img src="{{  asset($propiedad->foto_principal) }}" class="card-img-top" alt="..." style="height: 15em;">
   <h3 class="caption "><b class="text-white" style="color: white !important;">$ {{$propiedad->precio}}</b></h3>
  <div class="card-body">
    <h5 class="card-title">{{$propiedad->titulo}}</h5>
    <p class="card-text trunc" >{{$propiedad->descripcion}}</p>
@php

if($propiedad->tipo_propiedad =='depto-tipo-casa'){
  $propiedad->tipo_propiedad='ph';
}

elseif($propiedad->tipo_propiedad =='fondo-comercio'){
  $propiedad->tipo_propiedad='fondo';
}

elseif($propiedad->tipo_propiedad =='negocio-especial'){
 $propiedad->tipo_propiedad='negocio';
}


@endphp
  <a href="{{ url('detalles-'.$propiedad->tipo_propiedad.'-'.$propiedad->codigo_publicacion) }}" class="btn btn-warning">Ver mas detalles</a>
  </div>
</div>



@endforeach 
</div>
<div class="row">
	<div style="position: absolute;bottom: 0px; margin-left: 48%;">
{{ $resultados->links() }}</div>
</div>


</div>










{{--<div class="row ml-2 mr-2">
@for ($i = 0; $i < 6; $i++)



<div class="card ml-2 mt-2" style="width: 18rem;">
  <img src="{{  asset('img/casa.jpg') }}" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Card title {{ $i }}</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="#" class="btn btn-primary">Go somewhere</a>
  </div>
</div>


@endfor

</div>--}}


@endsection