@extends('layouts.app')

@section('content')

<div class="container">
@foreach($inmobiliarias as $inmobiliaria)

    



<div class="card mt-3 mb-3 ">
  <h5 class="card-header">{{$inmobiliaria->razon_social}}</h5>
  <div class="card-body">

  	<div class="row">
   <div class="col-md-10"> <h5 class="card-title">{{$inmobiliaria->razon_social}}</h5></div>
  <div class="col-md-2"><img src="{{ asset('storage').'/'.$inmobiliaria->logo}}" width="100" height="50"></div>
</div>
    <div class="row ">
    <a href="{{ url('inmobiliaria-'.$inmobiliaria->user_id)}}" class="btn btn-warning  ml-auto mr-auto ">Ver avisos ({{$inmobiliaria->cant_publicaciones}})</a>

</div>
  </div>
</div>



@endforeach 


{{ $inmobiliarias->links() }}
@endsection