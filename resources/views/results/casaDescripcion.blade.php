  <div class=" col-md-12 ">
    <div class="card text-center elevation-3">
      <div class="card-header">
        <blockquote class="blockquote mb-0">
        Características principales
        </blockquote>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-3">
          <i class="fas fa-bed"></i>  <b>Cantidad de dormitorios: </b>{{$descripcionPropiedad[0]->cant_dormitorios}}
          </div>
           <div class="col-md-3">
            <i class="fas fa-bath"></i>  <b>Cantidad de baños: </b>{{$descripcionPropiedad[0]->cant_baños}}
          </div>
           <div class="col-md-3">
            <i class="fas fa-warehouse"></i> <b>Cantidad de cocheras: </b>{{$descripcionPropiedad[0]->cant_chocheras}}
          </div>
           <div class="col-md-3">
            <i class="fas fa-calendar-times"></i> <b>Antiguedad: </b>{{$descripcionPropiedad[0]->antiguedad}} años
          </div>
        </div>
      </div>
    </div>
</div>

