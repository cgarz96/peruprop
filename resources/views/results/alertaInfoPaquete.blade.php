@extends('layouts.app')


@section('content')
<div class="container">
<div class="jumbotron mt-5 ">
  <h1 class="display-3">Ops! :(</h1>
  <p class="lead">No puedes comprar un paquete por que ya estas registrado como {{$tipo}}...</p>
  <hr class="my-4">
  <div class="lead d-flex justify-content-end">
    <a class="btn bg-gradient-success btn-lg" href="{{ url('/publicaciones') }}" role="button">Volver</a>
  </div>
</div>
</div>
 
@endsection 