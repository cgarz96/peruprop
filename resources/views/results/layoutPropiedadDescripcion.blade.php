@extends('layouts.app')

@section('content')

@guest
@else
{{ agregarAVistos($descripcionPropiedad[0]->propiedad_id) }}
@endguest
<div class="container">
	@include('partials.propiedad.titulo')

	<div class="row">
		<div class="col-md-9 mb-3">@include('partials.propiedad.galeria')</div>
		<div class="col-md-3">@include('partials.propiedad.card-contactar')</div>
	</div>
	<div class="row">
			
	@switch($tipo_propiedad)
	    @case('casa')
	        @include('results.casaDescripcion')
	        @php $servicio = 'Servicios de la Casa';$ambientes = 'Ambientes de la Casa';$instalaciones = 'Instalaciones de la Casa' @endphp
	        @break
	    @case('departamento')
	        @include('results.departamentoDescripcion')
	        @php $servicio = 'Servicios del Departamento';$ambientes = 'Ambientes del Departamento';$instalaciones = 'Instalaciones del Departamento' @endphp
	        @break
	    @case('campo')
	        @include('results.campoDescripcion')
	        @php $servicio = 'Servicios del Campo';$ambientes = 'Ambientes del Campo';$instalaciones = 'Instalaciones del Campo' @endphp
	        @break
	    @case('cochera')
	        @include('results.cocheraDescripcion')
	        @php $servicio = 'Servicios de la Cochera';$ambientes = 'Ambientes de la Cochera';$instalaciones = 'Instalaciones de la Cochera' @endphp
	        @break
	    @case('fondo')
	        @include('results.fondoComercioDescripcion')
	        @php $servicio = 'Servicios del Fondo de Comercio';$ambientes = 'Ambientes del Fondo de Comercio';$instalaciones = 'Instalaciones del Fondo de comercio' @endphp
	        @break
	    @case('ph')
	        @include('results.phDescripcion')
	        @php $servicio = 'Servicios del Ph';$ambientes = 'Ambientes del Ph';$instalaciones = 'Instalaciones del Ph' @endphp
	        @break
	    @case('negocio')
	        @include('results.negocioEspecialDescripcion')
	        @php $servicio = 'Servicios del Negocio Especial';$ambientes = 'Ambientes del Negocio Especial';$instalaciones = 'Instalaciones del Negocio Especial' @endphp
	        @break
	    @case('local')
	        @include('results.localDescripcion')
	        @php $servicio = 'Servicios del Local';$ambientes = 'Ambientes del Local';$instalaciones = 'Instalaciones del Local' @endphp
	        @break 
	    @case('oficina')
	        @include('results.oficinaDescripcion')
	        @php $servicio = 'Servicios de la Oficina';$ambientes = 'Ambientes de la Oficina';$instalaciones = 'Instalaciones de la Oficina' @endphp
	        @break 
	    @case('terreno')
	        @include('results.terrenoDescripcion')
	        @php $servicio = 'Servicios del Terreno';$ambientes = 'Ambientes del Terreno';$instalaciones = 'Instalaciones del Terreno' @endphp
	        @break
	    @case('quinta')
	        @include('results.quintaDescripcion')
	        @php $servicio = 'Servicios de la Casa';$ambientes = 'Ambientes de la Casa';$instalaciones = 'Instalaciones de la Casa' @endphp
	        @break  
	    @case('hotel')
	        @include('results.hotelDescripcion')
	        @php $servicio = 'Servicios del Hotel';$ambientes = 'Ambientes del Hotel';$instalaciones = 'Instalaciones del Hotel' @endphp
	        @break  
	    @case('galpon')
	        @include('results.galponDescripcion')
	        @php $servicio = 'Servicios del Galpón';$ambientes = 'Ambientes del Galpón';$instalaciones = 'Instalaciones del Galpón' @endphp
	        @break        

	    @default

	@endswitch
	</div>
	<div class="row">
		@include('partials.propiedad.mapa')
	</div>
	<div class="row">
		@include('partials.propiedad.descripcion')
	</div>
	<div class="row">
		@include('partials.propiedad.datos-basicos')
	</div>
	<div class="row d-flex justify-content-between">
		@include('partials.propiedad.servicios')
	</div>
</div>



<script src="{{ asset('js/maps.js')}}"></script>
@endsection