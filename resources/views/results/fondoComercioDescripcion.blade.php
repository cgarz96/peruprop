<div class="row col-md-12 mt-5 mb-3 ml-1">
  <div class=" col-md-12">
<div class="card text-center">
  <div class="card-header">
    <blockquote class="blockquote mb-0">
    Características principales
    </blockquote>
  </div>
  <div class="card-body">
      <div class="row">
        <div class="col-md-3">
        <i class="fas fa-bed"></i>  <b>Cantidad de baños: </b>{{$descripcionPropiedad[0]->cant_baños}}
        </div>
        <div class="col-md-3">
        <i class="fas fa-bed"></i>  <b>Expensas: </b>{{$descripcionPropiedad[0]->expensas}}
        </div>
        <div class="col-md-3">
        <i class="fas fa-bed"></i>  <b>Superficie de planta: </b>{{$descripcionPropiedad[0]->sup_total}}
        </div>
        <div class="col-md-3">
        <i class="fas fa-bed"></i>  <b>Estado: </b>{{$descripcionPropiedad[0]->estado}}
        </div>
      </div>
  </div>
</div>
</div>
</div>
