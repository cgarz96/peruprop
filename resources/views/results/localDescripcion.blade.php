
<div class=" col-md-12">
<div class="card text-center elevation-3">
  <div class="card-header">
    <blockquote class="blockquote mb-0">
    Características principales
    </blockquote>
  </div>
  <div class="card-body">
      <div class="row">
        <div class="col-md-3">
        <i class="fas fa-bed"></i>  <b>Última actividad: </b>{{$descripcionPropiedad[0]->ultima_actividad}}
        </div>
         <div class="col-md-3">
          <i class="fas fa-bath"></i>  <b>Tipo de local: </b>{{$descripcionPropiedad[0]->tipo_local}}
        </div>
         <div class="col-md-3">
          <i class="fas fa-warehouse"></i> <b>Cantidad de plantas: </b>{{$descripcionPropiedad[0]->cant_plantas}}
        </div>
         <div class="col-md-3">
          <i class="fas fa-calendar-times"></i> <b>Antiguedad: </b>{{$descripcionPropiedad[0]->antiguedad}} años
        </div>
      </div>
  </div>
</div>
</div>

