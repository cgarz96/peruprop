

@if(count($publicaciones)==0)
	<div class="container d-flex justify-content-center " >
	<div class="card mt-3 text-center elevation-3" style="width: 18rem;">
    <i class="far fa-file fa-7x text-teal mt-2"></i>
	  <div class="card-body text-center">
      <div class="d-flex justify-content-center ">
	    <h5 class="card-title">Aún no tienes avisos</h5></div>
	    <p class="card-text text-muted">Publica Fácil</p>
	    <a href="{{ url('publicaciones/aviso') }}" class="btn bg-gradient-olive">Publicar aviso</a>
	  </div>
	</div>
	</div>
@else
@if (session('mensaje'))
<div class="alert  alert-dismissible fade show mt-4 p-3 " style="background: #d4edda; color: #155724" role="alert">
  <strong>Exito! </strong> {{ session('mensaje') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif


<div class="container d-flex justify-content-center " style="min-height: 600px">

        <div class="row mt-3">
          <div class="col-12">
            <div class="card elevation-3">
              <div class="card-header">
                <h3 class="card-title">Listado de avisos</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 300px;">
                <table class="table table-head-fixed ">
                  <thead>
                    <tr>
                      <th >#</th>
                      <th>Tipo de propiedad</th>
                      <th>Tipo de operacion</th>
                      <th>Estado de publicación</th>
                      <th></th> 
                    </tr>
                  </thead>
                  <tbody>
                     @foreach($publicaciones as $dato)

                    <tr>
                      <th scope="row">{{$loop->iteration}}</th>
                      <td>{{$dato->tipo_propiedad}}</td> 
                      <td>{{$dato->tipo_operacion}}</td> 
                      <td>{{$dato->estado_publicacion}}</td>
                      <td>
                      @if($dato->estado_publicacion=='Pendiente')
                      @else
                             <a class="btn bg-gradient-success btn-sm" href="{{ url('busquedas-codigo-'.$dato->codigo_publicacion) }}">Ver</a>
                      @endif
                 
                      </td> 

                    </tr>

                   @endforeach 

                  </tbody>
                </table>
              </div>
            <div class="card-footer clearfix">
              <div class="float-left"><a href="{{ url('publicaciones/aviso') }}" class="btn bg-gradient-olive btn-sm">Publicar ahora</a></div>
                <div class="pagination pagination-sm m-0 float-right">
                 {{ $publicaciones->appends(Request::all())->links() }}
              
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->

</div>
@endif