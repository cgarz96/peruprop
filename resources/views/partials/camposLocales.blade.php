 <div >
  
  <div class="form-row">
    <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Última actividad: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="ultima_actividad" required="">
    </div>
     <div class="input-group mb-3 col-md-3">
      <div class="input-group-prepend">
      <label class="input-group-text" for="inputGroupSelect01">Tipo de local: </label>
      </div>
      <select class="custom-select "  required="" name="tipo_local">
           <option value="">--Selecione el tipo de  local--</option>
               <option value="Via pública">Via Pública</option>
                <option value="Galeria">Galeria</option>
                <option value="Otro">Otro </option>            
      </select>
    </div>


 <div class="form-group input-group mb-3 col-md-2">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Estado de local: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="estado" required="">
    </div>

     <div class="form-group input-group mb-3 col-md-2">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Supereficie cubierta: </span>
    </div>
      <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="superficie_cubierta" required="">
    </div>

     <div class="form-group input-group mb-3 col-md-2">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Cantidad de plantas: </span>
    </div>
      <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="cant_plantas" required="">
    </div>





    </div>

</div>