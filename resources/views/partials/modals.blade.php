  @section('modal-user')
<!-- Modal Login--> 
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >

  <div class="modal-dialog modal-dialog-centered modal-ancho" role="document" >
    <div class="modal-content">
      <div class="modal-header ">
       <h4 class="modal-title w-100 text-center animated bounce" id="exampleModalLabel">Iniciar Sesión</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="form-inline justify-content-between mb-2" style="display: none;">
          <button class="btn btn-primary "><i class="fab fa-facebook"></i> Iniciar Sesion </button> <button class="btn btn-danger "><i class="fab fa-google"></i> iniciar Sesion</button>
        </div>

        
    <form method="POST" action="{{ route('login') }}" id="login">
        <div class="form-group">
          <div class="input-group">  
            <input 
             type="email"
             class="form-control  @error('email') is-invalid @enderror" 
             id="emailUser" 
             name="email" value="{{ old('email') }}" 
             required 
             autocomplete="email"
             autofocus aria-describedby="emailHelp"
             placeholder="Ingrese su email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          </div>
          <div id="messageError"></div>
          </div>
          <div class="form-group">
            <div class="input-group">
            <input 
            type="password"
            id="passUser" 
            class="form-control @error('password') is-invalid @enderror"
            name="password"
            required autocomplete="current-password"
            placeholder="Ingrese su contraseña">
                      <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          </div>
          </div>
           <div class="form-inline mb-3">    
            <div class="form-check">
            <input type="checkbox" class="form-check-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label class="form-check-label " for="exampleCheck1">No cerrar sesión</label>
             @if (Route::has('password.request'))
          </div> <a class="ml-auto" href="{{ route('password.request') }}">¿Olvidaste tu contraseña?</a></div>
            @endif
          <button type="submit " class="btn bg-gradient-success btn-lg btn-block mb-3" id="Sesion">Iniciar Sesión</button>
          <div class="text-center"><b>¿Todavía no estás registrado? </b></div>
         <div class="text-center"> <a data-target="#exampleModalRegister"  data-toggle="modal" href="#" data-dismiss="modal">Registate ahora</a></div>
         
      </form>
    
      </div>
    </div>
  </div>
</div>




<!-- Modal registro-->
<div class="modal fade" id="exampleModalRegister" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered modal-ancho" role="document" >
    <div class="modal-content">
      <div class="modal-header ">
       <h4 class="modal-title w-100 text-center animated bounce" id="exampleModalLabel">Registrarse</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="form-inline justify-content-between mb-2" style="display: none;">
          <button class="btn btn-primary"><i class="fab fa-facebook"></i> Iniciar Sesion </button> <button class="btn btn-danger "><i class="fab fa-google"></i> iniciar Sesion</button>
        </div>
        <div class="text-center mb-3"></div>
        
    <form method="POST" action="{{ route('register') }}" id="register">
         @csrf
          <div class="form-group">
          <div class="input-group">
            <input 
             id="registerName" 
             type="text"
             class="form-control @error('name') is-invalid @enderror" 
             name="name"
             value="{{ old('name') }}" 
             required autocomplete="name" autofocus
             placeholder="Ingrese su nombre">
            <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
          </div>
          </div>
    <div class="form-group">
          <div class="input-group">
            <input
             id="registerEmail" 
             type="email"
             class="form-control  @error('email') is-invalid @enderror" 
             name="email" value="{{ old('email') }}" 
             required 
             autocomplete="email"
             autofocus aria-describedby="emailHelp"
             placeholder="Ingrese su email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>        
            </div>
            <div id="messageExistEmail"></div>
          </div>
          <div class="form-group">
          <div class="input-group">
            <input
             id="registerPassword" 
             type="password"
             class="form-control @error('password') is-invalid @enderror"
             name="password" required autocomplete="new-password"
             placeholder="Ingrese su contraseña">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>

          </div>
          <div id="messageLongPass"></div>
          </div>
          <div class="form-group">
            <div class="input-group">
            <input 
            id="registerPasswordConfirm" 
            type="password" 
            class="form-control"
            name="password_confirmation"
            required autocomplete="new-password"
            placeholder="Repita su contraseña">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          </div>
          </div>
          <div id="messageConfirmPass"></div>

          <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="agente" name="agente" value="agente">
            <label class="form-check-label" for="agente">Soy un Agente</label>
          </div>
          <button type="submit " class="btn bg-gradient-success btn-lg btn-block mb-3" id="Registrar">Registrate</button>
          <div class="text-center"><p>Al registrarme acepto los<a href="{{ asset('doc/TÉRMINOS Y CONDICIONES GENERALES DEL PORTAL PERU-PROP.COM.pdf')}}" target="_blank"> Términos y Condiciones de uso</a> </p></div>
         
      </form>
    
      </div>
    </div>
  </div>
</div>
@show
