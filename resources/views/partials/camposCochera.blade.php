<div >
  
  <div class="form-row">
    <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Tipo de cochera: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="tipo_cochera" required="">
    </div>
    <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Tipo de coche: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="tipo_coche" required="">
    </div>

 <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Tipo de acceso: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="tipo_de_acceso" required="">
    </div>


   <div class="input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Superficie cubierta: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="sup_cubierta" required=""   pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$" title="Información: No se admite mas de un punto, ni signos">
    
    </div>



 
    </div>



</div>