<div >
  
  <div class="form-row">
    <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Cantidad de baños</span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="cant_baños" required="" pattern="[0-9]" title="Información: Debe ser un numero entero.">
    </div>
    <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Cantidad de dormitorios</span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="cant_dormitorios" required="" pattern="[0-9]" title="Información: Debe ser un numero entero.">
    </div>

 <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Cantidad de cocheras</span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="cant_chocheras" required="" pattern="[0-9]" title="Información: Debe ser un numero entero.">
    </div>

    </div>

</div>