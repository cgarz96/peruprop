<div >
  
  <div class="form-row">
    <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Cantidad de baños: </span>
    </div>
      <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="cant_baños" required="">
    </div>
    <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Supercicie cubierta: </span>
    </div>
      <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="sup_cubierta" required="">
    </div>

 <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Supercicie descubierta: </span>
    </div>
      <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="sup_descubierta" required="">
    </div>

     <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Expensas: </span>
    </div>
      <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="expensas" required="">
    </div>
</div>

<div class="row">
     <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Disposición: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="disposicion" required="">
    </div>

     <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Cantidad de plantas: </span>
    </div>
      <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="cant_plantas" required="">
    </div>
     <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Estado: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="estado" required="">
    </div>

       <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Cantidad de dormitorios: </span>
    </div>
      <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="cant_dormitorios" required="">
    </div>
</div>
    

</div>