<div class="row">
   <div class=" mb-3 col-md-4">
   
    <div class="alert alert-primary">Seleccione los servicios que desea agregar</div>
     @foreach ($ServiciosRegistrados  as $i)
      <div class="form-check">
        <input class="form-check-input" type="checkbox" value="{{$i->id}}" onchange="agregarServicio(this.value);">
        <label class="form-check-label" for="defaultCheck1">
          {{$i->nombre}} 
        </label>
      </div>
     @endforeach 

    </div>


  <div class="mb-3 col-md-4 ">
    
 <div class="alert alert-primary">Seleccione las instalaciones que desea agregar</div>
 @foreach ($InstalacionesRegistradas  as $i)
  <div class="form-check">
    <input class="form-check-input" type="checkbox" value="{{$i->id}}" onchange="agregarInstalacion(this.value);">
    <label class="form-check-label" for="defaultCheck1">
      {{$i->nombre}} 
    </label>
  </div>
 @endforeach 

    </div>



<div class=" mb-3 col-md-4 " >
   
 <div class="alert alert-primary">Seleccione los ambientes que desea agregar</div>
 @foreach ($AmbientesRegistrados  as $i)
  <div class="form-check">
    <input class="form-check-input" type="checkbox" value="{{$i->id}}"  onchange="agregarAmbiente(this.value);">
    <label class="form-check-label" for="defaultCheck1">
      {{$i->nombre}} 
    </label>
  </div>
 @endforeach 

</div>

</div>
