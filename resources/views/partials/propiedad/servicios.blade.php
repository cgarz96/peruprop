

  <div class="col-md-4"> 
  <div class="card elevation-3">
    <div class="card-header bg-dark text-white text-center">
      <b>{{$ambientes}}</b>
    </div>
    <ul class="list-group list-group-flush">
    @foreach ($Ambientes  as $i)
      <li class="list-group-item"><i class="fas fa-check-circle text-success"></i> {{$i->nombre}} </li>
     @endforeach
    </ul>
  </div>
  </div>

  <div class="col-md-4">
  <div class="card elevation-3">
    <div class="card-header bg-dark text-white text-center">
      <b>{{$servicio}}</b>
    </div>
    <ul class="list-group list-group-flush">
    @foreach ($Servicios  as $i)
      <li class="list-group-item"><i class="fas fa-check-circle text-success"></i> {{$i->nombre}} </li>
     @endforeach
    </ul>
  </div>
  </div>

  <div class="col-md-4">
  <div class="card elevation-3">
    <div class="card-header bg-dark text-white text-center">
      <b>{{$instalaciones}}</b>
    </div>
    <ul class="list-group list-group-flush">
    @foreach ($Instalaciones  as $i)
      <li class="list-group-item"><i class="fas fa-check-circle text-success"></i> {{$i->nombre}} </li>
     @endforeach
    </ul>
  </div>
  </div>


