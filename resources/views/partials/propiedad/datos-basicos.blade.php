@php

  if($descripcionPropiedad[0]->tipo_operacion =='venta'){
    $descripcionPropiedad[0]->tipo_operacion='Venta';
  }elseif($descripcionPropiedad[0]->tipo_operacion =='alquiler'){
    $descripcionPropiedad[0]->tipo_operacion='Alquiler';
  }
  elseif($descripcionPropiedad[0]->tipo_operacion =='alquilerXtemporada'){
   $descripcionPropiedad[0]->tipo_operacion='Alquiler por temporada';
  }else{
    $descripcionPropiedad[0]->tipo_operacion='Tiempo compartido';
  }


@endphp
<div class=" col-md-12">
  <div class="card text-center elevation-3">
    <div class="card-header">
      <blockquote class="blockquote mb-0">
      Datos Básicos
      </blockquote>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-4">
         <b>Tipo de operación: </b>{{$descripcionPropiedad[0]->tipo_operacion}}
        </div>
         <div class="col-md-4">
          <b>Codigo de aviso: </b>{{$descripcionPropiedad[0]->codigo_publicacion}}
        </div>
        <div class="col-md-4">
          <b>Precio: </b>$ {{$descripcionPropiedad[0]->precio}}
        </div>
      </div>
    </div>
  </div>
</div>
