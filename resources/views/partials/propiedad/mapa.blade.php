<div class="col-md-12">
  <div class="card elevation-3">
	<div class="card-header text-center">
	    <blockquote class="blockquote mb-0">
	    Ubicación
	    </blockquote>
	  </div>
		<div id="map" style='width: 100%; height: 30em;' ></div>
		<input type="hidden" value="{{$descripcionPropiedad[0]->latitud}}" id="latitud">
		<input type="hidden" value="{{$descripcionPropiedad[0]->longitud}}" id="longitud">
		<div class="card-footer text-muted">
		   <b>Dirección: </b> <cite> {{$descripcionPropiedad[0]->direccion}}, {{$descripcionPropiedad[0]->ubicacion_completa}}</cite>
	   </div>
	</div>
</div>