@if($tipo=='Inmobiliaria')
  <div class="card text-center elevation-3">
    <div class="card-header">
   Contactá a la inmobiliaria
  </div> 
  <div class="card-body">
    <h5 class="card-title" style="display: none;">{{$propietario[0]->razon_social}}</h5>
    <div class="media">
      <div class="media-body">
        <h5 class="mt-0 mb-1">{{$propietario[0]->razon_social}}</h5>
      </div>
      <img src="{{ asset($propietario[0]->logo_anunciante) }}" class="" alt="..." width="30" height="30" style="border-radius: 10px;">
    </div>
    <form method="post" action="{{ route('enviarMail') }}">
      @csrf
      <div class="form-group">
        <label for="exampleInputEmail1">Correo</label>
        <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp"  required="">
      </div>
      <div class="form-group">
        <label for="exampleFormControlTextarea1">Mensaje</label>
        <textarea class="form-control" required="" id="exampleFormControlTextarea1" name="mensaje" rows="3" placeholder="Hola, vi esta propiedad en Peruprop y quiero que me contacten. Gracias."></textarea>
      </div>
      <input type="hidden" name="correo_propietario" value="{{$propietario[0]->correo_anunciante}}">
      <input type="hidden" name="propiedad_id" value="{{$descripcionPropiedad[0]->propiedad_id}}">

      @guest
        <a class="btn  bg-gradient-olive" data-toggle="modal" data-target="#exampleModal" role="button" href="#">Contactar</a>
      @else
      <button type="submit" class="btn bg-gradient-olive" class="contactar-spinner">Contactar</button>
      @endguest

      @if (session('status'))
        <div class="alert text-center mt-2" role="alert" style="background: #d4edda; color: #155724">
          {{ session('status') }}
        </div>
      @endif

    </form>
  </div>  
</div>

@elseif($tipo=='Agente')
  <div class="card text-center elevation-3">
  <div class="card-header">
   Contactá al Agente
  </div>
  <div class="card-body">
    <div class="d-flex justify-content-center">
    <h5 class="card-title">{{$propietario[0]->razon_social}}</h5></div>
    <p class="card-text"><i class="fas fa-phone-alt"></i> {{$propietario[0]->telefono_anunciante}}</p>
    <p class="card-text">Correo: {{$propietario[0]->correo_anunciante}}</p>   
  </div> 
</div>
@else
  <div class="card text-center elevation-3">
  <div class="card-header">
   Contactá al Dueño
  </div>
  <div class="card-body">
    <h5 class="card-title">{{$propietario[0]->razon_social}}</h5>
    <p class="card-text"><i class="fas fa-phone-alt"></i> {{$propietario[0]->telefono_anunciante}}</p>
    <p class="card-text">Correo: {{$propietario[0]->correo_anunciante}}</p>   
  </div> 
</div>
@endif

<script type="text/javascript">
  $('.contactar-spinner').click(function () {
   $(this).attr('disabled',true); 
  $(this).html(` <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
  Enviando correo...`);
  })
</script>