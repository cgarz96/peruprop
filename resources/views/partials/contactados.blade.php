

@if(count($historial)==0)
	<div class="container d-flex justify-content-center " >
	<div class="card mt-3 text-center elevation-3" style="width: 18rem;">
    <i class="far fa-address-card fa-7x text-teal"></i>
	  <div class="card-body">
	    <h5 class="card-title">Tu próximo hogar espera por ti</h5>
	    <p class="card-text text-muted">Comunícate hoy mismo con los anunciantes de tus avisos preferidos.</p>
	    <a href="{{ url('search') }}" class="btn bg-gradient-olive">Ir al listado</a>
	  </div>
	</div>
	</div>
@else
<div class="container d-flex justify-content-center " style="min-height: 600px"  >
Contactados
</div>
@endif