
	
 
  <div class="form-row">
    <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Estado de edificio</span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="estado" required="">
    </div>
    <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Tipo de balcon</span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="tipo_balcon" required="">
    </div>


     <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Expensas</span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="expensas" required=""  pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$" title="Información: No se admite mas de un punto, ni signos">
    </div>


 <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Cantidad de cocheras</span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="cant_chocheras" required=""  pattern="[0-9]" title="Información: Debe ser un numero entero.">
    </div>

    </div>



