
  
  <div class="form-row">
    <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Cantidad de baños: </span>
    </div>
      <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="cant_baños" required="">
    </div>
    <div class="form-group input-group mb-3 col-md-3" style="display: none;">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Superficie cubierta: </span>
    </div>
      <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="sup_cubierta" >
    </div>

 <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Cantidad de dormitorios: </span>
    </div>
      <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="cant_dormitorios" required="">
    </div>


   <div class="input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Tipo de piso: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="tipo_piso" required="">
    
    </div>

  </div>

<div  class="row">
  <div class="input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Tipo de techo: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="tipo_techo" required="">
    
    </div>

      <div class="input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Superficie total: </span>
    </div>
      <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="sup_total" required="">
    
    </div>

        <div class="input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Estado: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="estado" required="">
    
    </div>
 
  



</div>