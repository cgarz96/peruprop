<div >
  
  <div class="form-row">
    <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Longitud de frente: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="long_frente" required="" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$" title="Información: No se admite mas de un punto, ni signos">
    </div>
    <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Longitud de fondo: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="long_fondo" required="" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$" title="Información: No se admite mas de un punto, ni signos">
    </div>

 <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Superficie construible: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="sup_construible" required="" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$" title="Información: No se admite mas de un punto, ni signos">
    </div>




 
    </div>

<div class="row">
  <div class="form-group input-group mb-3 col-md-6">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Zonificación: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="zonificacion" required="">
    </div>
   <div class="input-group mb-3 col-md-6">
      <div class="input-group-prepend">
      <span class="input-group-text" for="inputGroupSelect01">Tipo de pendiente: </span>
      </div>
      <select class="custom-select "  required="" name="tipo_de_pendiente">
           <option value="">--Selecione el tipo de pendiente del terreno--</option>
                <option value="plana">Plana</option>
                <option value="ligeramente_suave">Ligeramente suave</option>
                <option value="suave">Suave </option>
                <option value="moderada">Moderada</option>
                <option value="fuerte">Fuerte</option>            
      </select>
    </div>
</div>

</div>