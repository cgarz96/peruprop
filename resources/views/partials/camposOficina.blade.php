<div >
  
  <div class="form-row">
    <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Cantidad de baños: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="cant_baños" required="" pattern="[0-9]" title="Información: Debe ser un numero entero.">
    </div>
    <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Supercicie cubierta: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="sup_cubierta" required="">
    </div>

 <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Ambientes: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="ambientes" required="">
    </div>

     <div class="form-group input-group mb-3 col-md-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Expensas: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="expensas" required="" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$" title="Información: No se admite mas de un punto, ni signos">
    </div>
</div>

<div class="row">
     <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Superficie de planta: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="sup_planta" required="" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$" title="Información: No se admite mas de un punto, ni signos">
    </div>

     <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Superficie total: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="sup_total" required="" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$" title="Información: No se admite mas de un punto, ni signos">
    </div>
     <div class="form-group input-group mb-3 col-md-4">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Estado: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="estado" required="">
    </div>
</div>
    

</div>