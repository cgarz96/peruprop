

@if(count($favoritos)==0)
	<div class="container d-flex justify-content-center " >
	<div class="card mt-3 text-center elevation-3" style="width: 18rem;">
	  <i class="far fa-heart fa-7x text-teal"></i>
	  <div class="card-body">
	    <h5 class="card-title">¿Cuántos inmuebles te llegaron al corazón?</h5>
	    <p class="card-text text-muted">Guárdalos aquí como favoritos hasta que encuentres el ideal.</p>
	    <a href="{{ url('search') }}" class="btn bg-gradient-olive">Ir al listado</a>
	  </div>
	</div>
	</div>
@else
<div class="container d-flex justify-content-center " style="min-height: 600px"  >
<div class="row container">
@foreach($favoritos as $propiedad)
<div class="col-md-12 ">
            <div class="card card-primary elevation-3 mt-2"> 
              <div class="card-header" hidden="">
                <h3 class="card-title">Ribbons</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">

                  <div class="col-md-4">
                    <div class="fotorama"  data-height="150"  >
                     {{-- @foreach($propiedad->fotos as $foto)
                      <img src="{{  asset($foto->ruta) }}" alt="Photo 1" class="img-fluid">
                       @endforeach--}}
                       <img src="{{  asset($propiedad->foto_principal) }}" alt="Photo 1" class="img-fluid">
  
                    </div>
                      <div class="ribbon-wrapper ribbon-sm" >
                          <div class="ribbon bg-success " style="font-size: 12px">
                            {{$propiedad->visibilidad}}
                          </div>
                        </div>
                    </div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-12">
                        <h5>{{$propiedad->titulo}}</h5>
                      </div>


                      <div class="col-md-12 " style="min-height: 73px">
                    <p class="block-with-text text-muted text-sm">{{$propiedad->descripcion}}</p>    
                      </div>

                 

                      
                    </div> 
                    <hr>
                    <div class="row">
                        <div class="col-md-6"><i class="fas fa-sort-amount-up-alt"></i> {{$propiedad->tipo_operacion}}</div>
                        <div class="col-md-6"> <i class="far fa-calendar-alt"></i> {{$propiedad->fecha_alta}}</div>
                      </div>
                  </div>

                      <div class="col-md-2">
                        <div class="row">
                          <div class="col-md-9 col-sm-9"><label>$ {{$propiedad->precio}}</label></div>
                          @guest
                         <div class="col-md-3 col-sm-3"><button class=" btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#exampleModal" data-micron="bounce" style="border-radius: 15px" ><i class="fas fa-heart"></i></button></div>
                          @else
                          <div class="col-md-3 col-sm-3"><button data-micron="bounce" id="{{$propiedad->propiedad_id}}" class=" btn btn-sm btn-outline-danger favorito {{ esFavorito($propiedad->propiedad_id) }}"    style="border-radius: 15px" ><i class="fas fa-heart"></i></button></div>
                          @endguest
                                 
                          </div>
                          <br>
                          <br>
                          <br>
                          <hr class="mb-3">

                          <div class="row justify-content-center">
                            <a  href="{{ url('busquedas-codigo-'.$propiedad->codigo_publicacion) }}" class="btn bg-gradient-olive">Ver mas detalles</a>
                          </div>

                        
                      </div>


                </div>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
   @endforeach 
  </div>

</div>
 <div class="pagination pagination-sm m-0 d-flex justify-content-center">
                    {{ $favoritos->appends(Request::all())->links() }}
                </div>
<script type="text/javascript">
  $('.favorito').click(function (e) {
  
  var id_favorito=this.id;
   $.ajax({
          url: "{{ route('favoritos') }}",
          data:{propiedad_id:id_favorito, _token: "{{ csrf_token() }}"},
          type: "POST",
          success: function(response){
          
          if (response=="add") {
            $('#'+id_favorito).addClass('active')
        console.log(response)}
          else{
            $('#'+id_favorito).removeClass('active');
            console.log(response)
          }
            },
             error : function(message) {
               console.log(message);
            }
          });

  });
</script>
@endif