@extends('layouts.app')

@section('content')


    <div class="row ml-2 mr-2 mb-2">
    @foreach($propiedades as $propiedad)
        
    <div class="card ml-2 mt-2" style="width: 23em; ">
	<div class="fotorama" data-width="300" data-height="200">
					@foreach($propiedad->fotos as $foto)
					<img src="{{  asset($foto->ruta) }}" >		
					@endforeach
					</div>
     <!-- <img src="{{  asset($propiedad->caracteristica->foto_principal) }}" class="card-img-top" alt="..." style="height: 15em;">-->
       <h3 class="caption "><b class="text-white" style="color: white !important;">$ {{$propiedad->precio}}</b></h3>
      <div class="card-body">
        <h5 class="card-title">{{$propiedad->titulo}}</h5>
        <p class="card-text trunc" >{{$propiedad->descripcion}}</p>
      <a href="{{ url('busquedas-codigo-'.$propiedad->codigo_publicacion) }}" class="btn btn-success">Ver mas detalles</a>
      </div>
    </div>

{{--var_dump($propiedad->fotos)--}}
				
    @endforeach 
    </div>

@endsection