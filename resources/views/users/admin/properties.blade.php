@extends('layouts.app-admin')

@section('content')

<section class="content">
	 <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Propiedades</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin') }}">Inicio</a></li>
              <li class="breadcrumb-item active">Propiedades</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<section class="container-fluid">
	@if ( session('mensaje') )
    <div class="alert alert-success mt-5 mb-5 mensaje">
    {{ session('mensaje') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
	  </button>
	</div>
	@else
	 <div class="alert alert-danger mt-5 mb-5 mensaje" style="display: none;">
	    Ocurrió un problema!
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">&times;</span>
	  </button>
	</div>
	@endif
</section>

<div class="container-fluid">

        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header text-center">
                <h3 class="card-title ">¿Desea registrar nuevos servicios?</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body " style="height: 200px;">
					 <form action="{{ url('/admin/properties/newservice') }}" method="POST"  > 
					 <div class="form-row mt-5">

						@csrf
					<div class="input-group mb-3 col-md-6">

					 <div class="input-group-prepend">
					    <button class="btn btn-primary " type="submit">Registrar</button>
					  </div>
						  <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="nombre" required="">
						</div>
					</form>

					<div class="input-group mb-3 col-md-6">
					  <div class="input-group-prepend">
					    <label class="input-group-text" for="inputGroupSelect01">Servicios agregados :</label>
					  </div>
					  <select class="custom-select " id="inputGroupSelect01">
					       <option value="">----</option>
					  	@foreach ($ServiciosRegistrados  as $i)
							<option value="{{$i->id}}">{{$i->nombre}} </option>
							 @endforeach 
					  </select>
					</div>
					</div>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
        <!-- /.row -->

        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header text-center">
                <h3 class="card-title ">¿Desea registrar nuevas instalaciones?</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body " style="height: 200px;">
				 <form action="{{ url('/admin/properties/newinstall') }}" method="POST"  >
				 <div class="form-row mt-5">
				@csrf
				<div class="input-group mb-3 col-md-6">

				 <div class="input-group-prepend">
				    <button class="btn btn-primary " type="submit">Registrar</button>
				  </div>
					  <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="nombre" required="">
				</div>
				</form>
				 <hr class="py-2">

				<div class="input-group mb-3 col-md-6">
				  <div class="input-group-prepend">
				    <label class="input-group-text" for="inputGroupSelect01">Instalaciones agregadas :</label>
				  </div>
				  <select class="custom-select " id="inputGroupSelect01">
				       <option value="">----</option>
				    @foreach ($InstalacionesRegistradas  as $i)
						<option value="{{$i->id}}">{{$i->nombre}} </option>
						 @endforeach 
				  </select>
				</div>

				</div>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
        <!-- /.row -->
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header text-center">
                <h3 class="card-title ">¿Desea registrar nuevos ambientes?</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body " style="height: 200px;">
				  <form action="{{ url('/admin/properties/newambience') }}" method="POST"  >
				 <div class="form-row mt-5">
				@csrf
				<div class="input-group mb-3 col-md-6">

				 <div class="input-group-prepend">
				    <button class="btn btn-primary " type="submit">Registrar</button>
				  </div>
					  <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="nombre" required="">
				</div>
				 <hr class="py-2">

				<div class="input-group mb-3 col-md-6">
				  <div class="input-group-prepend">
				    <label class="input-group-text" for="inputGroupSelect01">Ambientes agregados :</label>
				  </div>
				  <select class="custom-select " id="inputGroupSelect01">
				       <option value="">----</option>
				     @foreach ($AmbientesRegistrados  as $i)
						<option value="{{$i->id}}">{{$i->nombre}} </option>
						 @endforeach 
				  </select>
				</div>

				</div>

				</form>

				</div>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div>
</section>
<script type="text/javascript">
	setTimeout(function() {
        $(".mensaje").fadeOut(1500);
    },3000);

</script>
@endsection