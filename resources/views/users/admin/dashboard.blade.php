@extends('layouts.app-admin')

@section('content')
    <section class="content">
    	<br>
     <div class="error-page ">
        <h2 class="headline text-olive" ><i class="fas fa-smile-beam"></i></h2>

        <div class="error-content">
          <h3>Bienvenido Administrador <i class="fas fa-exclamation text-olive"></i></h3>

          <p hidden="">
            Para poder usar todos los servicios que ofrece el sistema es necesario completar su perfil de usuario, haciendo clik en el siguiente <a href="">enlace</a>.
          </p>
        </div>
        <!-- /.error-content -->
      </div>
  	</section>
@endsection