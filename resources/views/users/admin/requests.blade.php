@extends('layouts.app-admin')

@section('content')

<section class="content">
	    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Solicitudes de publicación</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin') }}">Inicio</a></li>
              <li class="breadcrumb-item active">Solicitudes</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 400px;">
                <table class="table table-head-fixed ">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Tipo propietario</th>
                      <th>Propiedad solicitada</th>
                      <th>Tipo de pago</th>
                      <th>Estado de pago</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  	 @foreach($publicacionesSolicitadas as $dato)

					    @if($dato->tipo_propiedad=='casa')
					    
					    @php
					    $dato->tipo_propiedad='Casa';
					    @endphp
					     @endif
					    <tr>
					      <th scope="row">{{$loop->iteration}}</th>
					    {{-- <td><img src="{{ asset('storage').'/'.$dato->logo}}" width="100" height="50"></td>--}}
					      <td>{{$dato->tipo_propietario}}</td>
					      <td>{{$dato->tipo_propiedad}}</td>
					      <td>{{$dato->payment_type}}</td>
					      <td>{{$dato->collection_status}}</td> 
					      <td>

					        <button class="btn btn-primary aceptar" type="button"  name="{{$dato->tipo_propietario}}" id="{{$dato->propiedad_id}}" value="{{$dato->user_id}}">Aprobar</button>
					      <button class="btn btn-danger rechazar" type="button"  id="{{$dato->id}}" >Rechazar</button></td>
					    </tr>

					   @endforeach 
                    

                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->


                            <!-- /.card-body -->
              <div class="card-footer clearfix">
                <div class="pagination pagination-sm m-0 float-right">
                {{ $publicacionesSolicitadas->appends(Request::all())->links() }}
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div>
</section>

<script type="text/javascript">



$("#search").on("keyup", function() {
    var value = $(this).val();
    if(value!='') {
       $("table tbody tr").hide();
    }else{
      $("table tbody tr").show();
  }
  $('table tbody tr td:contains("'+value+'")').parent('tr').show(); 
});




    $(".aceptar").click(function(e){
        $.ajax({
          url: "{{ route('publications-accept') }}",
          data:{id:this.id,user_id:this.value,tipo_propietario:this.name, _token: "{{ csrf_token() }}"},
          type: "POST",
          success: function(response){
          
console.log(response)
       bootbox.alert(response, function(){ 
            location.reload();
        });

            },
             error : function(message) {
               console.log(message);
            }
          });

  });




    $(".rechazar").click(function(e){
         
    $.ajax({
    url: "{{ route('publications-refused') }}",
    data:{id:this.id, _token: "{{ csrf_token() }}"},
    type: "POST",
    success: function(response){
    
console.log(response)
     bootbox.alert(response, function(){ 
          location.reload();
      });

      },
       error : function(message) {
         console.log(message);
      }
    });

  });





      </script>

      <script src="{{ asset('js/tables.js')}}"></script>

@endsection