@extends('layouts.app-admin')

@section('content')

<section class="content">
	 <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Inmobiliarias Registradas</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin') }}">Inicio</a></li>
              <li class="breadcrumb-item active">Inmbiliarias</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 400px;">
                <table class="table table-head-fixed ">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nombre Inmobiliaria</th>
                      <th>Cantidad de publicaciones</th>
                      <th>Avisos</th>
                    </tr>
                  </thead>
                  <tbody>
				    @foreach($inmobiliariasRegistradas as $dato)

				    <tr>
				      <th scope="row">{{$loop->iteration}}</th>
				      <td>{{$dato->razon_social}}</td> 
				      <td>{{$dato->cant_publicaciones}}</td>
				      <td><a href="{{ url('inmobiliaria-'.$dato->user_id)}}" class="btn btn-warning  ">Ver avisos</a> <button id="{{$dato->user_id}}" class="btn btn-danger" type="button" onclick="eliminarAvisos(this.id);">Eliminar avisos</button></td> 

				    </tr>

				   @endforeach 
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->


                            <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div>
</section>
<script>
  
  function eliminarAvisos(id) {
    $.ajax({
    url: "{{ route('publications-delete') }}",
    data:{user_id:id,tipo_propietario:'Inmobiliaria', _token: "{{ csrf_token() }}"},
    type: "POST",
    success: function(response){
    console.log(response);
    location.reload();

      },
       error : function(message) {
         console.log(message);
      }
    });
  }
</script>
@endsection