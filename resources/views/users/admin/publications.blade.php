@extends('layouts.app-admin')

@section('content') 

<section class="content">
	 <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Publicaciones</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin') }}">Inicio</a></li>
              <li class="breadcrumb-item active">Publicaciones</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
<div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 400px;">
                <table class="table table-head-fixed ">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Tipo propiedad</th>
                      <th>Tipo operación</th>
                      <th>Precio</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
				  	@foreach($publicaciones as $dato)
				    <tr>
				      <th scope="row">{{$loop->iteration}}</th>
				      <td>{{$dato->tipo_propiedad}}</td>
				      <td>{{$dato->tipo_operacion}}</td> 
				      <td>{{$dato->precio}}</td> 
				      <td><a class="btn btn-primary" href="{{ url('busquedas-codigo-'.$dato->codigo_publicacion) }}"  role="button">Ver publicación</a>
				      	<button type="button" class="btn btn-danger delete-i" nro_pago="{{$dato->pago_id}}" id="{{$dato->id}}" value="{{$dato->tipo_propietario}}" name="{{$dato->user_id}}">Eliminar</button></td>
				    </tr>
					@endforeach 
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->


                            <!-- /.card-body -->
              <div class="card-footer clearfix">
                <div class="pagination pagination-sm m-0 float-right">
                {{ $publicaciones->appends(Request::all())->links() }}
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div>
</section>
<script type="text/javascript">

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
	$('.delete-i').click(function(e){ 
  var pago_id = $(this).attr("nro_pago");
  $.ajax({
    url: "{{ route('publication-delete') }}",
    data:{id:this.id,user_id:this.name,tipo_propietario:this.value,pago_id:pago_id},
    type: "POST",
    success: function(response){
    console.log(response);
    location.reload();

      },
       error : function(message) {
         console.log(message);
      }
    });

});


</script>
@endsection