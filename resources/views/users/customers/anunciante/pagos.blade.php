@extends('layouts.app')


@section('content')

<div class="container">
	@isset($preference)
    <div class="d-flex justify-content-center mt-5">
	       <div class="col-md-4 card  ">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h4 class="text-center mt-4">
                        Pago por publicacion</h4>
                </div>
                <div class="panel-body text-center ">
                    <p class="lead">
                        <strong>$100</strong></p>
                </div>
                <ul class="list-group list-group-flush text-center">
                    <li class="list-group-item"><i class="icon-ok text-danger"></i>Una Publicacion</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i>Cualquier tipo de propiedad</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i>Publicacion permanente</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i>Medio
                    <img src="{{ asset('img/logo-mercadopago8.png')}}" class="rounded" alt="" style="width: 30%;">
                    </li>
                </ul> 
                <div class="panel-footer d-flex justify-content-center ">

                     <a href="{{$preference}}" class="btn btn-success m-3">Lo quiero</a>
                 
                </div>
            </div>
        </div>
</div>
@endisset

@isset($preferenceSub)
    <div class="d-flex justify-content-center mt-5">
           <div class="col-md-4 card  ">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h4 class="text-center mt-4">
                        Pago por mes</h4>
                </div>
                <div class="panel-body text-center ">
                    <p class="lead">
                        <strong>$100</strong></p>
                </div>
                <ul class="list-group list-group-flush text-center">
                    <li class="list-group-item"><i class="icon-ok text-danger"></i>Una Publicacion</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i>Cualquier tipo de propiedad</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i>Publicacion permanente</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i>Medio
                    <img src="{{ asset('img/logo-mercadopago8.png')}}" class="rounded" alt="" style="width: 30%;">
                    </li>
                </ul> 
                <div class="panel-footer d-flex justify-content-center ">

                     <a href="{{$preferenceSub}}" class="btn btn-success m-3">Lo quiero</a>
                 
                </div>
            </div>
        </div>
</div>
@endisset
</div> 

@endsection