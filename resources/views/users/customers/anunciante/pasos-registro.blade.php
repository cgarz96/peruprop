@extends('layouts.app')


@section('content')

<div class="container ">
    
 <div class=" mt-5  "> 
<ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link section-tab active" id="section1-tab" data-toggle="tab" href="#section1" role="tab" aria-controls="section1" aria-selected="true">Detalle de compra</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link section-tab" id="section2-tab"  role="tab" aria-controls="section2" aria-selected="false">Paga</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link section-tab" id="section3-tab"  role="tab" aria-controls="section3" aria-selected="false">¡Listo!</a>
  </li>
</ul>

<form method="post" id="form-user" action="{{ route('registrarAnunciante')}}" enctype="multipart/form-data">
    @csrf
<div class="tab-content mt-5" id="myTabContent">
  <div class="tab-pane fade show active" id="section1" role="tabpanel" aria-labelledby="section1-tab">

<!-- Seccion 1 -->


<div class="row ">
<div class="col-md-8">
                <div class="card mb-4 box-shadow "> 
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Plan  </h4> 
                    </div>
                    <div class="card-body ">
                        
                        <ul class="list-unstyled mt-3 mb-4">
                            <li class="">
                                <div class="row"> 
                                    <div class="col-md-6 col-sm-12">
                                     <h4><b class="">Plan Mensual </b></h4>
                                    </div> 
                                    <div class="col-md-6 col-sm-12 ">
                                     <h4><b class=" d-flex justify-content-end">S/ {{$modalidad[0]->precio}} </b></h4>
                                    </div> 
                                    </div>
                                </li>
                            <li><h4><b class="mr-4 text-muted" >Beneficios: </b></h4></li>
                            <li> <i class="fas fa-check-circle text-success"></i> 1 aviso que dura 30 días online. La visibilidad en el listado es destacada.</li>
                            <li><i class="fas fa-check-circle text-success"></i> Tiene mejor exposición y genera más interesados.</li>
                            <li> <i class="fas fa-check-circle text-success"></i> Se resalta con una etiqueta de destacado.</li>
                        </ul> 
                    </div>
                </div>
</div>

            <div class="col-md-4 ">
                <div class="card mb-4 box-shadow "> 
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Detalle de compra</h4> 
                    </div>
                    <div class="card-body ">
                        
                        <ul class="list-unstyled mt-3 mb-4">
              
                            <li>Plan Mensual S/{{$modalidad[0]->precio}} + IGV</li>
                            <hr>
                            <li><h1><b class="mr-4">Total </b> <b class="text-muted ml-5">S/ {{$modalidad[0]->precio}}</b></h1></li>
                            <hr>
                        </ul> 
                      <button id="b-next-section1" class="btn btn-lg btn-block btn-success" onclick="defineTab(this.id);" >Comprar »</button>
                    </div>
                </div>
            </div>



    </div>


 
  </div>





<style type="text/css">
    .altura{
        height: 20px;
    }
</style>





  <div class="tab-pane fade " id="section2" role="tabpanel" aria-labelledby="section2-tab"> 

<!-- Seccion 2 -->


<div class="row ">
<div class="col-md-8">
                <div class="card mb-4 box-shadow "> 
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Tus datos </h4> 
                    </div>
                    <div class="card-body ">
     <label>Estos datos serán utilizados para contactarte</label>                   
  <div class="form-row mb-1">
    <div class="col">
        <label class="m-0">Nombre (*):</label>
      <input type="text" class="form-control  required" placeholder="" name="nombre" value="{{$datos->first_name}}">
    </div>
    <div class="col">
         <label class="m-0">Apellido (*):</label>
      <input type="text" class="form-control  required" placeholder="" name="apellido" value="{{$datos->last_name}}">
    </div>
  </div>

    <div class="form-row mb-1">
    <div class="col-md-6">
        <label class="m-0">Movil (*):</label>
     <input type="text" class="form-control  required" placeholder="" value="{{$datos->cellphone}}"  name="movil_user" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$">
    </div>
    <div class="col-md-6">
         <label class="m-0">Teléfono</label>
      <input type="text" class="form-control " placeholder="" name="telefono_user" value="{{$datos->telephone}}" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$">
    </div>
  </div>
  <div class=" form-row mb-1">
        <div class="col">
        <label class="m-0">Correo electrónico (*):</label>
     <input type="email" class="form-control  required" placeholder="" value="{{$datos->email}}"  name="correo_contacto">
    </div>
  </div>

                    </div>

                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Datos de facturación </h4> 
                    </div>


        <div class="card-body ">
            <label> Al completar esta información se calcularán los impuestos sobre el plan.</label>
  <div class="form-row mb-1">
    <div class="col">
        <label class="m-0">DNI (*):</label>
      <input type="text" class="form-control  required" placeholder="Escribe tu DNI aquí sin puntos.." name="dni" value="{{$datos->document}}" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$">
    </div>
    <div class="col">
         <label class="m-0">Nombre/Razon social (*):</label>
      <input type="text" class="form-control  required" placeholder="Escribe el nombre de la inmobiliaria.." name="razon_social">
    </div>
  </div>

    <div class="form-row mb-1">
    <div class="col-md-4">
        <label class="m-0">Correo electrónico (*):</label>
      <input type="email" class="form-control  required" placeholder="" name="correo">
    </div>
    <div class="col-md-4">
         <label class="m-0">Teléfono:</label>
      <input type="text" class="form-control " placeholder="" name="telefono" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$">
    </div>
       <div class="col-md-4">
         <label class="m-0">Móvil:</label>
      <input type="text" class="form-control " placeholder="" name="movil" value="{{$datos->cellphone}}" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$">
    </div>
  </div>

    <div class="form-row mb-1">
    <div class="col">
        <label class="m-0">Domicilio (*):</label>
      <input type="text" class="form-control  required" placeholder="" name="domicilio">
    </div>
    <div class="col">
         <label class="m-0">Código postal (*):</label>
      <input type="text" class="form-control  required" placeholder="" name="codigo_postal" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$">
    </div>
  </div>

      <div class="form-row mb-1">
    <div class="col">
        <label class="m-0">Departamento (*):</label>
      <select class="form-control  required"  id="departamento">
          <option value=""></option>
          @foreach($departamentos as $departamento)
          <option value="{{$departamento->id}}"> {{$departamento->nombre_depto}}</option>
          @endforeach
      </select>
    </div>
    <div class="col">
         <label class="m-0">Provincia (*):</label>
      <select class="form-control  required" placeholder="" name="provincia" id="provincia">
          <option value=""></option>
      </select>
    </div>
  </div>

<input type="hidden" name="paquete_id" value="{{$modalidad[0]->id}}">
<input type="hidden" name="nombre_paquete" value="{{$modalidad[0]->nombre_paquete}}">
<input type="hidden" name="precio_paquete" value="{{$modalidad[0]->precio_igv}}">
<input type="hidden" name="nro_avisos" value="{{$modalidad[0]->nro_avisos}}">
<input type="hidden" name="departamento" id="idepartamento">
                    </div>


                </div>
</div>

            <div class="col-md-4 ">
                <div class="card mb-4 box-shadow "> 
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Detalle de compra</h4> 
                    </div>
                    <div class="card-body ">
                      
                        <ul class="list-unstyled mt-3 mb-4">
              
                            <li>Plan Mensual S/{{$modalidad[0]->precio}} + IGV</li>
                            <hr>
                            <li><h1><b class="mr-4">Total </b> <b class="text-muted ml-5">S/ {{$modalidad[0]->precio}}</b></h1></li>
                            <hr>
                        </ul> 
                      <button id="b-next-section2" class="btn btn-lg btn-block btn-success" onclick="defineTab(this.id);" >Continuar »</button>
                    </div>
                </div>
            </div>



    </div>


 
  </div>










  <div class="tab-pane fade" id="section3" role="tabpanel" aria-labelledby="section3-tab">

<!-- Seccion 3 -->


<div class="row ">
<div class="col-md-8">
                <div class="card mb-4 box-shadow "> 
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Ya casi terminas! </h4> 
                    </div>
                    <div class="card-body ">
                        <ul class="list-unstyled mt-3 mb-4">
              
                            <li><label>Al hacer click en el boton comprar, seras redirecionado a mercado pago para efectuar la operación.</label></li>
                            <hr>
                            <li><label>Una vez finalizado el proceso de pago el sistema le permitira acceder a nuestros servicios.</label></li>
                            <hr>
                        </ul> 
       
                    </div>
                </div>
</div>

            <div class="col-md-4 ">
                <div class="card mb-4 box-shadow "> 
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">Detalle de compra</h4> 
                    </div>
                    <div class="card-body ">
                        
                        <ul class="list-unstyled mt-3 mb-4">
              
                            <li>Plan Mensual S/{{$modalidad[0]->precio}} + IGV</li>
                            <hr>
                            <li><h1><b class="mr-4">Total </b> <b class="text-muted ml-5">S/ {{$modalidad[0]->precio_igv}}</b></h1></li>
                            <hr>
                        </ul> 
                      <button id="b-next-section3" class="btn btn-lg btn-block btn-success" onclick="defineTab(this.id);" >Comprar »</button>
                    </div>
                </div>
            </div>



    </div>


 
  </div>






</div>

</form>

</div>

</div>


<script type="text/javascript"> 
    var tab="";
$(".section-tab").addClass('inactive_tab');
$("#section1-tab").removeClass('inactive_tab');

function defineTab(id) {
    return tab=id;
}

    
    $('#form-user').submit(function(e){
    
    switch(tab){
        case 'b-next-section1':// seccion 1 activa a la seccion 2
        e.preventDefault();
        console.log(tab);
        $(".section-tab").attr('aria-selected',false);
        $(".section-tab").removeClass('active');
        $(".tab-pane").removeClass('active show');

        
        $("#section1-tab").addClass('successfull');

        $("#section2-tab").removeClass('inactive_tab');
        $("#section2-tab").attr('data-toggle','tab');
        $("#section2-tab").attr('href','#section2');
        $("#section2").addClass('active show');
        $("#section2-tab").attr('aria-selected',true);



        $(".required").attr('required',true);// para que los camposde las siguientes seccion sean reuierdos

        break;
        case 'b-next-section2':
        console.log(tab);
        $(".section-tab").attr('aria-selected',false);
        $(".section-tab").removeClass('active');
        $(".tab-pane").removeClass('active show');

        
        $("#section2-tab").addClass('successfull');

        $("#section3-tab").removeClass('inactive_tab');
        $("#section3-tab").attr('data-toggle','tab');
        $("#section3-tab").attr('href','#section3');
        $("#section3").addClass('active show');
        $("#section3-tab").attr('aria-selected',true);

        //$("#form-user").removeAttr('id');

        e.preventDefault();
        break;
        case 'b-next-section3':

        break;
    }



    });


$('#departamento').change(function(e){


//alert();

$("#idepartamento").val(this.options[this.selectedIndex].text);

$.ajax({
    url: "{{ route('provincias') }}",
    data:{id:this.value, _token: "{{ csrf_token() }}"},
    type: "POST",
    success: function(response){
    
        $("#provincia").html("");
        var provincias="";

        for (var i = 0; i < response.provincias.length; i++) {
        provincias+=`<option value="${response.provincias[i].nombre_prov}">${response.provincias[i].nombre_prov}</option>`

        }

        $("#provincia").html(provincias);

      },
       error : function(message) {
         console.log(message);
      }
    });


});



</script>

@section('modal-user'){{--Sobreescribo el modal de registro y login--}}
@endsection

@endsection