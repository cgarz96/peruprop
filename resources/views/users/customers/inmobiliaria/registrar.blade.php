@extends('layouts.app')


@section('content')
 <div class="container">
<div class="card mt-5">
  <div class="card-body">
  <form action="{{ route('registrarInmobiliaria')}}" method="post" enctype="multipart/form-data">
  	@csrf
    <div class="form-group">
      <label for="exampleInputEmail1">Razon social</label>
      <input type="text" class="form-control"  name="razon_social" required="">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Nombre de Inmobiliaria</label>
      <input type="text" class="form-control" name="nombre_inmobiliaria" required="">
    </div>
     <div class="form-group">
      <label for="exampleInputPassword1">Cédula de indentidad</label>
      <input type="text" class="form-control" name="cedula" required="">
    </div>
     <div class="form-group">
      <label for="exampleInputPassword1">Cuit</label>
      <input type="number" class="form-control" name="cuit" required="">
    </div>
     <div class="form-group">
      <label for="exampleInputPassword1">Correo electrónico</label>
      <input type="email" class="form-control" name="correo" required="">
    </div>
     <div class="form-group">
      <label for="exampleInputPassword1">Teléfono</label>
      <input type="number" class="form-control" name="telefono" required="">
    </div>
     <div class="form-group">
      <label for="exampleInputPassword1">Móvil</label>
      <input type="number" class="form-control" name="movil" required="">
    </div>
     <div class="form-group">
      <label for="exampleInputPassword1">Dirección</label>
      <input type="text" class="form-control" name="direccion" required="">
    </div>
        <div class="form-group input-group ">
         <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-default">Departamento</span>

        </div>
       <select class="custom-select " id="i" name="departamento_inmobiliaria" required="">
           <option value="">--Selecione el departamento--</option>
                <option value="Amazonas, Chachapoyas">Amazonas,Chachapoyas</option>    
                <option value="Áncash, Huaraz"> Áncash, Huaraz</option>
                <option value="Apurímac, Abancay">Apurímac, Abancay</option>
                <option value="Arequipa, Arequipa">Arequipa, Arequipa</option>
                <option value="Ayacucho, Ayacucho"> Ayacucho, Ayacucho</option>
                <option value="Cajamarca, Cajamarca"> Cajamarca, Cajamarca</option>
                <option value="Cusco, Cusco"> Cusco, Cusco</option>
                <option value="Huancavelica, Huancavelica">Huancavelica, Huancavelica</option>
                <option value="Huánuco, Huánuco">Huánuco, Huánuco</option>
                <option value="Ica, Ica">Ica, Ica</option>
                <option value="Junín, Huancayo"> Junín, Huancayo</option>
                <option value="La Libertad, Trujillo">La Libertad, Trujillo</option>
                <option value="Lambayeque, Chiclayo">Lambayeque, Chiclayo</option>
                <option value="Lima, Huacho"> Lima, Huacho</option>
                <option value="Loreto, Iquitos"> Loreto, Iquitos</option>
                <option value="Madre de Dios, Puerto Maldonado"> Madre de Dios, Puerto Maldonado</option>
                <option value="Huancavelica, Huancavelica">Moquegua, Moquegua</option>
                <option value="Pasco, Cerro de Pasco">Pasco, Cerro de Pasco</option>
                <option value="Piura, Piura">Piura, Piura</option>
                <option value="Puno, Puno"> Puno, Puno</option>
                <option value="San Martín, Moyobamba">San Martín, Moyobamba</option>
                <option value="Tacna, Tacna"> Tacna, Tacna</option>
                <option value="Tumbes, Tumbes">Tumbes, Tumbes</option>
                <option value="Ucayali, Pucallpa">Ucayali, Pucallpa</option>
      </select>
      </div>
   <div class="form-group">
      <label for="exampleFormControlFile1">Agregue su logo:</label>
      <input type="file" class="form-control-file" id="e" accept="image/png, image/jpeg" name="logo"  required="">
    </div>
    <hr>
    <div class="row">
    <button type="submit" class="btn btn-success ml-auto mr-auto">Registrarse</button></div>
  </form>
</div>
</div>
</div>

@endsection