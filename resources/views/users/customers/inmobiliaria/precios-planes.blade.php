@extends('layouts.app')


@section('content')

<style type="text/css">
section{
  height: 100%;
  width: 100%;
  position: relative;
  overflow: hidden;
  background: url("{{ asset('img/Casas-Modernas.jpg')}}");
}

/* section{
  height: 100%;
  width: 100%;
  position: relative;
  overflow: hidden;
  background: url("{{ asset('img/Casas-Modernas.jpg')}}");
}
section:before {
  content: "";
  position: absolute;
  top: 50%;
  left: 0;
  height: 100%;
  width: 250%;
  background: #f5f5f5;
  -webkit-transform: rotate(-5deg);
  -moz-transform: rotate(-5deg);
  transform: rotate(-5deg);
}*/
</style>
<section>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center text-white" style="background: rgba(0, 0, 0, 0.59);">
      <h1 class="display-4">Planes para inmobiliarias</h1>
      <p class="lead">Si tenés varios avisos para publicar te ofrecemos nuestros packs de abonos especiales para inmobiliarias. Los mismos están pensados para ayudarte a ahorrar tiempo y dinero, ya que con solo un click tu propiedad se publica automáticamente en Peruprop</p>
    </div>

<div class="text-center">
    <div class="container">
        <div class="row pt-4 d-flex justify-content-center" >
            @isset($paquetes[5]->tipo_paquete) 
            <div class="col-md-3 4 ">
                <div class="card mb-4 box-shadow elevation-3"> 
                    <div class="card-header bg-teal disabled color-palette">
                        <h5 class="my-0 font-weight-normal">{{$paquetes[5]->tipo_paquete}} <b class="text-teal"> dsadsdassd</b></h5> 
                    </div>
                    <div class="card-body ">
                        <h1><b>S/ {{$paquetes[5]->precio}} </b><small class="text-muted">+ IGV</small></h1>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li class="m-1">Promoción por 1 mes</li>
                            <li class="m-1">Cantidad de anuncios: {{$paquetes[5]->anuncio}}</li>
                            <li class="m-1">Visibilidad: {{$paquetes[5]->visibilidad}}</li>
                            <li class="m-1">Interés: {{$paquetes[5]->interes}}</li>
                            <li class="m-1">Límite de publicaciones: {{$paquetes[5]->nro_avisos}}</li>
                        </ul>
                        <br> 
                        @auth <!--Pregunto si estoy autenticado me registro y pago-->
                        <a class="btn btn-lg btn-block bg-gradient-success" href="{{  url('/publicaciones/registro/inmobiliaria/'.$paquetes[5]->slug) }}">Me interesa »</a>
                        @else
                        <a class="btn btn-lg btn-block bg-gradient-success" data-toggle="modal" data-target="#exampleModal" role="button" href="{{$paquetes[5]->slug}}">Me interesa »</a>
                        @endauth
                    </div>
                </div>
            </div>
@endisset
@isset($paquetes[0]->tipo_paquete) 
            <div class="col-md-3 4 ">
                <div class="card mb-4 box-shadow elevation-3"> 
                    <div class="card-header bg-teal disabled color-palette">
                        <h5 class="my-0 font-weight-normal">{{$paquetes[0]->tipo_paquete}}</h5> 
                    </div>
                    <div class="card-body ">
                        <h1><b>S/ {{$paquetes[0]->precio}} </b><small class="text-muted">+ IGV</small></h1>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li class="m-1">Promoción por 1 mes</li>
                            <li class="m-1">Cantidad de anuncios: {{$paquetes[0]->anuncio}}</li>
                            <li class="m-1">Visibilidad: {{$paquetes[0]->visibilidad}}</li>
                            <li class="m-1">Interés: {{$paquetes[0]->interes}}</li>
                            <li class="m-1">Límite de publicaciones: {{$paquetes[0]->nro_avisos}}</li>
                        </ul> 
                        @auth <!--Pregunto si estoy autenticado me registro y pago-->
                        <a class="btn btn-lg btn-block bg-gradient-success" href="{{  url('/publicaciones/registro/inmobiliaria/'.$paquetes[0]->slug) }}">Me interesa »</a>
                        @else
                        <a class="btn btn-lg btn-block bg-gradient-success" data-toggle="modal" data-target="#exampleModal" role="button" href="{{$paquetes[0]->slug}}">Me interesa »</a>
                        @endauth
                    </div>
                </div>
            </div>
@endisset
        </div>

        <div class="row pt-4">
        @isset($paquetes[1]->tipo_paquete) 
            <div class="col-md-3 ">
                <div class="card mb-4 box-shadow elevation-3"> 
                    <div class="card-header bg-teal disabled color-palette">
                        <h4 class="my-0 font-weight-normal">{{$paquetes[1]->tipo_paquete}}</h4> 
                    </div>
                    <div class="card-body ">
                        <h1><b>S/ {{$paquetes[1]->precio}} </b><small class="text-muted">+ IGV</small></h1>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li class="m-1">Plan {{$paquetes[1]->tipo_periodo}}</li>
                            <li class="m-1">Cantidad de avisos: {{$paquetes[1]->nro_avisos}}</li>
                            <li class="m-1">Visibilidad: {{$paquetes[1]->visibilidad}}</li>
                            <li class="m-1">Interés: {{$paquetes[1]->interes}}</li>
                        </ul> 
                        <br>
                        @auth <!--Pregunto si estoy autenticado me registro y pago--> 
                         <a class="btn btn-lg btn-block bg-gradient-success" href="{{  url('/publicaciones/registro/inmobiliaria/'.$paquetes[1]->slug) }}">Me interesa »</a>
                        @else
                        <a class="btn btn-lg btn-block bg-gradient-success" data-toggle="modal" data-target="#exampleModal" role="button" href="{{$paquetes[1]->slug}}">Me interesa »</a>
                        @endauth
                    </div>
                </div>
            </div>
        @endisset
@isset($paquetes[2]->tipo_paquete) 
            <div class="col-md-3 ">
                <div class="card mb-4 box-shadow elevation-3"> 
                    <div class="card-header bg-teal disabled color-palette">
                        <h4 class="my-0 font-weight-normal">{{$paquetes[2]->tipo_paquete}}</h4> 
                    </div>
                    <div class="card-body ">
                        <h1><b>S/ {{$paquetes[2]->precio}} </b><small class="text-muted">+ IGV</small></h1>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li class="m-1">Plan {{$paquetes[2]->tipo_periodo}}</li>
                            <li class="m-1">Cantidad de avisos: {{$paquetes[2]->nro_avisos}}</li>
                            <li class="m-1">Visibilidad: {{$paquetes[2]->visibilidad}}</li>
                            <li class="m-1">Interés: {{$paquetes[2]->interes}}</li>
                        </ul> 
                        <br>
                        @auth <!--Pregunto si estoy autenticado me registro y pago--> 
                         <a class="btn btn-lg btn-block bg-gradient-success" href="{{  url('/publicaciones/registro/inmobiliaria/'.$paquetes[2]->slug) }}">Me interesa »</a>
                        @else
                        <a class="btn btn-lg btn-block bg-gradient-success" data-toggle="modal" data-target="#exampleModal" role="button" href="{{$paquetes[2]->slug}}">Me interesa »</a>
                        @endauth
                    </div>
                </div>
            </div>
    @endisset
@isset($paquetes[3]->tipo_paquete) 
            <div class="col-md-3 ">
                <div class="card mb-4 box-shadow elevation-3"> 
                    <div class="card-header bg-teal disabled color-palette">
                        <h4 class="my-0 font-weight-normal">{{$paquetes[3]->tipo_paquete}}</h4> 
                    </div>
                    <div class="card-body ">
                        <h1><b>S/ {{$paquetes[3]->precio}} </b><small class="text-muted">+ IGV</small></h1>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li class="m-1">Plan {{$paquetes[3]->tipo_periodo}}</li>
                            <li class="m-1">Cantidad de avisos: {{$paquetes[3]->nro_avisos}}</li>
                            <li class="m-1">Visibilidad: {{$paquetes[3]->visibilidad}}</li>
                            <li class="m-1">Interés: {{$paquetes[3]->interes}}</li>
                        </ul> 
                        <br>
                        @auth <!--Pregunto si estoy autenticado me registro y pago--> 
                         <a class="btn btn-lg btn-block bg-gradient-success" href="{{  url('/publicaciones/registro/inmobiliaria/'.$paquetes[3]->slug) }}">Me interesa »</a>
                        @else
                        <a class="btn btn-lg btn-block bg-gradient-success" data-toggle="modal" data-target="#exampleModal" role="button" href="{{$paquetes[3]->slug}}">Me interesa »</a>
                        @endauth
                    </div>
                </div>
            </div>
@endisset
@isset($paquetes[4]->tipo_paquete) 
            <div class="col-md-3 ">
                <div class="card mb-4 box-shadow elevation-3 "> 
                    <div class="card-header bg-teal disabled color-palette">
                        <h4 class="my-0 font-weight-normal">{{$paquetes[4]->tipo_paquete}}</h4> 
                    </div>
                    <div class="card-body ">
                        <h1><b>S/ {{$paquetes[4]->precio}} </b><small class="text-muted">+ IGV</small></h1>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li class="m-1">Plan {{$paquetes[4]->tipo_periodo}}</li>
                            <li class="m-1">Cantidad de avisos: {{$paquetes[4]->nro_avisos}}</li>
                            <li class="m-1">Visibilidad: {{$paquetes[4]->visibilidad}}</li>
                            <li class="m-1">Interés: {{$paquetes[4]->interes}}</li>
                        </ul> 
                        <br>
                        @auth <!--Pregunto si estoy autenticado me registro y pago--> 
                         <a class="btn btn-lg btn-block bg-gradient-success" href="{{  url('/publicaciones/registro/inmobiliaria/'.$paquetes[4]->slug) }}">Me interesa »</a>
                        @else
                        <a class="btn btn-lg btn-block bg-gradient-success" data-toggle="modal" data-target="#exampleModal" role="button" href="{{$paquetes[4]->slug}}">Me interesa »</a>
                        @endauth
                    </div>
                </div>
            </div>
@endisset
        </div>
    </div>
</div>

<div class="container d-flex justify-content-center mt-3 mb-3">

<div class="row">
    <div class="col-md-6 text-white"><b>Medios de pago disponibles:</b></div>
    <div class="col-md-6">
        <img src="{{ asset('img/banner.png') }}" class="img-fluid elevation-3" alt="Responsive image" width="400" style="border-radius: 15px;">
    </div>

    
</div>

</div>

</section>

<hr>

<div class="container mb-5">
    <div class="row m-5">
        <div class="col-md-12 text-center headline"><h3>¿PORQUÉ ELEGIRNOS?</h3></div>
    </div>
    <div class="row text-center headline">
        <div class="col-md-4 ">
            <div class="col-md-12 m-3"> <i class="fas fa-user-tie fa-5x text-teal"></i></div>
            <div class="col-md-12"> SOMOS UNA EMPRESA PERUANA QUE CONOCE Y SE ADAPTA A TUS NECESIDADES</div>
        </div>
        <div class="col-md-4">
            <div class="col-md-12 m-3"> <i class="fas fa-envelope fa-5x text-teal"></i></div>
            <div class="col-md-12"> TENEMOS ALCANCE A TRAVEZ DEL E-MAIL Y DE TU USUARIO PARA ATENDER A TUS
CLIENTES</div>
        </div>
        <div class="col-md-4">
            <div class="col-md-12 m-3"> <i class="fas fa-adjust fa-5x text-teal"></i></div>
            <div class="col-md-12"> ESTILIZAMOS TUS ANUNCIOS PARA MAYOR ALCANCE Y EFICACIA</div>
        </div>
    </div>

</div>

<script type="text/javascript">
    ScrollReveal().reveal('.headline');
</script>
@endsection