@extends('layouts.app')

@section('content') 
<body class="bg-white">
<br> <br>

@if (session('status'))
<div class="alert  alert-dismissible fade show mt-1 p-3 " style="background: #d4edda; color: #155724" role="alert">
  <strong>Exito! </strong> {{ session('status') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
<ul class="nav nav-tabs border border-success border-top-0 border-left-0  border-right-0  justify-content-center " id="myTab" role="tablist">
  {{--<li class="nav-item " role="presentation">
    <a class="nav-link {{ request()->is('panel/estados') ? 'active':'' }} text-teal" id="aviso-tab" data-toggle="tab" href="#aviso" role="tab" aria-controls="aviso" aria-selected="{{ request()->is('panel/estados') ? true: false }}"><i class="far fa-file"></i> Mis avisos</a>
  </li>
  <li class="nav-item " role="presentation">
    <a class="nav-link {{ request()->is('panel/favoritos') ? 'active':'' }} text-teal" id="favorito-tab" data-toggle="tab" href="#favorito" role="tab" aria-controls="favorito" aria-selected="{{ request()->is('panel/favoritos') ? true: false }}"><i class="far fa-heart"></i> Favoritos</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link {{ request()->is('panel/vistos') ? 'active':'' }} text-teal" id="visto-tab" data-toggle="tab" href="#visto" role="tab" aria-controls="visto" aria-selected="false"><i class="far fa-check-circle"></i> Vistos</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link {{ request()->is('panel/contactados') ? 'active':'' }} text-teal" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"> <i class="far fa-address-card "></i> Contactados</a>
  </li>--}}

<li class="nav-item " role="presentation">
    <a class="nav-link {{ request()->is('panel/estados') ? 'active':'' }} text-teal" id="aviso-tab"  href="{{ url('panel/estados') }}" role="tab" aria-controls="aviso" aria-selected="{{ request()->is('panel/estados') ? true: false }}"><i class="far fa-file"></i> Mis avisos</a>
  </li>
  <li class="nav-item " role="presentation">
    <a class="nav-link {{ request()->is('panel/favoritos') ? 'active':'' }} text-teal" id="favorito-tab" href="{{ url('panel/favoritos') }}" role="tab" aria-controls="favorito" aria-selected="{{ request()->is('panel/favoritos') ? true: false }}"><i class="far fa-heart"></i> Favoritos</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link {{ request()->is('panel/vistos') ? 'active':'' }} text-teal" id="visto-tab" href="{{ url('panel/vistos') }}" role="tab" aria-controls="visto" aria-selected="false"><i class="far fa-check-circle"></i> Vistos</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link {{ request()->is('panel/contactados') ? 'active':'' }} text-teal" id="contact-tab" href="{{ url('panel/contactados') }}" role="tab" aria-controls="contact" aria-selected="false"> <i class="far fa-address-card "></i> Contactados</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
<div class="tab-pane fade {{ request()->is('panel/estados') ? 'show active':'' }}  "  id="aviso" role="tabpanel" aria-labelledby="aviso-tab" >
  @include('partials.estados')
  </div>
  <div class="tab-pane fade {{ request()->is('panel/favoritos') ? 'show active':'' }} "  id="favorito" role="tabpanel" aria-labelledby="favorito-tab" >
  @include('partials.favoritos')
   
  </div>
  <div class="tab-pane fade {{ request()->is('panel/vistos') ? 'show active':'' }} " id="visto" role="tabpanel" aria-labelledby="visto-tab">
    @include('partials.historial')
  </div>
  <div class="tab-pane fade {{ request()->is('panel/contactados') ? 'show active':'' }} " id="contact" role="tabpanel" aria-labelledby="contact-tab">
    @include('partials.contactados')
  </div>
</div>
</body>
@endsection