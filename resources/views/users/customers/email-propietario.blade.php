<!DOCTYPE html>
<html>
<head> 
	<title></title>
</head>
<body>
<div style="text-align: center;">
	<h1 style="background: #d4edda">Peruprop - Solicitud de contacto</h1>
	<h5><b>Mensaje</b>: {{$mensaje}}</h5>

	<h5>Contactarse al siguiente correo electrónico del interesado: {{$email}} </h5>
	<h5>Gracias por confiar en <b > <a href="https://peru-prop.com" style=" text-decoration: none; color:#155724;">Peruprop</a></b> </h5>
	<img src="{{ asset('LOGOpng-PERUPROP-2.PNG') }}" style="width: 200px;height: 200px; display: none;">
</div>
</body>
</html> 