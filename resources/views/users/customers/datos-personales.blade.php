@extends('layouts.app')

@section('content')
 




<section class="content container">
		    <section class="content-header" hidden="">
		      <div class="container-fluid">
		        <div class="row ">
		          <div class="col-sm-6">
		         
		          </div>
		          <div class="col-sm-6">
		            <ol class="breadcrumb float-sm-right">
		              <li class="breadcrumb-item"><a href="{{ url('home')}}">Inicio</a></li>
		              <li class="breadcrumb-item active">Perfil</li>
		            </ol>
		          </div>
		        </div>
		      </div><!-- /.container-fluid -->
		    </section>
	


            <!-- general form elements --> 
            <div class="card card-olive elevation-3 mx-5 mt-5">
              <div class="card-header">
                <h3 class="card-title">Mi perfil</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

           
              	<form role="form" method="POST" action="{{ route('actualizarDatosPersonales') }}" enctype="multipart/form-data">

              	
              	@csrf
                <div class="card-body">
				   <div class="form-row d-flex justify-content-center"> 	
				    <div class="form-group col-md-4">
				      
				      <div class="text-center">
						  <img src="{{ isset($resultadosUser[0]->avatar)?asset($resultadosUser[0]->avatar):'' }}" class="rounded" alt="..." width="300" height="200">
						</div>
				    </div>
				    <div class="form-group col-md-4">
	                    <label for="exampleInputFile">Foto de perfil</label>
	                    <div class="input-group">
	                      <div class="custom-file">
	                        <input type="file" class="custom-file-input" id="exampleInputFile" name="avatar" required="">
	                        <label class="custom-file-label" for="exampleInputFile"  data-browse="Elegir">Elegir foto de perfil</label>
	                      </div>
	                    </div>
				    </div>
				  </div>

				   <div class="form-row"> 	
				    <div class="form-group col-md-4">
				      <label for="inputEmail4">Nombre</label>
				      <input type="text" class="form-control" name="first_name" placeholder="" required="" value="{{ isset($resultadosUser[0]->first_name)?$resultadosUser[0]->first_name:'' }}">
				    </div>
				    <div class="form-group col-md-4">
				      <label for="inputPassword4">Apellido</label>
				      <input type="text" class="form-control" name="last_name" placeholder="" required="" value="{{ isset($resultadosUser[0]->last_name)?$resultadosUser[0]->last_name:'' }}">
				    </div>
				     <div class="form-group col-md-4">
				      <label for="inputEmail4">DNI</label>
				      <input type="text" class="form-control" name="document" placeholder="" required="" value="{{ isset($resultadosUser[0]->document)?$resultadosUser[0]->document:'' }}">
				    </div>
				  </div>
				  <div class="form-row">
				  	@foreach($resultadosUser as $item)
			
				    <div class="form-group col-md-3">
				      <label for="inputEmail4">Usuario</label>
				      <input type="text" class="form-control" name="name" placeholder="" required="" value="{{$item->name}}" disabled="">
				    </div>
				    <div class="form-group col-md-6">
				      <label for="inputEmail4">Email</label>
				      <input type="email" class="form-control" name="email" placeholder="" required="" value="{{$item->email}}" disabled="">
				    </div>
				    @endforeach
				     <div class="form-group col-md-3">
				      <label for="inputZip">Fecha de Nacimiento</label>
				      <input type="date" class="form-control" name="birth_date_user" required="" value="{{ isset($resultadosUser[0]->birth_date_user)?$resultadosUser[0]->birth_date_user:'' }}">
				    </div>
				  </div>
				  <div class="form-row">
				  <div class="form-group col-md-3">
				    <label for="inputAddress">Nacionalidad</label>
				    <input type="text" class="form-control" name="nacionalidad_user" placeholder="" required="" value="{{ isset($resultadosUser[0]->nacionalidad_user)?$resultadosUser[0]->nacionalidad_user:'' }}" >
				  </div>
				   <div class="form-group col-md-3">
				    <label for="inputAddress">Pais</label>
				    <input type="text" class="form-control" name="pais_user" placeholder="" required="" value="{{ isset($resultadosUser[0]->pais_user)?$resultadosUser[0]->pais_user:'' }}">
				  </div>
				  <div class="form-group col-md-3">
				    <label for="inputAddress">Provincia/Ciudad</label>
				    <input type="text" class="form-control" name="provincia_user" placeholder="" required="" value="{{ isset($resultadosUser[0]->provincia_user)?$resultadosUser[0]->provincia_user:'' }}">
				  </div>
	                  <div class="form-group col-md-3">
	                  	<label for="inputState">Direccion</label>
						<input type="text" class="form-control" name="address_user" placeholder="" required="" value="{{ isset($resultadosUser[0]->address_user)?$resultadosUser[0]->address_user:'' }}">
	                  </div>
				  </div>

				  <div class="form-row">
				 <div class="form-group col-md-3">
				      <label for="inputState">Sexo</label>
				      <select name="sexo_user" class="form-control" required="" >
						
					 	<option value="">Elija..</option>
						<option value="Masculino"   @isset($resultadosUser[0]->sexo_user)? @if($resultadosUser[0]->sexo_user == "Masculino") selected @endif @endisset >Masculino</option>
				        <option value="Femenino" @isset($resultadosUser[0]->sexo_user)? @if($resultadosUser[0]->sexo_user == "Femenino") selected @endif @endisset  >Femenino</option>
				        <option value="Otro"  @isset($resultadosUser[0]->sexo_user)? @if($resultadosUser[0]->sexo_user == "Otro") selected @endif @endisset >Otro</option>


				           
				        
				        
				      </select>
				    </div>
				    <div class="form-group col-md-3">
				      <label for="inputState">Teléfono</label>
						<input type="text" class="form-control" name="telephone" placeholder="" required="" value="{{ isset($resultadosUser[0]->telephone)?$resultadosUser[0]->telephone:'' }}">
				    </div>
				    <div class="form-group col-md-6">
				      <label for="inputState">Movil</label>
						<input type="text" class="form-control" name="cellphone" placeholder="" required="" value="{{ isset($resultadosUser[0]->cellphone)?$resultadosUser[0]->cellphone:'' }}">
				    </div>

				  </div>
	         @if (session('mensaje'))
        <div class="alert text-center" role="alert" style="background: #d4edda; color: #155724">
          {{ session('mensaje') }}
        </div>
      @endif
                </div>
                <!-- /.card-body -->
                <div class="card-footer d-flex justify-content-center">

                  <button type="submit" class="btn btn-success">Guardar cambios</button>

                </div>
              </form>
            </div>
            <!-- /.card -->
            <script type="text/javascript">
					            	  // Add the following code if you want the name of the file appear on select
					$(".custom-file-input").on("change", function() {
					  var fileName = $(this).val().split("\\").pop();
					  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
					});

            </script>
</section>








@endsection