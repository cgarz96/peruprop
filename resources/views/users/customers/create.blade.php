@extends('layouts.app')

@section('content')





<div class="container">
        @if (session('mensaje'))
        <div class="alert text-center mt-3" role="alert" style="background: #f8d7da; color: #721c24">
          {{ session('mensaje') }}
        </div>
      @endif

  <div class="alert alert-primary mb-2 mt-5 p-3 text-center" type="button" ><b>¿QUE DESEA PUBLICAR?</b></div>
 






<script type="text/javascript">
$( document ).ready(function() {
    $('#myModal').modal('toggle')
});


function elegirPaquete(visibilidad,prioridad) {
$("#aceptar").removeAttr('disabled');
$("#visibilidad").val(visibilidad);
$("#prioridad").val(prioridad);
}
</script>

<form action="{{ route('solicitarPublicacion') }}" method="POST" enctype="multipart/form-data" id="publicar">




<div class="modal" tabindex="-1" role="dialog" id="myModal" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Paquetes Activos</h5>
      </div>
      <div class="modal-body">
        @if(count($paquetes_user))
        <p>Actualmente tiene los siguientes paquete activos, elija uno:</p>
        @else
        <p>Actualmente no posee paquete activos, puede elegir uno en los siguientes enlaces
<ul>
  <li><a href="{{ url('publicaciones/precios/inmobiliaria') }}">Para inmobiliarias</a>.</li>
  <li><a href="{{ url('publicaciones/precios/anunciante') }}">Para particulares</a>.</li>
</ul>
        </p>
        @endif

 @foreach($paquetes_user as $paquete)         <div class="form-check">
        
  <input class="form-check-input" type="radio" name="paquete_user_id" class="paquete" onchange="elegirPaquete('{{$paquete->visibilidad}}','{{$paquete->prioridad}}' );" value="{{$paquete->paquete_user_id}}">
  <label class="form-check-label" for="exampleRadios1">
    {{$paquete->nombre_paquete}}  
  </label>

</div>  @endforeach
      </div>
      <div class="modal-footer">
        @if(count($paquetes_user))
        <button type="button" class="btn btn-primary" data-dismiss="modal" disabled="" id="aceptar">Aceptar</button>
        @else
        <a href="{{ url('panel/estados') }}" class="btn bg-gradient-primary">Volver</a>  
        @endif
      </div>
    </div>
  </div>
</div>





 <div class="form-row ">

    <div class="input-group mb-3 col-md-12">

      <div class="input-group-prepend">
      <span class="input-group-text" for="inputGroupSelect01">Opciones:</span>
      </div>
      <select class="custom-select " id="tipo_propiedad" required="" name="tipo_propiedad">
           <option value="">--Selecione el tipo de propiedad que desea publicar--</option>
                <option value="departamento">Departamento</option>
                <option value="ph" hidden="">Ph</option>
                <option value="casa">Casa</option>
                <option value="quinta" hidden="">Quinta</option>
                <option value="cochera">Cochera</option>
                <option value="local" hidden="">Local</option>
                <option value="hotel">Hotel</option>
                <option value="terreno">Terreno</option>
                <option value="oficina">Oficina</option>
                <option value="campo" hidden="">Campo</option>
                <option value="fondo" hidden="">Fondo de comercio</option>
                <option value="galpon" hidden="">Galpón</option>
                <option value="negocio" hidden="">Negocio especial</option>
      </select>
    </div>
  </div>

<div class="form-row mt-5">
    @csrf



     <div class="form-group input-group mb-3 col-md-6">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Título:</span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="titulo" required="" id="propiedad" >
    </div>
    <div class="input-group mb-3 col-md-6">
      <div class="input-group-prepend">
        <span class="input-group-text" for="inputGroupSelect01">Opciones:</span>
      </div>
      <select class="custom-select " id="inputGroupSelect01" name="tipo_operacion" required="">
           <option value="">--Selecione el tipo de operacion que desea realizar--</option>
                <option value="venta">Venta</option>
                <option value="alquiler">Alquiler</option>
                <option value="alquilerXtemporada">Alquiler por temporada</option>
                <option value="tiempoCompartido">Tiempo compartido</option>
      </select>
    </div>
</div>


  <div class="form-row">
 
    <div class="form-group input-group mb-3 col-md-6">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroup-sizing-default">Precio: </span>
    </div>
      <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="precio" required="" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$" title="Información: No se admite mas de un punto">
    </div>
    </div>
    <div class="input-group mb-3">
    <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-default">Descripción:</span>
        </div>
      <textarea class="form-control" aria-label="With textarea" rows="4" name="descripcion" required=""></textarea>
    </div>



<div id="campoFaltantes"></div>

 

    <span class="input-group-text" id="inputGroup-sizing-default">Defina la ubicacion de la propiedad en el mapa</span>
    <div id="map" class="mb-3" style='width: 100%; height: 30em;'></div>

 <div class="form-row">
      <div class="form-group input-group mb-3 col-md-6 ">
         <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-default">Dirección</span>

        </div>
        <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="direccion" required="" placeholder="Por ejemplo: Avda. Grau, 425">
      </div>
         <div class="form-group input-group mb-3  col-md-3">
         <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-default">Departamento</span>

        </div>
       <select class="custom-select " id="departamento" required="">
          <option value=""></option>
          @foreach($departamentos as $departamento)
          <option value="{{$departamento->id}}"> {{$departamento->nombre_depto}}</option>
          @endforeach
      </select>
      </div>

         <div class="form-group input-group mb-3  col-md-3">
         <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-default">Provincia</span>

        </div>
       <select class="custom-select "  name="provincia" required="" id="provincia">
       <option value=""></option>
      </select>
      </div>


</div>

      <div class="form-row">
      <div class="input-group mb-3 col-md-6">
     
   <div class="input-group-prepend">
        <button class="btn bg-gradient-olive " type="button">Fotos</button>
      </div>
      <div class="custom-file">
        <input type="file" class="custom-file-input" accept="image/png, image/jpeg" name="foto[]"  multiple="" required="" id="file">
        <label class="custom-file-label" for="inputGroupFile01" data-browse="Elegir">Elige las fotos</label>
      </div>

  {{-- <div class="form-group">
    <label for="exampleFormControlFile1">Fotos</label>
    <input type="file" class="form-control-file" id="exampleFormControlFile1" accept="image/png, image/jpeg" name="foto[]"  multiple="" required="">
  </div>--}}

    </div>
      <div class="form-group input-group mb-3 col-md-4">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-default" >Superficie</span>
        </div>
          <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="superficie" required="" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$" title="Información: No se admite mas de un punto, ni signos">
        </div>
       <div class="form-group input-group mb-3 col-md-2">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-default">Antiguedad</span>
        </div>
          <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" name="antiguedad" required="" pattern="[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$" title="Información: No se admite mas de un punto, ni signos">
        </div>
      </div>

      @include('partials.formOpciones')

      <div id="message"></div>
      <input type="hidden" id="lng" name="longitud" value="-77.03649841635975" >
         <input type="hidden" id="lat" name="latitud" value="-12.061789400008294">
         <input type="hidden" name="departamento" id="idepartamento">
         <input type="hidden" name="visibilidad" id="visibilidad">
         <input type="hidden" name="prioridad_visibilidad" id="prioridad">
 <button class="btn bg-gradient-success btn-lg btn-block mb-5"   >Publicar</button>
</form>






<hr>
 

  


<script type="text/javascript">
/*  $custom-file-text: (
  en: "Browse",
  es: "Elegir"
);*/



function agregarAmbiente(id) {
    $.ajax({
    url: "{{ route('agregarAmbiente') }}",
    data:{ambiente:id, _token: "{{ csrf_token() }}"},
    type: "POST",
    success: function(response){
  // mensaje(response);

      },
       error : function(message) {
         console.log(message);
      }
    });
}

function agregarInstalacion(id) {

  $.ajax({
    url: "{{ route('agregarInstalacion') }}",
    data:{instalacion:id, _token: "{{ csrf_token() }}"},
    type: "POST",
    success: function(response){
    //mensaje(response);     
      },
       error : function(message) {
         console.log(message);
      }
    });

}
 
function agregarServicio(id) {
   $.ajax({
    url: "{{ route('agregarServicio') }}",
    data:{servicio:id, _token: "{{ csrf_token() }}"},
    type: "POST",
    success: function(response){
  //mensaje(response);
      },
       error : function(message) {
         console.log(message);
      }
    });
}







$( "#tipo_propiedad" ).change(function() {
  var valor=this.value;
$.ajax({
    url: "{{ route ('camposEspecificos')  }}",
    data:{propiedad:valor, _token: "{{ csrf_token() }}"},
    type: "POST",
    success: function(response){
    //console.log( response)

    $("#campoFaltantes").html(response)

       

      },
       error : function(message) {
         console.log(message);
      }
    });



});




/**************Añardir a la session**********************/

function mensaje(respuestaMensaje) {
  var mensaje=` <div class="alert alert-success mt-5 mb-5 mensaje">
       ${respuestaMensaje}!
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>`;

    $("#message").html(mensaje);

      setTimeout(function() {
        $(".mensaje").fadeOut(1500);
    },3000);
}





$('#departamento').change(function(e){


$("#idepartamento").val(this.options[this.selectedIndex].text);

$.ajax({
    url: "{{ route('provincias') }}",
    data:{id:this.value, _token: "{{ csrf_token() }}"},
    type: "POST",
    success: function(response){
    
        $("#provincia").html("");
        var provincias="";

        for (var i = 0; i < response.provincias.length; i++) {
        provincias+=`<option value="${response.provincias[i].nombre_prov}">${response.provincias[i].nombre_prov}</option>`

        }

        $("#provincia").html(provincias);

      },
       error : function(message) {
         console.log(message);
      }
    });


});


  // Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});



/**************MAPA*********************/

mapboxgl.accessToken = 'pk.eyJ1IjoiZmFraHJhd3kiLCJhIjoiY2pscWs4OTNrMmd5ZTNra21iZmRvdTFkOCJ9.15TZ2NtGk_AtUvLd27-8xA';

var map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/streets-v11',
center: [-77.03649841635975, -12.061789400008294],
zoom: 10
});
 
var marker = new mapboxgl.Marker({
draggable: true,
color:'green'
})
.setLngLat([-77.03649841635975, -12.061789400008294])
.addTo(map);
 
function onDragEnd() {
var lngLat = marker.getLngLat();

 document.getElementById("lat").value = lngLat.lat;
 document.getElementById("lng").value = lngLat.lng;
//console.log(lngLat.lng + '//'+ lngLat.lat)

}
 
marker.on('dragend', onDragEnd);
</script>


</div>
@endsection

