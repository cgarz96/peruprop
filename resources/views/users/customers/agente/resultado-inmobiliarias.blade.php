@extends('layouts.app')

@section('content')
<div class="container">
<div class="card card-solid mt-5 " style="min-height: 500px">
     <div class="card-body pb-0">

		<div class="row d-flex align-items-stretch">
			@foreach($Inmobiliarias as $inmobiliaria)  
      @if($inmobiliaria->first_name!=NULL) 
            <div class="col-12 col-sm-6 col-md-4  " >
            	
              <div class="card bg-light elevation-1" style="min-height: 250px; min-width: 200px">
                <div class="card-header text-muted border-bottom-0">
                  Inmobiliaria
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-7">
                      <h2 class="lead"><b>{{$inmobiliaria->first_name}} {{$inmobiliaria->last_name}} </b></h2>
                      <p class="text-muted text-sm" >Encuentra las empresas inmobiliarias,constructoras y/o agentes especializados en el mercado inmobiliaria en <b>Peruprop</b> </p>
                      <ul class="ml-4 mb-0 fa-ul text-muted">
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Direccion: {{$inmobiliaria->pais_user}}, {{$inmobiliaria->provincia_user}}, {{$inmobiliaria->address_user}}</li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Teléfono: {{$inmobiliaria->telephone}}</li>
                      </ul>
                    </div>
                    <div class="col-5 text-center">
                      @if($inmobiliaria->avatar=='' || $inmobiliaria->avatar==NULL)
                      <i class="fas fa-user-circle fa-7x"></i>
                      @else
                      <img src="{{ asset('storage/'.$inmobiliaria->avatar) }}" alt="" class="img-circle img-fluid" style="height: 120px">
                      @endif
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="https://api.whatsapp.com/send?phone={{$inmobiliaria->cellphone}}&text=Hola como estas" class="btn btn-sm bg-gradient-olive">
                      <i class="fab fa-whatsapp"></i> Contactar
                    </a>
                  </div>
                </div>
              </div>
              
            </div>
           @endif 
        @endforeach   </div>
            </div>

                   <!-- /.card-body -->
        <div class="card-footer">
          <nav aria-label="Contacts Page Navigation">
          	<div class="pagination justify-content-center m-0">
          	{{ $Inmobiliarias->appends(Request::all())->links() }}
          	</div>
          </nav>
        </div>
        <!-- /.card-footer -->
           </div>

</div>
@endsection