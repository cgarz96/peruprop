@extends('layouts.app')


@section('content')
<div class="container">
<div class="jumbotron mt-5 ">
  <h1 class="display-3">Ops! :(</h1>
  <p class="lead">Mercado pago no pudo procesar tu pago, por favor haga click en "Volver a intentarlo"</p>
  <hr class="my-4">
  <div class="lead d-flex justify-content-end">
    <a class="btn bg-gradient-success btn-lg" href="{{ url('/publicaciones') }}" role="button">Volver a intentarlo</a>
  </div>
</div>
</div>
 
@endsection 