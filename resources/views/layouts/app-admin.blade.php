<!DOCTYPE html>
<html >
@include('components.head')
<body class="hold-transition sidebar-mini layout-navbar-fixed" >
<!-- Site wrapper -->
<div class="wrapper">
@include('components.navbar-admin')
<!-- /.navbar -->

<!---Sidebar-->
@include('components.sidebar-admin')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    @yield('content')

  </div>
  <!-- /.content-wrapper -->

<!--footer-->
@include('components.footer-admin')
  <!-- Control Sidebar -->
</div>
<!-- ./wrapper -->

</body>
</html>