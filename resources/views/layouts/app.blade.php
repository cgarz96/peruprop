<!doctype html> 
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
 <!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@include('components.head')
<body style="background-color: #f5f5f5;">
   <div id="app">
@include('components.navbar')

<main class="fijo" style="min-height: 600px"> <!-- detecta la minima altura -->
    @yield('content')
    </main> 



@include('components.footer')



@include('partials.modals')

<script>/*var token="{{ csrf_token() }}";
const redirect= "{{ url('/admin') }}";*/</script>

<script src="{{ asset('js/login-register.js') }}"></script>








   </div>
</body>
</html>
