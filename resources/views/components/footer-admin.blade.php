<footer class="main-footer">
    <!-- To the right -->
    <div class="d-flex justify-content-center">  <img src="{{ asset('img/LOGO-png--PERUPRO-1.png') }}" width="60" class="brand-image img-circle elevation-3"></div>
    <br>
    <!-- Default to the left -->
     <div class="d-flex justify-content-center"><strong>Copyright &copy; 2020 <a href="https://osunlar.org">Peruprop</a>. </strong> All rights reserved.</div>
  </footer>