<div class="col-md-3 px-4 mt-2 ">
  <form action="{{ route('search') }}" method="get">
 
{{--<div class="card border-success mb-3" style="width: 20em;">--}}
  <div class="card border-success mb-3 elevation-3">
  <div class="card-header"> <b class="ml-auto">Selección actual</b>    </div>
  <div class="card-body">
   <div class="row"> 
   {{--dd(app('request')->request->all())--}} 



   @foreach(app('request')->request->all() as $key => $filtro)
   @if( $key=='page')
    
    @else
    @switch($key)
        @case('bathroom')
        @php $key = 'Baños +'.$filtro; @endphp
        @break
        @case('pricefrom')
        @php $key = 'Precio desde $'.$filtro; @endphp
        @break
        @case('priceup')
        @php $key = 'Precio hasta $'.$filtro; @endphp
        @break
        @case('surfacefrom')
        @php $key = 'Superficie desde '.$filtro; @endphp
        @break
        @case('surfaceup')
        @php $key = 'Superficie hasta '.$filtro; @endphp
        @break
        @case('parking')
        @php $key = 'Estacionamiento +'.$filtro; @endphp
        @break
        @case('antiquity')
        @php $key = 'Antiguedad '.$filtro. 'años'; @endphp
        @break
        @case('bedroom')
        @php $key = 'Dormitorio +'.$filtro; @endphp
        @break
        @case('location')
        @php $key = $filtro; @endphp
        @break
        @case('address')
        @php $key = $filtro; @endphp
        @break
        @case('owner')
        @php $key = $filtro; @endphp
        @break
        @case('orderprice')
        @php $key = $filtro; @endphp
        @break
        @case('property')
        @php $key = $filtro; @endphp
        @break
        @case('operation')
        @php $key = $filtro; @endphp
        @break
      @endswitch
<span class="badge badge-pill badge-secondary ml-2 mb-1">{{$key}} <i style="display: none;" class="fas fa-times-circle"></i></span>
    @endif
    @endforeach
   
    </div>
      <div class="text-center mt-2"><a href="{{ url('search')}}"><i class="fas fa-trash "></i> Limpiar filtros</a> </div>
  </div>
</div>


{{--<div class="card border-success mb-3" style="width: 20em;">--}}
  <div class="card border-success mb-3 elevation-3" >
  <div class="card-header"> <b class="ml-auto mr-auto"> <i class="fas fa-filter"></i> Filtrar</b> </div>



  <nav class="navbar navbar-light bg-light py-1 px-1" >Ubicación
    <button  class="navbar-toggler  ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentUbicacion" aria-controls="navbarToggleExternalContentUbicacion" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentUbicacion">
    @isset($_GET['location'])
  {{--<input type="checkbox" name="location" id="location" class="hidden" value="{{Request::get('location')}}" checked="true">
 <div class="row mb-2 mt-2 mx-1"> 
    <div class="col-md-9 ">
    <input type="text"  class="form-control form-control-sm" id="location-text"  value="{{Request::get('location')}}" autocomplete="off">
    </div>
    <div class="col-md-3 ">
    <button class="btn btn-outline-success btn-sm col-sm-8" id="filtro-ubicacion"><i class="fas fa-plus"></i></button></div>
    </div>--}}

<input type="checkbox" name="location" id="location" class="hidden" value="{{Request::get('location')}}" checked="true">
<div class="row mb-2 mt-2 mx-1">
  <div class="col-md-9">
                <div class="form-group">
                  <select class="form-control form-control-sm select2 " data-placeholder="Escribe el depto" id="location-text" >
                  <option value="{{Request::get('location')}}">{{Request::get('location')}}</option>
                    @foreach($DepartamentosProvincias as $item)
                    <option value="{{$item->nombre_depto}}, {{$item->nombre_prov}}">{{$item->nombre_depto}}, {{$item->nombre_prov}}</option>
                    @endforeach      
                  </select>
                </div>
          </div>
         <div class="col-md-3 ">
    <button class="btn btn-outline-success btn-sm col-sm-8" id="filtro-ubicacion"><i class="fas fa-plus"></i></button></div>  
</div>

  @else

{{--
 <input type="checkbox" name="location" id="location" class="hidden">
 <div class="row mb-2 mt-2 mx-1"> 
    <div class="col-md-9 ml-2">
    <input type="text"  class="form-control" id="location-text" style="height:5px" autocomplete="off">
    </div>
     <div class="col-md-3 ">
    <button class="btn btn-outline-success btn-sm col-sm-12" id="filtro-ubicacion"><i class="fas fa-plus"></i></button></div>
    </div>
--}}

<input type="checkbox" name="location" id="location" class="hidden">
<div class="row mb-2 mt-2 mx-1">
  <div class="col-md-9">
                <div class="form-group">
                  <select class="form-control form-control-sm select2 " data-placeholder="Escribe el depto" id="location-text" >
                  <option></option>
                    @foreach($DepartamentosProvincias as $item)
                    <option value="{{$item->nombre_depto}}, {{$item->nombre_prov}}">{{$item->nombre_depto}}, {{$item->nombre_prov}}</option>
                    @endforeach      
                  </select>
                </div>
          </div>
         <div class="col-md-3 ">
    <button class="btn btn-outline-success btn-sm col-sm-8" id="filtro-ubicacion"><i class="fas fa-plus"></i></button></div>  
</div>




   @endisset


  </div>


  <nav class="navbar navbar-light bg-light py-1 px-1" >¿Qué desea hacer?
    <button  class="navbar-toggler  ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentUOperacion" aria-controls="navbarToggleExternalContentUOperacion" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentUOperacion">
    @isset($_GET['operation'])


 <fieldset class="form-group ml-2 mt-1">
      <div class="form-check icheck-success">
        
          <input type="radio" class="form-check-input text-success" name="operation"  value="venta" id="op-venta" >
          <label class="form-check-label" for="op-venta">Venta
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="operation"  value="alquiler" id="op-alquiler">
          <label class="form-check-label" for="op-alquiler">Alquiler
        </label>
      </div>
      <div class="form-check icheck-success">
          <input type="radio" class="form-check-input" name="operation"  value="alaquilertemporada" id="op-alaquilertemporada" >
          <label class="form-check-label" for="op-alaquilertemporada">Alquiler por temporada
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="operation"  value="tiempocompartido" id="op-tiempocompartido" >
          <label class="form-check-label" for="op-tiempocompartido">Tiempo compartido
        </label>
      </div>
    </fieldset>
  <script> $("#op-{{Request::get('operation')}}").attr('checked',true);</script>


  @else


 <fieldset class="form-group ml-2 mt-1">
      <div class="form-check icheck-success">
        
          <input type="radio" class="form-check-input text-success" name="operation"  value="venta"  id="op-venta" >
          <label class="form-check-label" for="op-venta">Venta
        </label>
      </div>
      <div class="form-check icheck-success">
          <input type="radio" class="form-check-input" name="operation"  value="alquiler" id="op-alquiler" >
          <label class="form-check-label" for="op-alquiler">Alquiler
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="operation"  value="alaquilertemporada" id="op-alaquilertemporada"  >
          <label class="form-check-label" for="op-alaquilertemporada">Alquiler por temporada
        </label>
      </div>
      <div class="form-check icheck-success">
          <input type="radio" class="form-check-input" name="operation"  value="tiempocompartido" id="op-tiempocompartido" > 
      <label class="form-check-label" for="op-tiempocompartido">Tiempo compartido
        </label>
      </div>
    </fieldset>

   @endisset


  </div>




    <nav class="navbar navbar-light bg-light py-1 px-1" >Tipo de propiedad
    <button  class="navbar-toggler  ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentPropiedad" aria-controls="navbarToggleExternalContentPropiedad" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentPropiedad">
    @isset($_GET['property'])

 <fieldset class="form-group ml-2 mt-1 ">
      <div class="form-check radio-item icheck-success" hidden="">
        
          <input type="radio" class="form-check-input text-success" name="property"  value="campo" id="p-campo" >
        <label for="p-campo" class="form-check-label">Campo</label>
      </div>
      <div class="form-check icheck-success">
      <input type="radio" class="form-check-input" name="property"  value="casa" id="p-casa">
      <label class="form-check-label" for="p-casa">Casa</label>
      </div>
      <div class="form-check icheck-success">
      <input type="radio" class="form-check-input" name="property"  value="cochera" id="p-cochera" >
      <label class="form-check-label" for="p-cochera">Cochera</label>
      </div>
      <div class="form-check icheck-success">
      <input type="radio" class="form-check-input" name="property"  value="departamento" id="p-departamento" >
      <label class="form-check-label" for="p-departamento">Departamento</label>
      </div>
      <div class="form-check" hidden="">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="property"  value="fondo" id="p-fondo" >
          Fondo de comercio
        </label>
      </div>
      <div class="form-check" hidden="">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="property"  value="galpon" id="p-galpon" >
          Galpón
        </label>
      </div>
      <div class="form-check" hidden="">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="property"  value="hotel" id="p-hotel" >
          Hotel
        </label>
      </div>
      <div class="form-check icheck-success">
          <input type="radio" class="form-check-input" name="property"  value="local" id="p-local" >
        <label class="form-check-label" for="p-local">Local</label>
      </div>
      <div class="form-check" hidden="">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="property"  value="negocio" id="p-negocio" >
          Negocio especial
        </label>
      </div>
      <div class="form-check icheck-success">
      <input type="radio" class="form-check-input" name="property"  value="oficina" id="p-oficina" >
      <label class="form-check-label" for="p-oficina">Oficina</label>
      </div>
      <div class="form-check" hidden="">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="property"  value="ph" id="p-ph" >
          Ph 
        </label>
      </div>
      <div class="form-check" hidden="">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="property"  value="quinta" id="p-quinta" >
          Quinta
        </label>
      </div>
      <div class="form-check icheck-success">
          <input type="radio" class="form-check-input" name="property"  value="terreno" id="p-terreno" >
           <label class="form-check-label" for="p-terreno">Terreno </label>
      </div>
    </fieldset>
  <script> $("#p-{{Request::get('property')}}").attr('checked',true);</script>


  @else

<fieldset class="form-group ml-2 mt-1">
      <div class="form-check radio-item" hidden="">
        <label class="form-check-label">
          <input type="radio" class="form-check-input text-success" name="campo"  value="venta" id="p-campo" >
          Campo
        </label>
      </div>
      <div class="form-check icheck-success">
      <input type="radio" class="form-check-input" name="property"  value="casa" id="p-casa">
     <label class="form-check-label" for="p-casa">Casa</label>
      </div>
      <div class="form-check icheck-success">
          <input type="radio" class="form-check-input" name="property"  value="cochera" id="p-cochera" >
       <label class="form-check-label" for="p-cochera">Cochera</label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="property"  value="departamento" id="p-departamento" >
        <label class="form-check-label" for="p-departamento">Departamento
        </label>
      </div>
      <div class="form-check" hidden="">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="property"  value="fondo" id="p-fondo" >
          Fondo de comercio
        </label>
      </div>
      <div class="form-check" hidden="">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="property"  value="galpon" id="p-galpon" >
          Galpón
        </label>
      </div>
      <div class="form-check" hidden="">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="property"  value="hotel" id="p-hotel" >
          Hotel
        </label>
      </div>
      <div class="form-check icheck-success">
          <input type="radio" class="form-check-input" name="property"  value="local" id="p-local" >
          <label class="form-check-label" for="p-local">Local
        </label>
      </div>
      <div class="form-check" hidden="">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="property"  value="negocio" id="p-negocio" >
          Negocio especial
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="property"  value="oficina" id="p-oficina" >
          <label class="form-check-label" for="p-oficina">Oficina
        </label>
      </div>
      <div class="form-check" hidden="">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="property"  value="ph" id="p-ph" >
          Ph 
        </label>
      </div>
      <div class="form-check" hidden="">
      <label class="form-check-label">
          <input type="radio" class="form-check-input" name="property"  value="quinta" id="p-quinta" >
          Quinta
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="property"  value="terreno" id="p-terreno" >
          <label class="form-check-label" for="p-terreno">Terreno 
        </label>
      </div>
    </fieldset>
   @endisset


  </div>


  <nav class="navbar navbar-light bg-light py-1 px-1" >Dirección
    <button  class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentDireccion" aria-controls="navbarToggleExternalContentDireccion" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentDireccion">
    {{--dd(Request::get('address'))--}}
    @isset($_GET['address'])
  
  <input type="checkbox" name="address" id="address" value="{{Request::get('address')}}" class="hidden" checked="true" >
  <div class="row mb-2 mt-2"> 
    <div class="col-md-9 mx-1">
    <input type="text"  class="form-control form-control-sm" id="address-text" value="{{Request::get('address')}}" autocomplete="off"> 
    </div>
    <div class="col-md-3">
    <button class="btn btn-outline-success btn-sm col-sm-8" id="filtro-direccion"><i class="fas fa-plus"></i></button></div>
    </div>

    @else
    <input type="checkbox" name="address" id="address" class="hidden" >
 <div class="row mb-2 mt-2 mx-1"> 
    <div class="col-md-9 ">
    <input type="text"  class="form-control form-control-sm" id="address-text"   autocomplete="off"> 
    </div>
    <div class="col-md-3">
    <button class="btn btn-outline-success btn-sm col-sm-8" id="filtro-direccion"><i class="fas fa-plus"></i></button></div>
    </div>

 @endisset



  </div>



    <nav class="navbar navbar-light bg-light py-1 px-1" >Precio
    <button  class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentPrecio" aria-controls="navbarToggleExternalContentPrecio" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentPrecio">
    @isset($_GET['orderprice'])
 @if($_GET['orderprice'])
<fieldset class="form-group ml-2 mt-1">
      <div class="form-check icheck-success">
        
          <input type="radio" class="form-check-input text-success" name="orderprice"  value="mayor-precio" id="o-mayor-precio" >
          
     <label class="form-check-label" for="o-mayor-precio"> Mayor precio  </label>
      </div>
      <div class="form-check icheck-success">
    
          <input type="radio" class="form-check-input" name="orderprice" value="menor-precio" id="o-menor-precio">
       <label class="form-check-label" for="o-menor-precio">Menor precio
        </label>
      </div>
</fieldset>
 <script> $("#o-{{Request::get('orderprice')}}").attr('checked',true);</script>
  @endif
 @else

 <fieldset class="form-group ml-2 mt-1">

        <div class="form-check icheck-success">
          <input type="radio" class="form-check-input" name="orderprice"  value="mayor-precio" id="mayor-precio-check" >
        <label class="form-check-label" for="mayor-precio-check">Mayor precio
        </label>

      </div>

              <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="orderprice"  value="menor-precio" id="menor-precio-check" >
        <label class="form-check-label" for="menor-precio-check">Menor precio
        </label>
      </div>
      
</fieldset>

@endisset
 @isset($_GET['pricefrom'],$_GET['priceup'])
@if($_GET['pricefrom'] && $_GET['priceup'])

 <div class="row mb-2 mt-2 mx-1"> 
   

    <div class="col-md-4">
     <input type="checkbox" name="pricefrom" id="pricefrom" class="hidden" checked="true" value="{{Request::get('pricefrom')}}">  
    <input type="number"  class="form-control form-control-sm" placeholder="S/ Desde" id="pricefrom-text" value="{{Request::get('pricefrom')}}"  autocomplete="off">
    </div>

    <div class="col-md-4">
    <input type="checkbox" name="priceup" id="priceup" class="hidden" checked="true" value="{{Request::get('priceup')}}" > 
    <input type="number"  class="form-control form-control-sm" placeholder="S/ Hasta" id="priceup-text" value="{{Request::get('priceup')}}"  autocomplete="off">
    </div>
<div class="col-md-4">
    <button class="btn btn-outline-success btn-sm col-sm-8" id="filtro-precio"><i class="fas fa-check"></i></button>
       </div>

    </div>

     <script> 
     /* var pricefrom=$("#pricefrom-text").val();
      var priceup=$("#priceup-text").val();
     $("#p-{{Request::get('orderprice')}}").attr('value',pricefrom);
     $("#p-{{Request::get('orderprice')}}").attr('value',priceup);*/
   </script>
  @endif
 @else


 <div class="row mb-2 mt-2 mx-1"> 
  <input type="checkbox" name="pricefrom" id="pricefrom" class="hidden" >
<input type="checkbox" name="priceup" id="priceup" class="hidden">
    <div class="col-md-4 ">
    <input type="number"  class="form-control form-control-sm" placeholder="S/ Desde" id="pricefrom-text"  autocomplete="off">
    </div>
    <div class="col-md-4 ">
    <input type="number"  class="form-control form-control-sm" placeholder="S/ Hasta" id="priceup-text"  autocomplete="off">
    </div>
    <div class="col-md-4">
    <button class="btn btn-outline-success btn-sm col-sm-8" id="filtro-precio"><i class="fas fa-check"></i></button></div>
    </div>

@endisset







  </div>


    <nav class="navbar navbar-light bg-light py-1 px-1" >Dormitorios
    <button  class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentDormitorio" aria-controls="navbarToggleExternalContentDormitorio" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentDormitorio">
   @isset($_GET['bedroom'])
 <div class="row mb-2 mt-2"> 
  <input type="checkbox" name="bedroom" id="bedroom" class="hidden" checked="true" value="{{Request::get('bedroom')}}">
  @if($_GET['bedroom'])
   <div class="col-md-2 ml-2">    
  <button class="btn btn-outline-success btn-sm filtro-dormitorio col-sm-12" value="1" id="d-1">+1</button></div>
  <div class="col-md-2 ml-2">        
  <button class="btn btn-outline-success btn-sm filtro-dormitorio col-sm-12" value="2" id="d-2">+2</button></div>
  <div class="col-md-2 ml-2">
  <button class="btn btn-outline-success btn-sm filtro-dormitorio col-sm-12" value="3" id="d-3">+3</button></div>
  <div class="col-md-2 ml-2">
  <button class="btn btn-outline-success btn-sm filtro-dormitorio col-sm-12" value="4" id="d-4">+4</button></div>
  <div class="col-md-2 ml-2">
  <button class="btn btn-outline-success btn-sm filtro-dormitorio col-sm-12" value="5" id="d-5">+5</button></div>
   <script> $("#d-{{Request::get('bedroom')}}").addClass('active');</script>
    @endif
    </div>
    @else


<div class="row mb-2 mt-2  "> 
  <input type="checkbox" name="bedroom" id="bedroom" class="hidden">
   <div class="col-md-2 ml-2">    
  <button class="btn btn-outline-success btn-sm filtro-dormitorio col-sm-12" value="1" >+1</button></div>
  <div class="col-md-2 ml-2">        
  <button class="btn btn-outline-success btn-sm filtro-dormitorio col-sm-12" value="2">+2</button></div>
  <div class="col-md-2 ml-2">
  <button class="btn btn-outline-success btn-sm filtro-dormitorio col-sm-12" value="3" >+3</button></div>
  <div class="col-md-2 ml-2">
  <button class="btn btn-outline-success btn-sm filtro-dormitorio col-sm-12" value="4" >+4</button></div>
  <div class="col-md-2 ml-2 ">
  <button class="btn btn-outline-success btn-sm filtro-dormitorio col-sm-12" value="5" >+5</button></div>

    </div>

     @endisset

  </div>


      <nav class="navbar navbar-light bg-light py-1 px-1" >Baños
    <button  class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentBaño" aria-controls="navbarToggleExternalContentBaño" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentBaño">
  @isset($_GET['bathroom'])
<input type="checkbox" name="bathroom" id="bathroom" class="hidden" checked="true" value="{{Request::get('bathroom')}}">
 @if($_GET['bathroom'])
 <div class="row mb-2 mt-2"> 
   <div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-banio col-sm-12" value="1" id="b-1">+1</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-banio col-sm-12" value="2" id="b-2">+2</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-banio col-sm-12" value="3" id="b-3">+3</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-banio col-sm-12" value="4" id="b-4">+4</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-banio col-sm-12" value="5" id="b-5">+5</button></div>

    </div>

       <script> $("#b-{{Request::get('bathroom')}}").addClass('active');</script>
    @endif





  @else

<input type="checkbox" name="bathroom" id="bathroom" class="hidden">
 <div class="row mb-2 mt-2"> 
   <div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-banio col-sm-12" value="1" >+1</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-banio col-sm-12" value="2" >+2</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-banio col-sm-12" value="3" >+3</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-banio col-sm-12" value="4" >+4</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-banio col-sm-12" value="5" >+5</button></div>

    </div>

    @endisset

  </div>


      <nav class="navbar navbar-light bg-light py-1 px-1" >Cocheras
    <button  class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentEstacionamiento" aria-controls="navbarToggleExternalContentEstacionamiento" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentEstacionamiento">
    @isset($_GET['garage'])
<input type="checkbox" name="garage" id="parking" class="hidden" checked="true" value="{{Request::get('garage')}}">
 @if($_GET['garage'])
 <div class="row mb-2 mt-2"> 
   <div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-estacionamiento col-sm-12" value="1" id="p-1">+1</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-estacionamiento col-sm-12" value="2" id="p-2">+2</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-estacionamiento col-sm-12" value="3" id="p-3">+3</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-estacionamiento col-sm-12" value="4" id="p-4">+4</button></div>
<div class="col-md-2 ml-2 ">
<button class="btn btn-outline-success btn-sm filtro-estacionamiento col-sm-12" value="5" id="p-5">+5</button></div>

    </div>

         <script> $("#p-{{Request::get('garage')}}").addClass('active');</script>
    @endif





  @else



    <input type="checkbox" name="garage" id="parking" class="hidden">
 <div class="row mb-2 mt-2"> 
   <div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-estacionamiento col-sm-12" value="1">+1</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-estacionamiento col-sm-12" value="2">+2</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-estacionamiento col-sm-12" value="3">+3</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-estacionamiento col-sm-12" value="4">+4</button></div>
<div class="col-md-2 ml-2">
<button class="btn btn-outline-success btn-sm filtro-estacionamiento col-sm-12" value="5">+5</button></div>

    </div>
       @endisset

  </div>


        <nav class="navbar navbar-light bg-light py-1 px-1" >Antigüedad
    <button  class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentantiguedad" aria-controls="navbarToggleExternalContentantiguedad" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentantiguedad">
@isset($_GET['antiquity'])
 @if($_GET['antiquity'])
 <fieldset class="form-group ml-2 mt-1">
      <div class="form-check icheck-success">
        
          <input type="radio" class="form-check-input text-success" name="antiquity"  value="0" id="a-0" >
          <label class="form-check-label" for="a-0">A estrenar
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="antiquity"  value="5" id="a-5">
          <label class="form-check-label" for="a-5">Hasta 5 años
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="antiquity"  value="10" id="a-10" >
          <label class="form-check-label" for="a-10">Hasta 10 años
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="antiquity"  value="20" id="a-20" >
          <label class="form-check-label" for="a-20">Hasta 20 años
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="antiquity"  value="20mas" id="a-20mas" >
          <label class="form-check-label" for="a-20mas">Más de 20 años
        </label>
      </div>
    </fieldset>
  <script> $("#a-{{Request::get('antiquity')}}").attr('checked',true);</script>
    @endif





  @else





<fieldset class="form-group ml-2 mt-1">
      <div class="form-check icheck-success">
        
          <input type="radio" class="form-check-input text-success" name="antiquity"  value="0"  id="a-0">
          <label class="form-check-label" for="a-0">A estrenar
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="antiquity"  value="5" id="a-5">
          <label class="form-check-label" for="a-5">Hasta 5 años
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="antiquity"  value="10" id="a-10" >
          <label class="form-check-label" for="a-10">Hasta 10 años
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="antiquity"  value="20" id="a-20" >
          <label class="form-check-label" for="a-20">Hasta 20 años
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="antiquity"  value="20mas" id="a-20mas" >
          <label class="form-check-label" for="a-20mas">Más de 20 años
        </label>
      </div>
    </fieldset>

  @endisset

  </div>





    <nav class="navbar navbar-light bg-light py-1 px-1" >Superficie
    <button  class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentSuperficie" aria-controls="navbarToggleExternalContentSuperficie" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentSuperficie">
 @isset($_GET['surfacefrom'],$_GET['surfaceup'])
 @if($_GET['surfacefrom'] && $_GET['surfaceup'])
    <input type="checkbox" name="surfacefrom" id="surfacefrom" class="hidden" checked="true" value="{{Request::get('surfacefrom')}}">
<input type="checkbox" name="surfaceup" id="surfaceup" class="hidden" checked="true" value="{{Request::get('surfaceup')}}">
<span class="ml-2">Total</span>
 <div class="row mb-2 mt-2 mx-1"> 
    <div class="col-md-4 ">
    <input type="number"  class="form-control form-control-sm" placeholder="Desde" id="surfacefrom-text" value="{{Request::get('surfacefrom')}}">
    </div>
    <div class="col-md-4 ">
    <input type="number"  class="form-control form-control-sm" placeholder="Hasta" id="surfaceup-text" value="{{Request::get('surfaceup')}}">
    </div>
    <div class="col-md-4">
    <button class="btn btn-outline-success btn-sm col-sm-8" id="filtro-superficie"><i class="fas fa-check"></i></button>  </div>
    </div>

 @endif
 @else


        <input type="checkbox" name="surfacefrom" id="surfacefrom" class="hidden" >
<input type="checkbox" name="surfaceup" id="surfaceup" class="hidden">
<span class="ml-2">Total</span>
 <div class="row mb-2 mt-2 mx-1"> 
    <div class="col-md-4 ">
    <input type="number"  class="form-control form-control-sm" placeholder="Desde" id="surfacefrom-text">
    </div>
    <div class="col-md-4 ">
    <input type="number"  class="form-control form-control-sm" placeholder="Hasta" id="surfaceup-text">
    </div>
    <div class="col-md-4">
    <button class="btn btn-outline-success btn-sm col-sm-8" id="filtro-superficie"><i class="fas fa-check"></i></button></div>
    </div>
@endisset

  </div>


    <nav class="navbar navbar-light bg-light py-1 px-1" >Tipo de anunciante
    <button  class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentTipo" aria-controls="navbarToggleExternalContentTipo" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentTipo">
    @isset($_GET['owner'])
 @if($_GET['owner'])
<fieldset class="form-group ml-2 mt-1">
      <div class="form-check icheck-success">
        
          <input type="radio" class="form-check-input text-success" name="owner"  value="Inmobiliaria" id="o-Inmobiliaria" >
          <label class="form-check-label" for="o-Inmobiliaria">Inmobiliaria
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="owner" value="Anunciante" id="o-Anunciante">
          <label class="form-check-label" for="o-Anunciante">Dueño único
        </label>
      </div>
</fieldset>

  <script> $("#o-{{Request::get('owner')}}").attr('checked',true);</script>
    @endif





  @else




<fieldset class="form-group ml-2 mt-1">
      <div class="form-check icheck-success">
        
          <input type="radio" class="form-check-input text-success" name="owner"  value="Inmobiliaria" id="o-Inmobiliaria" >
          <label class="form-check-label" for="o-Inmobiliaria">Inmobiliaria
        </label>
      </div>
      <div class="form-check icheck-success">
      
          <input type="radio" class="form-check-input" name="owner" value="Anunciante" id="o-Anunciante">
          <label class="form-check-label" for="o-Anunciante">Dueño único
        </label>
      </div>
</fieldset>
  @endisset

  </div>


{{--


    <nav class="navbar navbar-light bg-light py-1 px-1" >Ambientes
    <button  class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentAmbiente" aria-controls="navbarToggleExternalContentAmbiente" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentAmbiente">
<fieldset class="form-group ml-2 mt-1">
      <div class="form-check">
        <label class="form-check-label">
          <input class="form-check-input" type="checkbox" value="" >
          Patio
        </label>
      </div>
      <div class="form-check ">
        <label class="form-check-label">
          <input class="form-check-input" type="checkbox" value="" >
          Hall
        </label>
      </div>
    </fieldset>
  </div>


      <nav class="navbar navbar-light bg-light py-1 px-1" >Servicios
    <button  class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentServicio" aria-controls="navbarToggleExternalContentServicio" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentServicio">
<fieldset class="form-group ml-2 mt-1">
    @foreach($servicios as $servicio)
      <div class="form-check">
        <label class="form-check-label">
          <input class="form-check-input" type="checkbox" name="service" value="{{$servicio->nombre}}" >
          {{$servicio->nombre}}
        </label>
      </div>
      @endforeach
    </fieldset>
  </div>


      <nav class="navbar navbar-light bg-light py-1 px-1" >Instalaciones
    <button  class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContentInstalacion" aria-controls="navbarToggleExternalContentInstalacion" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-caret-down text-success"></i>
    </button>
  </nav>
  <div class="collapse" id="navbarToggleExternalContentInstalacion">
<fieldset class="form-group ml-2 mt-1">
      <div class="form-check">
        <label class="form-check-label">
          <input class="form-check-input" type="checkbox" value="patio" name="instalation[]" >
          Patio
        </label>
      </div>
      <div class="form-check ">
        <label class="form-check-label">
          <input class="form-check-input" type="checkbox" value="hall" name="instalation[]" >
          Hall
        </label>
      </div>
    </fieldset>
  </div>

  --}}
 <button class="btn bg-gradient-success">Aplicar</button>

     
  <!--</div>-->
</div>
</form>
</div>