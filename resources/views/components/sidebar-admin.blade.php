  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-olive elevation-2 ">
    <!-- Brand Logo -->
    <a href="{{ url('admin') }}" class="brand-link bg-white">
    <img src="{{ asset('img/LOGO-png--PERUPRO-1.png') }}" alt="Logo" class="img-size-50 img-circle ">
      <span class="brand-text font-weight-light">Peruprop</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar ">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-4 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('img/admin.PNG') }}" class="img-circle" alt="User Image">
        </div>
        <div class="info">
               
            <a href="#" class="d-block">Administrador</a>
         
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ url('admin/requests') }}" class="nav-link {{ request()->is('admin/requests') ? 'bg-olive':''}} ">
              <i class="nav-icon fas fa-arrow-circle-right"></i>
              <p>
                Solicitudes
                <span class="badge badge-info right" hidden="">2</span>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview" hidden="">
            <a href="#" class="nav-link {{-- request()->is('home/patients') ? 'active text-white ':''--}} ">
              <i class=" nav-icon fas fa-hospital-user "></i>
              <p>
                Inmobiliarias
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('admin/real-states') }}" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Administrar Inmobiliarias</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link {{-- request()->is('home/patients') ? 'active text-white ':''--}} ">
              <i class="nav-icon fas fa-user-tie"></i>
              <p>
                Anunciantes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('admin/advertisers/agents') }}" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Administrar Agentes</p>
                </a>
              </li>
              <li class="nav-item">
              <a href="{{ url('admin/advertisers/real-states') }}" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Administrar Inmobiliarias</p>
                </a>
              </li>
              <li class="nav-item">
              <a href="{{ url('admin/advertisers/particulars') }}" class="nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Administrar Particulares</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview" hidden="">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
              <p>
                Correo
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Recibidos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../mailbox/compose.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Escribir</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item" >
            <a href="{{ url('admin/publications') }}" class="nav-link">
            <i class="nav-icon fas fa-house-user"></i>
              <p>
                Publicaciones
              </p>
            </a>
          </li>
          <li class="nav-item" >
            <a href="{{ url('admin/properties') }}" class="nav-link">
              <i class="nav-icon fab fa-houzz"></i>
              <p>
                Propiedades
              </p>
            </a>
          </li>
          <li class="nav-item" hidden="">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users-cog"></i>
              <p>
                Usuarios
                <span class="badge badge-info right" hidden="">2</span>
              </p>
            </a>
          </li>

          <li class="nav-item" hidden="">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-wallet"></i>
              <p>
                Pagos
                <span class="badge badge-info right" hidden="">2</span>
              </p>
            </a>
          </li>
          <li class="nav-item" hidden="">
            <a href="" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Perfil
                <span class="badge badge-info right" hidden="">2</span>
              </p>
            </a>
          </li>


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>