<nav class="navbar navbar-expand-lg navbar-light bg-white sticky-top elevation-3 text-center pace-success" >
  <div class="container">
     <a class="navbar-brand mr-3" style="padding: 0px; margin: 0px" href="{{ url('/')}}"><img src="{{  asset('img/LOGO-NAVBAR.png') }}"  class="d-inline-block align-top" alt="" style="padding: 0px; margin: 0px;  height:50px;width: 65px ;"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
{{-- width="70" height="50"--}}
    @isset($habilitarBuscadorNav)
          <!-- SEARCH FORM -->
        <form class="form-inline " method="get" action="{{ route('search') }}">
          <div class="input-group input-group">
          <select class="selectpicker" data-live-search="true" data-size="5" title="Buscar por ubicación" name="location">
                            @foreach($DepartamentosProvincias as $item)
                            <option data-tokens="{{$item->nombre_depto}}, {{$item->nombre_prov}}"
                              value="{{$item->nombre_depto}}, {{$item->nombre_prov}}">{{$item->nombre_depto}}, {{$item->nombre_prov}}</option>
                            @endforeach 
          </select>
         <div class="input-group-append">
              <button class="btn bg-gradient-olive" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form>
     @endisset



<style type="text/css">
  
  /* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {

.link-style{
  background-color: #d4edda !important;
  margin: 0px 0px 3px 0px;
 }
}
}
</style>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">


    <ul class="navbar-nav ml-auto">
      <li class="nav-item {{ request()->is('/') ? 'active border-bottom border-success':'' }} link-style" >
        <a class="nav-link " href="{{  url('/') }}"  >Propiedades </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" style="display: none;">Garantias de Alquiler</a>
      </li>
      <li class="nav-item {{ request()->is('publicaciones') ? 'active border-bottom border-success':'' }} link-style ">
        <a class="nav-link  " href="{{  url('/publicaciones') }}" >Publicar</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="#" style="display: none;" >Noticias</a>
      </li>
   <li class="nav-item">
    @if(mostrarOpcionReducida())

     @else

     @endif 

     @guest{{--Para el caso de contesta que no este auteticado no pregunte por id de user--}}
       <a href="{{ url('listado-agentes') }}" class="nav-link btn bg-gradient-olive btn-sm link-style text-white" ><i class="fas fa-search"></i> Buscar Agentes</a>
      @else
         @if(mostrarBusqueda()['url']=='')
           @else
           <a href="{{ url(mostrarBusqueda()['url']) }}" class="nav-link btn bg-gradient-olive btn-sm  link-style text-white" ><i class="fas fa-search"></i> {{mostrarBusqueda()['title']}}</a>
           @endif
      @endguest 
       </li>
    </ul>

      <ul class="navbar-nav ml-auto">
        @guest
        <button type="button" class="btn bg-gradient-success" data-toggle="modal" data-target="#exampleModal" data-micron="bounce"> Mi cuenta</button>
        @else
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {{ Auth::user()->name }}
        </a>
          <!-- Here's the magic. Add the .animate and .slide-in classes to your .dropdown-menu and you're all set! -->
          <div class="dropdown-menu dropdown-menu-right animate slideIn " aria-labelledby="navbarDropdown">
          
            @if(mostrarOpcionReducida())
            <a class="dropdown-item" href="#" hidden="">Alertas</a>
            <a class="dropdown-item" href="{{ url('panel/favoritos') }}">Favoritos</a>
            <a class="dropdown-item" href="{{ url('panel/vistos') }}">Vistos</a>
            <a class="dropdown-item" href="{{ url('panel/contactados') }}">Contactados</a>
            <div class="dropdown-divider"></div>
            @else
            @endif
            <a class="dropdown-item" href="{{ route('changePassword') }}">Cambiar contraseña</a>
            <a class="dropdown-item" href="{{ url('changeInformation') }}">Datos personales</a>
            <a class="dropdown-item" href="{{ route('changeEmail') }}">Configuración de correo</a>
            <a class="dropdown-item" href="{{ url('panel/estados') }}">Mis avisos</a>
            <a class="dropdown-item" href="{{ url('publicaciones/aviso') }}">Publicá ahora</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Ayuda</a>
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Cerrar sesión</a>
          </div>
        </li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
    @endguest
      </ul>
    </div>
  </div>
</nav> 
