@extends('layouts.app') 

@section('content')

<style type="text/css">

/* Breakpoints */

@media (min-width: 992px){


.altura{
height: 500px !important;
}

  .carousel-content {
     background: rgba(0, 0, 0, 0.59);
        /*background: rgba(26, 70, 104, 0.51);*/
     padding: 30px;
     border-radius: 10px;
     position: absolute;
     top: 30%;
     left: 0;/* centramos el div absoluto*/
     right: 0;
     margin: auto;
     z-index: 1;
     color: white;
     text-shadow: 0 1px 2px rgba(0,0,0,0.6);

}
{{--
.color{color: white !important;}

#valor::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: white !important;
  opacity: 1; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: white;
}

::-ms-input-placeholder { /* Microsoft Edge */
  color: white;
}

--}}
}

@media (max-width: 992px){/* Para Movil*/


  .carousel-content {
     background: rgba(0, 0, 0, 0.59);
        /*background: rgba(26, 70, 104, 0.51);*/
     padding: 30px;
        border-radius: 10px;
     position: absolute;
     top: 30%;
     left: 0;/* centramos el div absoluto*/
     right: 0;
     margin: auto;
     z-index: 1;
     {{-- color: white;--}}
     text-shadow: 0 1px 2px rgba(0,0,0,0.6);

}

    .form-search-responsive{
      display: block !important;
    }

    #form-search{
      display: none !important;
    }

    .altura{
      height: 450px !important;
    }


   {{-- #valor-responsivo::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: white !important;
  opacity: 1; /* Firefox */
}--}}

}
/*
@media (min-width: 992px){
    .search-sec{
        position: relative;
        top: -14px;
        background: rgba(26, 70, 104, 0.51);
    }
}*/
/*
@media (max-width: 992px){
    .search-sec{
        background: #1A4668;
    }
}*/

</style>

{{--fotos a 1330*600--}}
<section>
    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('img/portada1.jpeg') }}" class="d-block w-100 altura" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{ asset('img/portada2.jpeg') }}" class="d-block w-100 altura" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{ asset('img/portada3.jpeg') }}" class="d-block w-100 altura" alt="...">
            </div>

            <div class="carousel-item">
                <img src="{{ asset('img/portada4.jpeg') }}" class="d-block w-100 altura" alt="...">
            </div>
              <div class="carousel-item">
                <img src="{{ asset('img/portada5.jpeg') }}" class="d-block w-100 altura" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev" style="display: none;">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next" style="display: none;">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>

 <script type="text/javascript">
  $('.carousel').carousel({
  interval: 3000
})

 </script>

<!--Formulario responsivo-->
<form  class="form-search-responsive carousel-content" style="display: none;" method="get" action="{{ route('search') }}" id="form-search-responsive" >
  <div id="token-responsivo"></div>
  <div class="form-row">
<div class="input-group m-2 ">
  <select class="custom-select bg-white " style="border-top-left-radius: 5px;border-bottom-left-radius: 5px;" hidden="">
    <option selected>Venta</option>
    <option value="1">Alquiler</option>
    <option value="2">Alquiler por temporada</option>
    <option value="3">Tiempo Compartido</option>
  </select>
   <select class="custom-select bg-white desactivarFiltro-responsivo" style="border-top-left-radius: 5px;border-bottom-left-radius: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;" name="property">
            <option value="departamento">Departamento</option>
            <option value="ph" hidden="">Ph</option>
            <option value="casa">Casa</option>
            <option value="quinta" hidden="">Quinta</option>
            <option value="cochera">Cochera</option>
            <option value="local" hidden="">Local</option>
            <option value="hotel">Hotel</option>
            <option value="terreno">Terreno</option>
            <option value="oficina">Oficina</option>
            <option value="campo" hidden="">Campo</option>
            <option value="fondo" hidden="">Fondo de comercio</option>
            <option value="galpon" hidden="">Galpón</option>
            <option value="negocio" hidden="">Negocio especial</option>
  </select>
</div>
          <div class="input-group m-2">
            <input type="text" class="form-control pl-2" placeholder="Escriba aqui" name="location" required id="valor-responsivo" autocomplete="off">
            <input type="hidden"  name="operation" id="filtroIngresado-responsivo">
            <span class="input-group-prepend ">
                <button class="btn btn-primary  " type="submit" style="border-top-right-radius: 5px;border-bottom-right-radius: 5px;"><span class="fas fa-search px-3 text-white" ></span></button>
            </span>
          </div>
</div>

  <div class="form-row " hidden="">
    <div class="col-sm-12 m-2"><button type="button" class="btn btn-success btn-lg btn-block filter-responsivo" value="codigo">BUSCAR POR CÓDIGO</button> </div>
    <div class="col-sm-12 m-2"><button type="button" class="btn btn-success btn-lg btn-block filter-responsivo" value="inmobiliaria">BUSCAR POR INMOBILIARIA</button></div>
     
  </div>

  </form>
<!--Fin formulario responsivo-->

<form action="{{ route('search') }}" method="get" id="form-search">  

        <div class="container carousel-content">
  
 
<div id="token"></div>
          <h3 class="mt-5 animated slideInDown" hidden="">Propiedades en venta y alquiler  </h3>
          <h1 class="display-5 animated slideInLeft">  Un lugar para cada etapa de tu vida</h1>
          <div class=" mb-2 row ">
            <div class="col-12 d-flex justify-content-center">
                <div class="btn-group" role="group" aria-label="Basic example" style="height: 85%;">
                  <button type="button" style="padding-bottom: 15px" class="btn bg-gradient-success  filter dis" id="venta" data-micron="bounce">Venta</button>
                  <button type="button" style="padding-bottom: 15px" class="btn bg-gradient-success filter dis" id="alquiler" data-micron="bounce">Alquiler</button>
                  <button type="button" style="padding-bottom: 15px" class="btn bg-gradient-success filter dis" id="alquilerTemporada" data-micron="bounce">Alquiler por temporada</button>
                  <button type="button" style="padding-bottom: 15px" class="btn bg-gradient-success filter dis" id="tiempoCompartido" data-micron="bounce">Tiempo compartido</button>
                </div>
              </div>
          </div>
         
          <div class="input-group" >
            <div class="input-group-prepend  desactivarFiltro"  >
            <select  class="custom-select input-group-text input-group-prepend desactivarFiltro text- bg-secondary px-2" required  name="property"  style="border-top-left-radius: 5px;border-bottom-left-radius: 5px;">
            <option value="" >Buscar por</option>
            <option value="departamento">Departamento</option>
            <option value="ph" hidden="">Ph</option>
            <option value="casa">Casa</option>
            <option value="quinta" hidden="">Quinta</option>
            <option value="cochera">Cochera</option>
            <option value="local">Local</option>
            <option value="hotel" hidden="">Hotel</option>
            <option value="terreno">Terreno</option>
            <option value="oficina">Oficina</option>
            <option value="campo" hidden="">Campo</option>
            <option value="fondo" hidden="">Fondo de comercio</option>
            <option value="galpon" hidden="">Galpón</option>
            <option value="negocio"hidden="">Negocio especial</option>
            </select>
            </div>
            <input type="text" class="form-control pl-2 color" placeholder="Escriba aqui" name="location" required id="valor" autocomplete="off" >

            <input type="hidden"  name="operation" id="filtroIngresado">
            <span class="input-group-prepend " hidden="">
                <button class="btn btn-primary input-group-text " type="submit" style="border-top-right-radius: 5px;border-bottom-right-radius: 5px;"><span class="fas fa-search px-3 text-white" ></span></button>
            </span>
            <span class="input-group-prepend ">
                <button class="btn btn-primary  " type="submit" style="border-top-right-radius: 5px;border-bottom-right-radius: 5px;"><span class="fas fa-search px-3 text-white" ></span></button>
            </span>
          </div>
            <div class=" mt-2 row " hidden="">
              <div class="col-8 ml-auto">
                  <div class="btn-group " role="group" aria-label="Basic example" >
                    <button type="button" class="btn btn-secondary filter" id="codigo">Buscar por código</button>
                    <button type="button" class="btn btn-secondary filter" id="inmobiliaria">Buscar por inmobiliaria</button>
                  </div>
                </div>
            </div>
        </div>




</form>

<div class="container mt-4" >
  <div class="card elevation-3">
  <div class="card-body">
    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-4 text-center"><i class="far fa-building fa-3x text-teal"></i></div>
          <div class="col-md-8">DIVERSIDAD PARA ENCONTRAR INMUEBLES EN LINEA</div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-4 text-center"><i class="fas fa-search-location fa-3x text-teal"></i></div>
          <div class="col-md-8">ESPECIFICA TU BÚSQUEDA</div>    
        </div> 
      </div>
    </div>
  </div>
</div>
</div>
   
      <div class="container" hidden="">

        <!-- Example row of columns -->
        <div class="row">
          <div class="col-md-4">
            <h2>EFICIENCIA </h2>
            <p>EN PERU PROP ENCONTRARAS ANUNCIOS DE TERRENOS, DEPARTAMENTOS, CASAS, PROYECTOS Y LOCALES COMERCIALES EN VENTA Y ALQUILER EN PERU DE LA FORMA MÁS SIMPLE. 
            SOMOS LA ALTERNATIVA MAS SEGURA PARA COMPRAR O VENDER TU PROPIEDAD EN PERU 
            </p>
            <p><a class="btn btn-secondary" href="#" role="button" style="display: none;">View details »</a></p>
          </div>
          <div class="col-md-4">
            <br><br>
            <p>PUBLICA O ENCUENTRA TU PROPIEDAD DE ACUERDO A TUS PREFERENCIAS, UBICACIÓN Y PRECIO
            COMPARA ENTRE TODAS LAS OPCIONES Y ENCUENTRA TU PROYECTO Y/O HOGAR 
            </p>
            <p><a class="btn btn-secondary" href="#" role="button" style="display: none;">View details »</a></p>
          </div>
          <div class="col-md-4">
            <br><br>
            <p>CONSULTA ACERCA DE LOS DIFERENTES PROYECTOS QUE TENEMOS EN PERÚ: DEPARTAMENTOS Y OFICINAS. PERU PUEDE SER UNA GRAN OPCIÓN DE COMPRA PARA TU PRÓXIMA PROPIEDAD. </p>
            <p><a class="btn btn-secondary" href="#" role="button" style="display: none;">View details »</a></p>
          </div>
        </div>
    
        <hr>

      </div> <!-- /container -->
<style type="text/css">
  .hidden{
    display: none;
  }
</style>
   <script>

        var filtro="venta";

        $("#filtroIngresado").val(filtro);
        $( ".filter" ).click(function(e) {

        filtro=this.id ;
        //alert(filtro)
        $(".filter").removeClass('active');
        $(this).addClass('active');

        if (filtro=='codigo' || filtro=='inmobiliaria') {

        $("#form-search").removeAttr('action');
        $("#form-search").removeAttr('method');
        $("#form-search").attr('method','POST');
        $("#form-search").attr('action','{{ route("search-post")}}');
        $("#token").html('@csrf');


        $(".dis").attr('disabled',true);
        $(".dis").removeClass('filter');

         $(".desactivarFiltro").addClass('hidden');
        $(".desactivarFiltro").removeAttr('name');
        $(".desactivarFiltro").removeAttr('required');

  

        if (filtro=='codigo') {
          $("#valor").attr('placeholder','Ingrese el código de publicación');
        }else{
          $("#valor").attr('placeholder','Ingrese el nombre de la inmobiliaria');
        }
        }
        $("#filtroIngresado").val(filtro);
        });

       



/*Formulario responsivo*/

        var filtro_responsivo="venta";

        $("#filtroIngresado-responsivo").val(filtro_responsivo);
        $( ".filter-responsivo" ).click(function(e) {

        filtro_responsivo=this.value ;
        //alert(filtro_responsivo)
        $(".filter-responsivo").removeClass('active');
        $(this).addClass('active');

        if (filtro_responsivo=='codigo' || filtro_responsivo=='inmobiliaria') {

        $("#form-search-responsive").removeAttr('action');
        $("#form-search-responsive").removeAttr('method');
        $("#form-search-responsive").attr('method','POST');
        $("#form-search-responsive").attr('action','{{ route("search-post")}}');
        $("#token-responsivo").html('@csrf');


        $(".dis-responsivo").attr('disabled',true);
        $(".dis-responsivo").removeClass('filter');

         $(".desactivarFiltro-responsivo").addClass('hidden');
        $(".desactivarFiltro-responsivo").removeAttr('name');
        $(".desactivarFiltro-responsivo").removeAttr('required');

  

        if (filtro_responsivo=='codigo') {
          $("#valor-responsivo").attr('placeholder','Ingrese el código de publicación');
        }else{
          $("#valor-responsivo").attr('placeholder','Ingrese el nombre de la inmobiliaria');
        }
        }
        $("#filtroIngresado-responsivo").val(filtro_responsivo);
        });





      </script>

@endsection
