@extends('layouts.app')
@section('title','Peruprop-Precios')
@section('content') 
<div class="container">

   <div class="row mt-5 d-flex  justify-content-center ">
       
        <div class="col-md-4 card ">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h4 class="text-center mt-4">
                        Pago por publicacion</h4>
                </div>
                <div class="panel-body text-center ">
                    <p class="lead">
                        <strong>$100</strong></p>
                </div>
                    <ul class="list-group list-group-flush text-center">
                        <li class="list-group-item"><i class="icon-ok text-danger"></i>Una Publicacion</li>
                        <li class="list-group-item"><i class="icon-ok text-danger"></i>Cualquier tipo de propiedad</li>
                        <li class="list-group-item"><i class="icon-ok text-danger"></i>Publicacion permanente</li>
                        <li class="list-group-item"><i class="icon-ok text-danger"></i>Medio
                        <img src="{{ asset('img/logo-mercadopago8.png')}}" class="rounded" alt="" style="width: 30%;">
                        <img src="{{ asset('img/pagoefectivo logo.png')}}" class="rounded" alt="" style="width: 30%;">
                        </li>
                     </ul> 
                <div class="panel-footer text-white">
                      @auth <!--Pregunto si estoy autenticado me registro y pago--> 

                      @if($tipo=="inmobiliaria")

                    <a class="btn btn-lg btn-block btn-warning my-4" href="{{  url('/publicaciones/registro/inmobiliaria/pago-unico') }}" >Me interesa</a>

                        @else

                    <a class="btn btn-lg btn-block btn-warning my-4" href="{{  url('/publicaciones/registro/anunciante/pago-unico') }}" >Me interesa</a>

                    @endif

                    @else <!--De lo contrario mustra login-->
                    <a class="btn btn-lg btn-block btn-warning my-4" data-toggle="modal" data-target="#exampleModal" role="button">Me interesa</a>
                       @endauth
                </div>
            </div>
        </div>
<div class="col-md-1"></div>

        <div class="col-md-4 card ">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h4 class="text-center mt-4">
                        Subscripcion mensual</h4>
                </div>
                <div class="panel-body text-center ">
                    <p class="lead">
                        <strong>$50 / Mes</strong></p>
                </div>
                    <ul class="list-group list-group-flush text-center">
                        <li class="list-group-item"><i class="icon-ok text-danger"></i>Una Publicacion</li>
                        <li class="list-group-item"><i class="icon-ok text-danger"></i>Cualquier tipo de propiedad</li>
                        <li class="list-group-item"><i class="icon-ok text-danger"></i>Publicacion permanente</li>
                        <li class="list-group-item"><i class="icon-ok text-danger"></i>Medio
                        <img src="{{ asset('img/logo-mercadopago8.png')}}" class="rounded" alt="" style="width: 30%;">
                        </li>
                     </ul> 
                <div class="panel-footer text-white">
                      @auth <!--Pregunto si estoy autenticado me registro y pago--> 
                      @if($tipo=="inmobiliaria")
                    <a class="btn btn-lg btn-block btn-warning my-4" href="{{  url('/publicaciones/registro/inmobiliaria/mensual') }}" >Me interesa</a>
                        @else

                    <a class="btn btn-lg btn-block btn-warning my-4" href="{{  url('/publicaciones/registro/anunciante/mensual') }}" >Me interesa</a>

                    @endif
                    @else <!--De lo contrario mustra login-->
                    <a class="btn btn-lg btn-block btn-warning my-4" data-toggle="modal" data-target="#exampleModal" role="button">Me interesa</a>
                       @endauth
                </div>
            </div>
        </div>

</div>


</div>
@endsection 


