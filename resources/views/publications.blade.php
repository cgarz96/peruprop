@extends('layouts.app')
@section('title','Peruprop-Publicaciones')
@section('content')


     <div class="container">
        <div class="text-center  mt-5 animated bounce"><h1>Publicá tu propiedad en Peruprop</h1></div>
        <div class="row mt-5">
          <div class="col-md-6 animated slideInLeft">
              <div  class="text-center">
                <img src="img/publicar-inmobiliarias.svg" class="rounded" alt="" style="width: 100%;">
              </div>
            <h2>¿Éres una inmobiliaria?</h2>
            <h3>La mayor difusión de tus ofertas en internet</h3>
            <p hidden="">Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>

            <p><a class="btn bg-gradient-success" href="{{  url('/publicaciones/precios/inmobiliaria') }}" role="button">Publicar mis propiedades  »</a></p>
       
          </div>
          <div class="col-md-6 animated slideInRight">
              <div class="text-center">
                <img src="img/publicar-particular.svg" alt="" class="rounded" style="width: 50%; height: 200px;">
              </div>
            
            <h2>¿Éres un propietario?</h2>
            <h3>Vendé o Alquilá tu inmueble en Peruprop</h3>
            <p hidden="">Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
           
            <p><a class="btn bg-gradient-success" href="{{  url('/publicaciones/precios/anunciante') }}" role="button">Publicar un aviso »</a></p>
       
          </div>
        
        </div>    
    </div>
@endsection 