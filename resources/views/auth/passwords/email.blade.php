@extends('layouts.app')

@section('content')
<div class="login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{ url('/') }}"><b>Peruprop</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">¿Olvidaste tu contraseña? Aquí puede recuperar fácilmente una nueva contraseña.</p>

      <form action="{{ route('password.email') }}" method="post">
      	 @csrf
        <div class="input-group">
          <input id="email" type="email" class="form-control  @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        @error('email')
		 <span class="text-danger" >
		    <p>{{ $message}}</p>
		 </span>                                             
         @enderror
 		<br>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn bg-gradient-success btn-block">{{ __('Send Password Reset Link') }}</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <br>
      @if (session('status'))
        <div class="alert text-center" role="alert" style="background: #d4edda; color: #155724">
          {{ session('status') }}
        </div>
      @endif

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
</div>
@endsection