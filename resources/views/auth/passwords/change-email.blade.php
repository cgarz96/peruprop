@extends('layouts.app')

@section('content')
 <div class="login-page">
<div class="login-box">
  <div class="login-logo">
    <a ><b>Peruprop</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Ingrese su contraseña actual para poder cambiar su email ahora.</p>

      <form method="POST" action="{{ route('updateEmail') }}">
        @csrf

        
        <div class="input-group">
          <input id="password_current" type="password" class="form-control @error('password_current') is-invalid @enderror" name="password_current" value="{{ $password_current ?? old('password_current') }}" required autocomplete="password" autofocus placeholder="Ingrese su contraseña actual">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
       @error('password_current')
     <span class="text-danger" >
        <p>{{ $message}}</p>
     </span>                                             
       @enderror
       <br>
        <div class="input-group">
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" required autocomplete="new-email" placeholder="Ingrese su nuevo correo">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
         @error('email')
     <span class="text-danger" >
        <p>{{ $message }}</p>
     </span>                                             
         @enderror
         <br>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn bg-gradient-success btn-block">{{ 'Cambiar Correo electrónico' }}</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <br>
     @if (session('status'))
        <div class="alert text-center" role="alert" style="background: #d4edda; color: #155724">
          {{ session('status') }}
        </div>
      @endif
     @if (session('message'))
        <div class="alert text-center" role="alert" style="background: #f8d7da; color: #721c24">
          {{ session('message') }}
        </div>
      @endif

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
</div>
@endsection