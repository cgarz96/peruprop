@extends('layouts.app')
@section('title','Peruprop-Resultados')
@section('content') 
<link rel="stylesheet" type="text/css" href="{{ asset('css/search-custom.css') }}">
<div class="col-md-12">
<div class="row">

@include('components.sidebar-search')
<div class="col-md-9 " style="min-height: 600px;">
<div class="row  card-header">
  <div class="col-md-9 mt-2">
<b>Propiedades e Inmuebles en {{Request::get('operation')}}</b>

</div>


 <b class="ml-2">{{$nro_resultados}} resultados</b>
</div>


@foreach($propiedades as $propiedad)
            <div class="card card-primary elevation-3 mt-2"> 
              <div class="card-header" hidden="">
                <h3 class="card-title">Ribbons</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">

                  <div class="col-md-4">
                    <div class="fotorama"  data-height="150"  >
                      @foreach($propiedad->fotos as $foto)
                      <img src="{{  asset($foto->ruta) }}" alt="Photo 1" class="img-fluid">
                       @endforeach
  
                    </div>
                      <div class="ribbon-wrapper ribbon-sm" >
                          <div class="ribbon bg-success " style="font-size: 12px">
                            {{$propiedad->visibilidad}}
                          </div>
                        </div>
                    </div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-12">
                        <h5>{{$propiedad->titulo}}</h5>
                      </div>


                      <div class="col-md-12 " style="min-height: 73px">
                    <p class="block-with-text text-muted text-sm">{{$propiedad->descripcion}}</p>    
                      </div>

                 

                      
                    </div> 
                    <hr>
                    <div class="row">
                        <div class="col-md-6"><i class="fas fa-sort-amount-up-alt"></i> {{$propiedad->tipo_operacion}}</div>
                        <div class="col-md-6"> <i class="far fa-calendar-alt"></i> {{$propiedad->fecha_alta}}</div>
                      </div>
                  </div>

                      <div class="col-md-2">
                        <div class="row">
                          <div class="col-md-9 col-sm-9"><label>$ {{$propiedad->precio}}</label></div>
                          @guest
                         <div class="col-md-3 col-sm-3"><button class=" btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#exampleModal" data-micron="bounce" style="border-radius: 15px" ><i class="fas fa-heart"></i></button></div>
                          @else
                          <div class="col-md-3 col-sm-3"><button data-micron="bounce" id="{{$propiedad->propiedad_id}}" class=" btn btn-sm btn-outline-danger favorito {{ esFavorito($propiedad->propiedad_id) }}"    style="border-radius: 15px" ><i class="fas fa-heart"></i></button></div>
                          @endguest
                                 
                          </div>
                          <br>
                          <br>
                          <br>
                          <hr class="mb-3">

                          <div class="row justify-content-center">
                            <a  href="{{ url('busquedas-codigo-'.$propiedad->codigo_publicacion) }}" class="btn bg-gradient-olive">Ver mas detalles</a>
                          </div>

                        
                      </div>


                </div>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
   @endforeach 





</div>


</div>

        <div class="pagination d-flex justify-content-end mb-0" >
 {{ $propiedades->appends(Request::all())->links() }}
            </div>
</div>

  




<script type="text/javascript">
  $('.select2').select2();
  $('.filtro-dormitorio').click(function (e) {//Opciones para domritorios
    e.preventDefault();
    //alert(this.value)
    $('#bedroom').attr('checked',false);
    $(".filtro-dormitorio").removeClass('active');
    $(this).addClass('active');
    $('#bedroom').attr('value',this.value);
    $('#bedroom').attr('checked',true);
  });
  $('.filtro-banio').click(function (e) {//Opciones para baño
    e.preventDefault();
    //alert(this.value)
    $('#bathroom').attr('checked',false);
    $(".filtro-banio").removeClass('active');
    $(this).addClass('active');
    $('#bathroom').attr('value',this.value);
    $('#bathroom').attr('checked',true);
  });
  $('.filtro-estacionamiento').click(function (e) {//Opciones para estacionaminetos
    e.preventDefault();
    //(this.value)
    $('#parking').attr('checked',false);
    $(".filtro-estacionamiento").removeClass('active');
    $(this).addClass('active');
    $('#parking').attr('value',this.value);
    $('#parking').attr('checked',true);
  });

    $('#filtro-direccion').click(function (e) {//Opciones para domritorios
    e.preventDefault();
    var address =$("#address-text").val();
    if (address=="") {
    $('#address').attr('checked',false);
    }else{
      $('#address').attr('value',address);
      $('#address').attr('checked',true);
    }
    
  });

  $('#filtro-ubicacion').click(function (e) {//Opciones para ubicacion
    e.preventDefault();
    var location =$("#location-text").val();
    if (location=="") {
    $('#location').attr('checked',false);
    }else{
      $('#location').attr('value',location);
      $('#location').attr('checked',true);
    }
    
  });
  $('#filtro-precio').click(function (e) {//Opciones para ubicacion
    e.preventDefault();
    var pricefrom =$("#pricefrom-text").val();
    var priceup =$("#priceup-text").val();
    if (pricefrom=="" || priceup=="") {
    $('#pricefrom').attr('checked',false);
    $('#priceup').attr('checked',false);
    }else{
      $('#pricefrom').attr('value',pricefrom);
      $('#priceup').attr('value',priceup);
      $('#priceup').attr('checked',true);
      $('#pricefrom').attr('checked',true);
    }
    
  });

  $('#filtro-superficie').click(function (e) {//Opciones para ubicacion
    e.preventDefault();
    var surfacefrom =$("#surfacefrom-text").val();
    var surfaceup =$("#surfaceup-text").val();
    if (surfacefrom=="" || surfaceup=="") {
    $('#surfacefrom').attr('checked',false);
    $('#surfaceup').attr('checked',false);
    }else{
      $('#surfacefrom').attr('value',surfacefrom);
      $('#surfaceup').attr('value',surfaceup);
      $('#surfaceup').attr('checked',true);
      $('#surfacefrom').attr('checked',true);
    }
    
  });

$('.favorito').click(function (e) {
  
  var id_favorito=this.id;
   $.ajax({
          url: "{{ route('favoritos') }}",
          data:{propiedad_id:id_favorito, _token: "{{ csrf_token() }}"},
          type: "POST",
          success: function(response){
          
          if (response=="add") {
            $('#'+id_favorito).addClass('active')
        console.log(response)}
          else{
            $('#'+id_favorito).removeClass('active');
            console.log(response)
          }
            },
             error : function(message) {
               console.log(message);
            }
          });

  });


</script>

@endsection