<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caracteristica extends Model
{
        public function propiedad()
    {
    	return $this->belongsTo(Propiedad::class);
    }
}
