<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Caracteristica;

class PhController extends Controller
{
		public function registrarCaracteristica($propiedad_id,$request,$ruta)
		{
			       $caracteristica = new Caracteristica;
		           $caracteristica->propiedad_id=$propiedad_id;
		           $caracteristica->superficie = $request->superficie;
		           $caracteristica->antiguedad = $request->antiguedad;
		           $caracteristica->cant_baños = $request->cant_baños;
		           $caracteristica->cant_dormitorios = $request->cant_dormitorios;
		           $caracteristica->expensas = $request->expensas;
		           $caracteristica->sup_cubierta = $request->sup_cubierta;
		           $caracteristica->sup_descubierta = $request->sup_descubierta;
		           $caracteristica->disposicion = $request->disposicion;
		           $caracteristica->cant_plantas = $request->cant_plantas;
		           $caracteristica->estado = $request->estado;
		           $caracteristica->foto_principal = $ruta;
		           $caracteristica->save();
		}

}
