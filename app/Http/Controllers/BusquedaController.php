<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request; 

use Illuminate\Support\Facades\DB;
use App\Propiedad;
use App\Caracteristica;
use App\Servicio;

class BusquedaController extends Controller
{
    public function filtrar(Request $request)
    {
    	if ($request->operation=='codigo' || $request->operation=='inmobiliaria' ) {
				
		return redirect()->route('busquedaEspecifica' , ['operation' => $request->operation, 'location' => $request->location]);

		}

    }

    public function search(Request $request)
    {
        $tipo = $request->input('property',null);
        $operacion = $request->input('operation',null);
        $departamento = $request->input('location',null);
        $antiguedad=$request->input('antiquity',null);
        $dormitorio = $request->input('bedroom',null);
        $baño = $request->input('bathroom',null);
        $tipo_propietario = $request->input('owner',null);
        $precio_desde = $request->input('pricefrom',null);
        $precio_hasta = $request->input('priceup',null);
        $superficie = $request->input('surface',null);
        $estacionamiento = $request->input('parking',null);
        $servicio = $request->input('service',null);
        $precio = $request->input('orderprice',null);
        $cochera = $request->input('garage',null);
        $direccion = $request->input('address',null);

        $propiedades=Propiedad::obtener()
        ->tipo($tipo)
        ->operacion($operacion)
        ->ubicacion($departamento)
        ->precio($precio_desde,$precio_hasta)
        ->banios($baño)
        ->duenio($tipo_propietario)
        ->antiguedad($antiguedad)
        ->dormitorio($dormitorio)
        ->cochera($cochera)
        ->preciorden($precio)
        ->direccion($direccion)
        ->simplePaginate(5);


        $DepartamentosProvincias = DB::table('peru_departamentos')
            ->join('peru_provincias', 'peru_departamento_id', '=', 'peru_departamentos.id')
            ->select('*')
            ->get();


         $habilitarBuscadorNav="nav-buscador";
         $nro_resultados=count($propiedades);
         $data = compact(['propiedades','nro_resultados','habilitarBuscadorNav','DepartamentosProvincias']);
        // return dd($data);
         return view('search', $data);
   
    }



    public function busquedaEspecifica(Request $request)
    {
    	if ($request->operation=='codigo') {// Busqueda por codigo


        $campos= DB::table('propiedads')->where('codigo_publicacion',$request->location)->limit(1)->get();

        if (count($campos)==0) {// retorna true si esta vacio

                 return view('results.sinResultados');

        }

        $id= $campos[0]->id;
        $tipo= $campos[0]->tipo_propietario;

        $user_id= $campos[0]->user_id;
        $tipo_propiedad= $campos[0]->tipo_propiedad;



        $Servicios = DB::table('servicios')
            ->join('propiedad__servicios', 'servicio_id', '=', 'servicios.id')
            ->select('nombre')
            ->where('propiedad_id', '=', $id)
            ->groupByRaw('nombre')
            ->get();
        $Instalaciones = DB::table('instalacions')
            ->join('propiedad__instalacions', 'instalacion_id', '=', 'instalacions.id')
            ->select('nombre')
            ->where('propiedad_id', '=', $id)
            ->groupByRaw('nombre')
            ->get();
        $Ambientes = DB::table('ambientes')
            ->join('propiedad__ambientes', 'ambiente_id', '=', 'ambientes.id')
            ->select('nombre')
            ->where('propiedad_id', '=', $id)
            ->groupByRaw('nombre')
            ->get();  
        $Fotos = DB::table('fotos')
            ->select('ruta')
            ->where('propiedad_id', '=', $id)
            ->get();          




        $descripcionPropiedad = DB::table('propiedads')
          ->join( 'caracteristicas','caracteristicas.propiedad_id','=', 'propiedads.id')
          ->where('propiedads.codigo_publicacion', '=', $request->location)
          ->select('*')
          ->get();


                $propietario = DB::table('anunciantes')
                ->select('*')
                ->where('user_id', $user_id)
                ->get();          

        $data = compact(['descripcionPropiedad','Fotos','Instalaciones','Servicios','Ambientes','propietario','tipo_propiedad','tipo']);
         //return dd($data); 
        return view('results.layoutPropiedadDescripcion',$data);



      //return dd($data);

        }else{//Busqueda por inmobiliaria
        $inmobiliarias = DB::table('inmobiliarias')
            ->select('inmobiliarias.*')
            ->where('razon_social', 'like',  $request->location.'%')
            ->paginate(2);

            $data = compact(['inmobiliarias']);

             if (count($inmobiliarias)==0) {// retorna true si esta vacio

                 return view('results.sinResultados');

                }else{

                        return view('results.inmobiliaria.nombres',$data);  

                }       
        }
    }


}

