<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Caracteristica;

class CampoController extends Controller
{
    public function registrarCaracteristica($propiedad_id,$request,$ruta)
		{
			       $caracteristica = new Caracteristica;
		           $caracteristica->propiedad_id=$propiedad_id;
		           $caracteristica->superficie = $request->superficie;
		           $caracteristica->foto_principal = $ruta;
		           $caracteristica->save();
		}
}
