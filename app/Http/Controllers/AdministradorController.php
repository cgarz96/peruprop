<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Str;

use App\Servicio;
use App\Ambiente;
use App\Instalacion;
use App\Pago;

use App\Propiedad;
use App\Caracteristica;

use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class AdministradorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	 $user=Auth::user();// Me fijo si esta autenticado
    
        if (!$user || $user->role_id!=1) {
            return redirect('/');
          }

        return view('users.admin.dashboard');
    }


    public function requestProperties()
    {
       $user=Auth::user();// Me fijo si esta autenticado
    
        if (!$user || $user->role_id!=1) {
            return redirect('/');
          }

      $publicacionesSolicitadas = DB::table('propiedads')
      ->join( 'caracteristicas','caracteristicas.propiedad_id','=', 'propiedads.id')
      ->join( 'pagos','pagos.id','=', 'propiedads.pago_id')
      ->where('propiedads.codigo_publicacion', '=', NULL)
      ->where('propiedads.estado_publicacion', '=', 'Pendiente')
      ->select('*')
      ->simplePaginate(5);

      //return dd($publicacionesSolicitadas);

        $data=compact(['publicacionesSolicitadas']);
        //return dd($data);
        return view('users.admin.requests',$data);
    }

    public function acceptPublicactions(Request $request)
    {

      $codigo_publicacion=Str::random(20);
      $fecha_inicio = Carbon::now();
      $fecha_fin = Carbon::now()->addMonth();


          DB::table('propiedads')
          ->where('id',$request->id)
          ->update(['codigo_publicacion' => $codigo_publicacion,'fecha_alta'=>$fecha_inicio,'fecha_baja'=>$fecha_fin,'estado_publicacion'=>'Aprobado']);
          
          DB::table('anunciantes')
                ->where('user_id',$request->user_id)
                ->increment('cant_publicaciones', 1); 


              return "Aceptado";
    }


    public function refusedPublications(Request $request)
    {
          DB::table('propiedads')
          ->where('id',$request->id)
          ->update(['estado_publicacion' => 'Rechazado']);

          return "listo";
    }



    public function publications($value='')
    {

       $publicaciones= DB::table('propiedads')
      ->select('*')
      ->simplePaginate(6);

      $data=compact(['publicaciones']);
      return view('users.admin.publications',$data);
    }

    public function realState() 
    {
       
      $inmobiliariasRegistradas = DB::table('anunciantes')
      ->select('*')
      ->get();

      $data=compact(['inmobiliariasRegistradas']);
      return view('users.admin.real-state',$data);
    }


     public function advertisers($tipo_anunciante) 
    {
      switch ($tipo_anunciante) {
        case 'real-states':
          $role_id=4;
          break;
        case 'agents':
          $role_id=3;
          break;
        case 'particulars':
          $role_id=5;
          break;
        
        default:
          $role_id=NULL;
          break;
      }
       
      $anunciantesRegistrados = DB::table('anunciantes')->where('role_id',$role_id)
      ->select('*')
      ->simplePaginate(6);

      $data=compact(['anunciantesRegistrados']);
      return view('users.admin.advertisers',$data);
    }

     public function properties()
    {

        $ServiciosRegistrados=Servicio::paginate();
        $InstalacionesRegistradas=Instalacion::paginate();
        $AmbientesRegistrados=Ambiente::paginate();

        $data = compact(['ServiciosRegistrados', 'InstalacionesRegistradas', 'AmbientesRegistrados']);

        return view('users.admin.properties',$data);
    }

     public function deletePublications(Request $request)
    {
     $ids= DB::table('propiedads')
      ->where('user_id',$request->user_id)
      ->select('id','pago_id')
      ->get();
     DB::table('propiedads')
      ->where('user_id',$request->user_id)
      ->delete();

      DB::table('anunciantes')
      ->where('user_id',$request->user_id)
      ->update(['cant_publicaciones' => 0]);       
      

      foreach ($ids as $key => $propiedad) {
       
         DB::table('pagos')
        ->where('id',$propiedad->pago_id)
        ->update(['estado' => 'eliminado']);


      }

   //$raiz=public_path().'\img\\'.$request->user_id;
    // AdministradorController::rrmdir($raiz);
      return "ok";

    }

     public function deletePublication(Request $request)
    {
      //return $request->all();

      DB::table('pagos')
        ->where('id',$request->pago_id)
        ->update(['estado' => 'eliminado']);
       
     DB::table('propiedads')
      ->where('id',$request->id)
      ->delete();

 
      DB::table('anunciantes')
              ->where('user_id',$request->user_id)
              ->decrement('cant_publicaciones', 1);      



      $ruta=public_path().'\img\\'.$request->user_id.'\\'.$request->id;
     // $ruta=public_path().'\img\\'.$request->id;
      AdministradorController::rrmdir($ruta);

      return "ok";
    }



    /************************************************************
     * Funcion para eliminar carpeta y archivos que contiene *
    ************************************************************/

    public function rrmdir($src) {

          if (file_exists($src)) {
          $dir = opendir($src);
          while(false !== ( $file = readdir($dir)) ) {
              if (( $file != '.' ) && ( $file != '..' )) {
                  $full = $src . '/' . $file;
                  if ( is_dir($full) ) {
                      rrmdir($full);
                  }
                  else {
                      unlink($full);
                  }
              }
          }
          closedir($dir);
          rmdir($src);
      }else{
         echo "la ruta $src no existe";
      }
    }


     public function registerService(Request $request)
    {
        $FormularioServicio=request()->except('_token');
         Servicio::insert($FormularioServicio); 
         return back()->with('mensaje', 'Nuevo servicio agregado!');
    }
     public function registerInstalation(Request $request)
    {
       // return $request->all(); 
        $FormularioInstalacion=request()->except('_token');
         Instalaciones::insert($FormularioInstalacion); 
         return back()->with('mensaje', 'Nueva instalación agregada!');
    }
     public function registerAmbience(Request $request)
    {
        $FormularioAmbiente=request()->except('_token');
         Ambientes::insert($FormularioAmbiente); 
         return back()->with('mensaje', 'Nuevo ambiente agregado!');
    }
}
 