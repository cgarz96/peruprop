<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;

use App\User;

use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $tipoUsuario= $request->user()->isAdmin();
        if ($tipoUsuario=="User" || $tipoUsuario=="Agente")
        {
        //return "2";//Respuesta para ajax
        return url('/'); 
        }
        else
        {
        //return "1";  
        return url('/admin'); 
        }
    }

    public function changePassword()
    {  
        return view('auth.passwords.change');
    }

    public function updatePassword(Request $request)
    {
    $rules = [
        'password_current'=>'required',
        'password'=>'required|min:8|confirmed',
    ];
    $messages = [
            'password_current.required' => 'El campo es requerido',
            'password.required' => 'El campo es requerido',
            'password.confirmed' => 'Las contraseñas no coinciden',
            'password.min' => 'El mínimo permitido son 8 caracteres',
        ];


    $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()){
            return back()->withErrors($validator);
        }
        else{
            if (Hash::check($request->password_current,Auth::user()->password)){
                $user = new User;
                $user->where('email', '=', Auth::user()->email)
                     ->update(['password' => bcrypt($request->password)]);
                return back()->with('status', 'Contraseña cambiada con éxito');
            }
            else
            {
                
                return back()->with('message', 'Contraseña actual incorrecta');
            }
        } 

    }
    public function changeEmail()
    {
        return view('auth.passwords.change-email');
    }

    public function updateEmail(Request $request)
    {
     $rules = [
            'password_current'=>'required',
            'email'=>'required|string|email|max:255|unique:users',
        ];
        $messages = [
                'password_current.required' => 'El campo es requerido',
                'email.required' => 'El email es requerido',
                'email.unique' => 'Ya existe este correo por favor ingrese otro',
                'email.email' => 'El formato de correo es inválido',
            ];


        $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()){
                return back()->withErrors($validator);
            }
            else{
                if (Hash::check($request->password_current,Auth::user()->password)){
                    $user = new User;
                    $user->where('id', '=', Auth::user()->id)
                         ->update(['email' => $request->email]);
                    return back()->with('status', 'Correo cambiado con éxito');
                }
                else
                {
                    
                    return back()->with('message', 'Contraseña actual incorrecta');
                }
            } 
    }

}