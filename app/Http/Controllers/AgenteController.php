<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class AgenteController extends Controller
{
    public function listadoInmobiliarias()
    {

      $Inmobiliarias = User::where('tipo_propietario','Inmobiliaria')->simplePaginate(6);

      $data=compact(['Inmobiliarias']);
      

      return view('users.customers.agente.resultado-inmobiliarias',$data);
    }
}
