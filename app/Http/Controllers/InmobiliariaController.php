<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Inmobiliaria;
use App\Propiedad;
use App\Paquete_User;
use App\Paquete;
use App\PeruDepartamento;


use MP;

use App\Pago;

use App\Agente;

use App\User;

use App\Anunciante;

use File;

use Illuminate\Support\Str;

use Illuminate\Support\Facades\Storage; 

use Illuminate\Support\Carbon;

class InmobiliariaController extends Controller
{
    public function formulario(Request $request)
    {

      $modalidad= DB::table('paquetes')->where('slug',$request->modalidad)
      ->select('*')
      ->get();

    	if (Auth::user()->role_id==2 || Auth::user()->tipo_propietario==NULL) {

      $datos =Auth::user();
      $departamentos=PeruDepartamento::all();


      $data=compact(['departamentos','modalidad','datos']);

			return view('users.customers.inmobiliaria.pasos-registro',$data);
 
	 	  }elseif (Auth::user()->role_id==5) {
    		//return dd('Usted es un Particular'); 
        $tipo='Particular'; 
        $data=compact('tipo');
        return view('results.alertaInfoPaquete',$data);
    	}

      else{


      $data=compact(['modalidad']);

      return view('users.customers.inmobiliaria.pasos-pagos',$data);



   		}   
    	
    }

    public function obtenerPropiedades(Request $request)
    {

      $propiedades=Propiedad::obtener()->inmobiliaria($request->user_id)->paginate(1);

        $DepartamentosProvincias = DB::table('peru_departamentos')
            ->join('peru_provincias', 'peru_departamento_id', '=', 'peru_departamentos.id')
            ->select('*')
            ->get();


                       $nro_resultados=count($propiedades);
                        if ($nro_resultados==0) {// retorna true si esta vacio

                        return view('results.sinResultados');

                    }else{
                  $data = compact(['propiedades','nro_resultados','DepartamentosProvincias']);
                return view('search', $data);
                }
    }


    public function registrar(Request $request) 
    {
        

      $precio_paquete=floatval($request->precio_paquete);





 if (Auth::user()->tipo_propietario==''  || Auth::user()->tipo_propietario==NULL) {// Para el caso si el usuario retrocede de mp al formulario de registro
          if (Auth::user()->role_id==3) {$role_id=3;$tipo_propietario='Agente';}else{$role_id=4;$tipo_propietario='Inmobiliaria';}
          $user= User:: find(Auth::user()->id);
            if ($request->hasFile('logo')) {

            $file_path = public_path().$user->avatar; 
            if(File::exists($file_path)) File::delete($file_path);            
            $name=Str::random(20);  
            $filename=$name.'.'.$request->logo->extension();
            $filenameDB='/img/logos/'.$filename;
            $path = $request->file('logo')->move(public_path().'/img/logos/',$filename);
            
            }

             $id_usuario=Auth::user()->id;
             $anunciante = new Anunciante;
             $anunciante->user_id= $id_usuario;
             $anunciante->razon_social= $request->razon_social;
             $anunciante->nombre_anunciante= $request->nombre;
             $anunciante->apellido_anunciante= $request->apellido;
             $anunciante->codigo_postal_anunciante= $request->codigo_postal;
             $anunciante->documento_anunciante=$request->dni;//
             $anunciante->movil_anunciante= $request->movil_user;//
             $anunciante->movil_anunciante_fact= $request->movil;//
             $anunciante->correo_anunciante=$request->correo_contacto;
             $anunciante->correo_anunciante_fact=$request->correo;
             $anunciante->telefono_anunciante=$request->telefono_user;
             $anunciante->telefono_anunciante_fact=$request->telefono;
             $anunciante->domicilio_anunciante=$request->domicilio;//
             $anunciante->departamento_anunciante=$request->departamento;//
             //$anunciante->districto_anunciante=$request->districto;//
             $anunciante->provincia_anunciante=$request->provincia;//
             $anunciante->logo_anunciante=$filenameDB;//
             $anunciante->role_id=$role_id;//
             $anunciante->cant_publicaciones= 0;
             $anunciante->save();

             DB::table('users')->where('id', $id_usuario)
                  ->update(['tipo_propietario' => $tipo_propietario,'role_id'=>$role_id]);
            if ($request->paquete_id==9) {//PaqueteGratis
              $fecha_expiracion = Carbon::now()->addMonth();


               $pago = new Pago;
               $pago->user_id = $id_usuario;
               $pago->collection_id=0;
               $pago->merchant_order_id=0;
               $pago->collection_status='approved';
               $pago->payment_type='free';
               $pago->preference_id=0;
               $pago->processing_mode='free';
               $pago->site_id='peruprop';
               $pago->external_reference='peruprop';
               $pago->merchant_account_id=0;

               $pago->save();
               $pago_id = $pago->id;
               $paquete_id=$request->paquete_id;
               $nro_avisos=intval($request->nro_avisos);

               $paquete_usuario = new Paquete_User;
               $paquete_usuario->user_id= $id_usuario;
               $paquete_usuario->paquete_id= $paquete_id;
               $paquete_usuario->pago_id= $pago_id;
               $paquete_usuario->publicaciones_restantes= $nro_avisos;
               $paquete_usuario->fecha_compra_paquete= Carbon::now();
               $paquete_usuario->fecha_expiracion_paquete= $fecha_expiracion;
               $paquete_usuario->save();
               return redirect('panel/estados')->with('status','Se registró su pago en nuestro sistema.En este panel puede ver las solicitudes realizadas.');
            }

          }
 

        $dominio = env("DOMAIN_SITE", "");
        $currency_id = env("CURRENCY", "");

 
     /*Prefrencia de pago por unica vez*/  
        $preferenceData = [
          'items' => [
            [
              'title' => 'PAGO DE '.$request->nombre_paquete,
              'description' => 'Pago de paquete en peruprop',
              //'picture_url' => 'https://es.wikipedia.org/wiki/Disco_compacto#/media/Archivo:OD_Compact_disc.svg',
              'quantity' => 1,
              'currency_id' => $currency_id,//PEN
              'unit_price' => $precio_paquete
            ]
          ],
          // "auto_return" => "approved",// retorno automatico  a la pagina de mi siste
          "back_urls" => array(
              "success" => $dominio."/publicaciones/registro/guardar-pago?package=".$request->paquete_id."&plain=".$request->nro_avisos,
              "failure" => $dominio."/status-error",
              "pending" => $dominio."/publicaciones/registro/guardar-pago?package=".$request->paquete_id."&plain=".$request->nro_avisos
          ),

        ];

        $preference = MP::create_preference($preferenceData);


         

           $preference= $preference['response']['sandbox_init_point'];
           return redirect($preference);

    //return view('users.customers.inmobiliaria.pagos')->with('preference',$preference);  
               
       
    }

    public function listadoAgentes()
    {

      $Agentes = User::where('role_id',3)->simplePaginate(6);

      $data=compact(['Agentes']);
      

      return view('users.customers.inmobiliaria.resultado-agentes',$data);
    }

}
