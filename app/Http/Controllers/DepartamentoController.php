<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Caracteristica;

class DepartamentoController extends Controller
{
    	  
		public function registrarCaracteristica($propiedad_id,$request,$ruta)
		{
			       $caracteristica = new Caracteristica;
		           $caracteristica->propiedad_id=$propiedad_id;
		           $caracteristica->superficie = $request->superficie;
		           $caracteristica->antiguedad = $request->antiguedad;
		           $caracteristica->estado = $request->estado;
		           $caracteristica->tipo_balcon = $request->tipo_balcon;
		           $caracteristica->expensas = $request->expensas;
		           $caracteristica->cant_chocheras = $request->cant_chocheras;
		           $caracteristica->foto_principal = $ruta;
		           $caracteristica->save();
		}

    	  
}
