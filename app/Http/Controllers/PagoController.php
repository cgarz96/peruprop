<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Carbon;

use MP;

use App\Pago;

use Illuminate\Support\Facades\Auth;

use App\Paquete;

use App\Paquete_User;


use Illuminate\Support\Facades\DB;

class PagoController extends Controller
{



public function index(Request $request)
{
	# code...
}



    public function guardarPreferenciaMP(Request $request)
    {
		 if (isset($_GET['collection_id'] )) {

	if ($_GET['collection_id']=='null' || $_GET['merchant_order_id']=='null' || $_GET['collection_status']== 'null' || $_GET['payment_type'] == 'null' || $_GET['preference_id']=='null' || $_GET['processing_mode']=='null') {
	return view('users.customers.error-pago');
}	      
		     // $payment_info = MP::get_payment_info($_GET['merchant_order_id'] );// obtengo el estado de la orden de compra

		     // if ($payment_info["status"] == 200) {

		     //  return dd($payment_info["response"]);

		DB::beginTransaction();
			try {
    			   $fecha_expiracion = Carbon::now()->addMonth();


		       	   $user_id=Auth::user()->id;
		       	   $pago = new Pago;
		           $pago->user_id = $user_id;
		           $pago->collection_id=$_GET['collection_id'];
		           $pago->merchant_order_id=$_GET['merchant_order_id'];
		           $pago->collection_status=$_GET['collection_status'];
		           $pago->payment_type=$_GET['payment_type'];
		           $pago->preference_id=$_GET['preference_id'];
		           $pago->processing_mode=$_GET['processing_mode'];
		           $pago->site_id=$_GET['site_id'];
		           $pago->external_reference=$_GET['external_reference'];
		           $pago->merchant_account_id=$_GET['merchant_account_id'];

		           $pago->save();
		           $pago_id = $pago->id;
		           $paquete_id=$_GET['package'];
		           $nro_avisos=intval($_GET['plain']);

			       $paquete_usuario = new Paquete_User;
	               $paquete_usuario->user_id= $user_id;
	               $paquete_usuario->paquete_id= $paquete_id;
	               $paquete_usuario->pago_id= $pago_id;
	               $paquete_usuario->publicaciones_restantes= $nro_avisos;
	               $paquete_usuario->fecha_compra_paquete= Carbon::now();
	               $paquete_usuario->fecha_expiracion_paquete= $fecha_expiracion;
	               $paquete_usuario->save();

					//$request->session()->put('pago_id', $pago_id);

		           //return redirect('publicaciones/aviso');

		   DB::commit();
		    // all good
		} catch (\Exception $e) {
		    DB::rollback();
		    // something went wrong
		    return view('users.customers.error-pago');
		    

		}
		return redirect('panel/estados')->with('status','Se registró su pago en nuestro sistema.En este panel puede ver las solicitudes realizadas.');


		    //  }


		    }else{
		      dd('Acceso NO autoriazado');
		    }  

    }
}
