<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Caracteristica;

class CocheraController extends Controller
{
		public function registrarCaracteristica($propiedad_id,$request,$ruta)
		{
			       $caracteristica = new Caracteristica;
		           $caracteristica->propiedad_id=$propiedad_id;
		           $caracteristica->superficie = $request->superficie;
		           $caracteristica->tipo_cochera = $request->tipo_cochera;
		           $caracteristica->tipo_coche = $request->tipo_coche;
		           $caracteristica->tipo_de_acceso = $request->tipo_de_acceso;
		           $caracteristica->sup_cubierta = $request->sup_cubierta;
		           $caracteristica->foto_principal = $ruta;
		           $caracteristica->save();
		}
}
