<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Caracteristica;

class QuintaController extends Controller
{
		public function registrarCaracteristica($propiedad_id,$request,$ruta)
		{
			       $caracteristica = new Caracteristica;
		           $caracteristica->propiedad_id=$propiedad_id;
		           $caracteristica->superficie = $request->superficie;
		           $caracteristica->antiguedad = $request->antiguedad;
		           $caracteristica->estado = $request->estado;
		           $caracteristica->cant_baños = $request->cant_baños;
		           $caracteristica->cant_dormitorios = $request->cant_dormitorios;
		           $caracteristica->sup_total = $request->sup_total;
		           $caracteristica->tipo_techo = $request->tipo_techo;
		           $caracteristica->tipo_piso = $request->tipo_piso;
		           $caracteristica->foto_principal = $ruta;
		           $caracteristica->save();
		}
}
