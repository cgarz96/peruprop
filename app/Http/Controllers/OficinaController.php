<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Caracteristica;

class OficinaController extends Controller
{
		public function registrarCaracteristica($propiedad_id,$request,$ruta)
		{
			       $caracteristica = new Caracteristica;
		           $caracteristica->propiedad_id=$propiedad_id;
		           $caracteristica->superficie = $request->superficie;
		           $caracteristica->antiguedad = $request->antiguedad;
		           $caracteristica->cant_baños = $request->cant_baños;
		           $caracteristica->sup_cubierta = $request->sup_cubierta;
		           $caracteristica->ambientes = $request->ambientes;
		           $caracteristica->expensas = $request->expensas;
		           $caracteristica->sup_planta = $request->sup_planta;
		           $caracteristica->sup_total = $request->sup_total;
		           $caracteristica->estado = $request->estado;
		           $caracteristica->foto_principal = $ruta;
		           $caracteristica->save();
		}

}
