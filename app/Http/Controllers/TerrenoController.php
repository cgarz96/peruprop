<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Caracteristica;

class TerrenoController extends Controller
{
		public function registrarCaracteristica($propiedad_id,$request,$ruta)
		{
			       $caracteristica = new Caracteristica;
		           $caracteristica->propiedad_id=$propiedad_id;
		           $caracteristica->superficie = $request->superficie;
		           $caracteristica->antiguedad = $request->antiguedad;
		           $caracteristica->tipo_de_pendiente = $request->tipo_de_pendiente;
		           $caracteristica->long_frente = $request->long_frente;
		           $caracteristica->long_fondo = $request->long_fondo;
		           $caracteristica->sup_construible = $request->sup_construible;
		           $caracteristica->zonificacion = $request->zonificacion;
		           $caracteristica->foto_principal = $ruta;
		           $caracteristica->save();
		}
}
