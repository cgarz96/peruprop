<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Caracteristica;

class FondoComercioController extends Controller
{
		public function registrarCaracteristica($propiedad_id,$request,$ruta)
		{
			       $caracteristica = new Caracteristica;
		           $caracteristica->propiedad_id=$propiedad_id;
		           $caracteristica->superficie = $request->superficie;
		           $caracteristica->antiguedad = $request->antiguedad;
		           $caracteristica->estado = $request->estado;
		           $caracteristica->cant_baños = $request->cant_baños;
		           $caracteristica->sup_total = $request->sup_total;
		           $caracteristica->expensas = $request->expensas;
		           $caracteristica->foto_principal = $ruta;
		           $caracteristica->save();
		}
}
