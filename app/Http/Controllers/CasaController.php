<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Caracteristica;

class CasaController extends Controller
{
    public function registrarCaracteristica($propiedad_id,$request,$ruta)
    {
    	   $caracteristica = new Caracteristica;
           $caracteristica->propiedad_id=$propiedad_id;
           $caracteristica->superficie = $request->superficie;
           $caracteristica->antiguedad = $request->antiguedad;
           $caracteristica->cant_baños = $request->cant_baños;
           $caracteristica->cant_dormitorios = $request->cant_dormitorios;
           $caracteristica->tipo_techo = $request->tipo_techo;
           $caracteristica->cant_chocheras = $request->cant_chocheras;
           $caracteristica->foto_principal = $ruta;
           $caracteristica->save();

    	//echo $tipo_propiedad.$propiedad_id.$request->tipo_operacion;
    }
}
