<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Caracteristica;

class LocalController extends Controller
{
    public function registrarCaracteristica($propiedad_id,$request,$ruta)
		{
			       $caracteristica = new Caracteristica;
		           $caracteristica->propiedad_id=$propiedad_id;
		           $caracteristica->superficie = $request->superficie;
		           $caracteristica->antiguedad = $request->antiguedad;
		           $caracteristica->estado = $request->estado;
		           $caracteristica->ultima_actividad = $request->ultima_actividad;
		           $caracteristica->tipo_local = $request->tipo_local;
		           $caracteristica->sup_cubierta = $request->sup_cubierta;
		           $caracteristica->cant_plantas = $request->cant_plantas;
		           $caracteristica->cant_baños = $request->cant_baños;
		           $caracteristica->foto_principal = $ruta;
		           $caracteristica->save();
		}
}
