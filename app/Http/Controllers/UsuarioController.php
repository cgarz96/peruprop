<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Servicio;
use App\Ambiente;
use App\Instalacion;

use App\Propiedad;
use App\Caracteristica;
use App\Propiedad_Servicio;
use App\Propiedad_Instalacion;
use App\Propiedad_Ambiente;
use App\Foto;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str; 

use App\Http\Controllers\CasaController;
use App\Http\Controllers\DepartamentoController;
use App\Http\Controllers\LocalController;
use App\Http\Controllers\HotelController;
use App\Http\Controllers\TerrenoController;
use App\Http\Controllers\CampoController;
use App\Http\Controllers\CocheraController;
use App\Http\Controllers\OficinaController;
use App\Http\Controllers\PhController;
use App\Http\Controllers\GalponController;
use App\Http\Controllers\FondoComercioController;
use App\Http\Controllers\NegocioEspecialController;
use App\Http\Controllers\QuintaController;

use File;

use Carbon\Carbon; 


use App\PeruDepartamento;

use App\Favorito_Usuario;

use App\Visto_Usuario;

use App\Contactado_Usuario;


use App\User;

use App\Agente;

use App\Inmobiliaria;

use App\Anunciante;


use Illuminate\Support\Facades\Storage;

class UsuarioController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
/*    public function __construct()
    {
        $this->middleware('auth');
    }
*/

    public function nuevaPropiedad($value='')
    { 
  
      $user_id=Auth::user()->id;

      $departamentos=PeruDepartamento::all();

      $paquetes_user=DB::table('paquete__users')->join('paquetes','paquetes.id','=','paquete__users.paquete_id')
      ->where('user_id',$user_id)
      ->where('publicaciones_restantes','<>',0)
      ->where('fecha_expiracion_paquete','>',Carbon::now())
            //y estado distinto  de vencido o completo 
      ->select('paquetes.*','paquete__users.id as paquete_user_id')
      ->get();

    	$ServiciosRegistrados=Servicio::paginate();
        $InstalacionesRegistradas=Instalacion::paginate();
        $AmbientesRegistrados=Ambiente::paginate();

        $data = compact(['ServiciosRegistrados', 'InstalacionesRegistradas', 'AmbientesRegistrados','paquetes_user','departamentos']);
    
        return view('users.customers.create',$data);
    }



     public function agregarServicio(Request $request)
    {

      $request->session()->push('servicio', $request->servicio);

        return "Servicio agregado";
    }
     public function agregarInstalacion(Request $request)
    {

      $request->session()->push('instalacion', $request->instalacion);

         return "Instalacion agregada";
    }

     public function agregarAmbiente(Request $request)
    {
      $request->session()->push('ambiente', $request->ambiente);

          return "Ambiente agregado";
    }



    public function solicitarPropiedad(Request $request)
    {

    DB::beginTransaction();
      try {

		  $user_id=Auth::user()->id;
      $tipo_propietario=Auth::user()->tipo_propietario;

      $paquetes_user=DB::table('paquete__users')
      ->where('id',$request->paquete_user_id)
      ->select('pago_id','paquete_id')
      ->get();

      DB::table('paquete__users')
          ->where('id',$request->paquete_user_id)
          ->decrement('publicaciones_restantes', 1); 


      $paquete_id=$paquetes_user[0]->paquete_id;
      $pago_id=$paquetes_user[0]->pago_id;

 

  	   $propiedad = new Propiedad;
  	   $propiedad->pago_id=$pago_id; 
  	   $propiedad->tipo_propiedad=$request->tipo_propiedad;
  	   $propiedad->user_id = $user_id;
  	   $propiedad->tipo_propietario = $tipo_propietario;
  	   $propiedad->titulo = $request->titulo;
       $propiedad->tipo_operacion = $request->tipo_operacion;
       $propiedad->descripcion = $request->descripcion;
       $propiedad->precio = $request->precio;
       $propiedad->direccion = $request->direccion;
       $propiedad->departamento = $request->departamento;
       $propiedad->provincia = $request->provincia;
       $propiedad->ubicacion_completa = $request->departamento.", ".$request->provincia;
       $propiedad->latitud = $request->latitud;
       $propiedad->longitud = $request->longitud;
       $propiedad->paquete_id = $paquete_id;
       $propiedad->visibilidad = $request->visibilidad;
       $propiedad->estado_publicacion = 'Pendiente';
       $propiedad->prioridad_visibilidad = $request->prioridad_visibilidad;
       $propiedad->save();
       $propiedad_id = $propiedad->id;

        $ruta=public_path().'/img/'.$user_id.'/';
        if (!file_exists($ruta)) {
            mkdir($ruta, 0777, true);
         
        }

     /*       if ($request->hasFile('foto')) {
            $file=$request->file('foto');
            foreach ($file as $key => $value) {
            $name=$propiedad_id.$value->getClientOriginalName();
            $value->move($ruta.$propiedad_id.'/',$name);
             //$value->move(public_path().'/img/'.$propiedad_id.'/',$name);
             $rutaBD='/img/'.$user_id.'/'.$propiedad_id.'/'.$name;
              $foto= new Foto;
              $foto->propiedad_id=$propiedad_id;
              $foto->ruta=$rutaBD;
              $foto->save();
            }
                
            }*/

            if ($request->hasFile('foto')) {
            $file=$request->file('foto');
            foreach ($file as $key => $value) {

            $name=Str::random(20);  
            $filename=$name.'.'.$value->extension();
            $value->move($ruta.$propiedad_id.'/',$filename);
             $rutaBD='/img/'.$user_id.'/'.$propiedad_id.'/'.$filename;
             
              $foto= new Foto;
              $foto->propiedad_id=$propiedad_id;
              $foto->ruta=$rutaBD;
              $foto->save();
            }
                
            }
 
 
    	
		   UsuarioController::registrarCaracteristica($request->tipo_propiedad,$propiedad_id,$request,$rutaBD);



		   if ($request->session()->has('servicio')) {


				$ServicioAgregar = $request->session()->get('servicio');
				foreach ($ServicioAgregar as  $value) {
				      $serv_casa= new Propiedad_Servicio;
				      $serv_casa->propiedad_id=$propiedad_id;
				      $serv_casa->servicio_id=$value;
				      $serv_casa->save();
				    }
        }

        if ($request->session()->has('instalacion')) {
         

        				 $InstalacionAgregar=$request->session()->get('instalacion');

        				    foreach ($InstalacionAgregar as  $value) {
        				      $ins_casa= new Propiedad_Instalacion;
        				      $ins_casa->propiedad_id=$propiedad_id;
        				      $ins_casa->instalacion_id=$value;
        				      $ins_casa->save();
        				    }
        }

        if ($request->session()->has('ambiente')) {
          

        				    $AmbienteAgregar=$request->session()->get('ambiente');

        				    foreach ($AmbienteAgregar as  $value) {
        				      $ins_casa= new Propiedad_Ambiente;
        				      $ins_casa->propiedad_id=$propiedad_id;
        				      $ins_casa->ambiente_id=$value;
        				      $ins_casa->save();
        				    }
        }
				$request->session()->forget(['ambiente', 'instalacion','servicio','pago_id']);

       DB::commit();
        // all good
    } catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        return back()->with('mensaje','Ocurrió un problema intentalo de nuevo.');
        

    }
        return redirect('panel/estados')->with('mensaje','Se registro su solicitud de publicación.Este proceso se encuentra a espera de aprobación del administrador.Gracias.'); 
    }



    public function registrarCaracteristica($tipo_propiedad,$propiedad_id,$request,$ruta)
    	{
    		 
    switch ($tipo_propiedad) {
      case 'ph':
      $result = (new PhController)->registrarCaracteristica($propiedad_id,$request,$ruta);

        break;
      case 'casa':
         
		  $result = (new CasaController)->registrarCaracteristica($propiedad_id,$request,$ruta);
          
        break;
      case 'departamento':
      	  $result = (new DepartamentoController)->registrarCaracteristica($propiedad_id,$request,$ruta);

        break;
      case 'quinta':
      $result = (new QuintaController)->registrarCaracteristica($propiedad_id,$request,$ruta);

        break;
      case 'cochera':
      $result = (new CocheraController)->registrarCaracteristica($propiedad_id,$request,$ruta);

        break;
      case 'local':
      		$result = (new LocalController)->registrarCaracteristica($propiedad_id,$request,$ruta);

        break;
      case 'hotel':
      		$result = (new HotelController)->registrarCaracteristica($propiedad_id,$request,$ruta);

        break;
      case 'terreno':
      		$result = (new TerrenoController)->registrarCaracteristica($propiedad_id,$request,$ruta);

        break;
      case 'oficina':
      	$result = (new OficinaController)->registrarCaracteristica($propiedad_id,$request,$ruta);

        break;
      case 'campo':
        $result = (new CampoController)->registrarCaracteristica($propiedad_id,$request,$ruta);
        break;
      case 'fondo':
      $result = (new FondoComercioController)->registrarCaracteristica($propiedad_id,$request,$ruta);

        break;
      case 'galpon':
      $result = (new GalponController)->registrarCaracteristica($propiedad_id,$request,$ruta);

        break;
      case 'negocio':
      $result = (new NegocioEspecialController)->registrarCaracteristica($propiedad_id,$request,$ruta);

        break;
      
      default:
        return dd('Error de nombre propiedad');
        break;
    }
    	}





    	/*Esta funcion retorna vistas por medio de ajax en base a lo que solicite el usuario*/
    public function obtenerCampos(Request $request)
    {

    switch ($request->propiedad) {
      case 'ph':
        return view('partials.camposPh');
        break;
      case 'casa':
         return view('partials.camposCasa');
        break;
      case 'departamento':
         return view('partials.camposDeptos');
        break;
      case 'quinta':
        return view('partials.camposQuinta');
        break;
      case 'cochera':
        return view('partials.camposCochera');
        break;
      case 'local':
        return view('partials.camposLocales');
        break;
      case 'hotel':
        return view('partials.camposHoteles');
        break;
      case 'terreno':
        return view('partials.camposTerreno');
        break;
      case 'oficina':
        return view('partials.camposOficina');
        break;
      case 'campo':
        return "";
        break;
      case 'fondo':
        return view('partials.camposFondoComercio');
        break;
      case 'galpon':
        return view('partials.camposGalpon');
        break;
      case 'negocio':
        return view('partials.camposNegocioEspecial');
        break;
      
      default:
        return dd('Error en la consulta');
        break;
    }
   


    }


 

    public function enviarMail(Request $request)
    {
      //return $request->all();

  
       $correo_user=$request->email;
       $correo_propietario=$request->correo_propietario;
       $mensaje_consulta=$request->mensaje;



       //$data = array('email'=>$request->email, 'mensaje'=>$request->mensaje);
      Mail::send('users.customers.email-usuario',$request->all(),function ($msj) use ($correo_user){
        $msj->subject('Solicitud de contacto - Peruprop');
        $msj->to($correo_user);
      });

      Mail::send('users.customers.email-propietario',$request->all(),function ($msj) use ($correo_propietario){
        $msj->subject('Notificacion - Peruprop');
        $msj->to($correo_propietario);
      });

      $anunciante= DB::table('anunciantes')
      ->where('correo_anunciante',$correo_propietario)
      ->select('*')->limit(1)->get();
       $user_contactado_id=$anunciante[0]->user_id;
      $contacto= new Contactado_Usuario;
      $contacto->user_id=Auth::user()->id;
      $contacto->propiedad_id=$request->propiedad_id;
      $contacto->user_contactado_id=$user_contactado_id;
      $contacto->mensaje=$mensaje_consulta;
      $contacto->fecha_contactado=Carbon::now();
      $contacto->save();

      return back()->with('status','Su mensaje se envió con éxito. Gracias.');
    }





    public function obtenerProvincias(Request $request)
    {
    $provincias= DB::table('peru_provincias')->where('peru_departamento_id',$request->id)
      ->select('*')
      ->get();
      $data=compact(['provincias']);

      return $data;
    }
    public function obtenerDistrictos(Request $request)
    {
    $districtos= DB::table('peru_districtos')->where('peru_departamento_id',$request->id)
      ->select('*')
      ->get();
      $data=compact(['districtos']);

      return $data;
    }

    public function paquetesDetalles($tipo)
    {
      $paquetes= DB::table('paquetes')
      ->where('tipo_publicante',$tipo)
      ->orWhere('tipo_publicante',NULL)
      ->select('*')
      ->get();

      $data=compact(['paquetes']);

      if ($tipo=="inmobiliaria") {
          return view('users.customers.inmobiliaria.precios-planes',$data);
        }else{
          //return view('price')->with('tipo',$tipo);
          return view('users.customers.anunciante.precios-planes',$data); 
        }


      
    } 


    public function agregarQuitarFavorito(Request $request)
    {
      $user_id=Auth::user()->id;
      $buscarFavorito=DB::table('favorito__usuarios')->where('user_id',$user_id)->where('propiedad_id',$request->propiedad_id)->get();

      if (count($buscarFavorito)==0) {
      $favorito= new Favorito_Usuario;
      $favorito->user_id=$user_id;
      $favorito->propiedad_id=$request->propiedad_id;
      $favorito->fecha_favorito=Carbon::now();
      $favorito->save();
      return "add";
      }else{
       DB::table('favorito__usuarios')->where('propiedad_id',$request->propiedad_id)->delete();
      return "removed";
      }
    }



    public function mostrarEstados($estados)
    {
    $user_id=Auth::user()->id;  
    $publicaciones= Propiedad::obtenerpropiedades()->where('user_id',$user_id)
      ->select('*')
      ->simplePaginate(6); 

    $historial=Propiedad::obtenerpropiedades()->obtenerhistorial($user_id)->simplePaginate(4);
    $favoritos=Propiedad::obtenerpropiedades()->obtenerfavoritos($user_id)->simplePaginate(4);;

    $data=compact(['publicaciones','favoritos','historial']);

      return view('users.customers.estados',$data);
    }


    public function indexDatosPersonales()
    {
      $resultadosUser=User::where('id',Auth::user()->id)->get();

      //return $resultadosUser;
      //return $url = Storage::url('perfiles_user/yZ2C7FPjNDNf7S87onUxh8fTSJMEbsED8CbO3y4u.png');
      $data=compact(['resultadosUser']);
      return view('users.customers.datos-personales',$data);
    }
    public function ActualizarDatosPersonales(Request $request)
    {
      
           $user= User:: find(Auth::user()->id);
            
            if ($request->hasFile('avatar')) {

            $file_path = public_path().$user->avatar; 
            if(File::exists($file_path)) File::delete($file_path);            
            $name=Str::random(20);  
            $filename=$name.'.'.$request->avatar->extension();
            $filenameDB='/img/avatars/'.$filename;
            $path = $request->file('avatar')->move(public_path().'/img/avatars/',$filename);
            
            }
           $user->first_name=$request->first_name; 
           $user->last_name=$request->last_name;
           $user->document = $request->document;
           $user->nacionalidad_user = $request->nacionalidad_user;
           $user->cellphone = $request->cellphone;
           $user->telephone = $request->telephone;
           $user->pais_user=$request->pais_user;
           $user->provincia_user=$request->provincia_user;
           $user->sexo_user=$request->sexo_user;
           $user->address_user=$request->address_user;
           $user->birth_date_user=$request->birth_date_user;
           $user->avatar=$filenameDB;
           $user->update();

           return back()->with('mensaje','Datos personales actualizados.');
    }

    public function eliminarDelHistorial(Request $request)
    {
      $user_id=Auth::user()->id;
      DB::table('visto__usuarios')->where('user_id',$user_id)->where('propiedad_id',$request->propiedad_id)->delete();
      return "removed";
    }


}


