<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Propiedad_Servicio extends Model
{
    public function propiedad()
    {
    	return $this->belongsTo(Propiedad::class);
    }
}
