<?php
 
namespace App;

use Illuminate\Database\Eloquent\Model;


use Carbon\Carbon; 

class Propiedad extends Model
{
    //

   /* public function caracteristicaa()
    {
        return $this->belongsTo(Caracteristica::class);
    }*/

    public function caracteristica()
    {
    	//return $this->hasOne('App\Caracteristica', 'foreign_key', 'propiedad_id');
        return $this->hasOne(Caracteristica::class);
    }

    public function servicio()
    {
    	//return $this->hasMany('App\Propiedad_Servicio', 'foreign_key', 'propiedad_id');
        return $this->hasMany(Propiedad_Servicio::class);
    }

    public function instalacion()
    {
        return $this->hasMany(Propiedad_Instalacion::class);
    	//return $this->hasMany('App\Propiedad_Instalacion', 'foreign_key', 'propiedad_id');
    }
    public function ambiente()
    {
        return $this->hasMany(Propiedad_Ambiente::class);
    	//return $this->hasMany('App\Propiedad_Ambiente', 'foreign_key', 'propiedad_id');
    }
    public function fotos()
    {
    	//return $this->hasMany('App\Foto', 'foreign_key', 'propiedad_id');
        return $this->hasMany(Foto::class);
    }

    public function favoritos()
    {
        //return $this->hasMany('App\Foto');
        return $this->hasOne('App\Favorito_Usuario', 'foreign_key', 'propiedad_id'); 
    }

    public function scopeObtener($query)
    {
         return $query->join( 'caracteristicas','caracteristicas.propiedad_id','=', 'propiedads.id')
         ->where('codigo_publicacion', '<>',  NULL)
         ->where('fecha_baja', '>', Carbon::now())
         ->orderBy('prioridad_visibilidad','asc');
    }
    public function scopeTipo($query,$tipo)
    {
        if ($tipo) {
         return $query->where('tipo_propiedad',$tipo);
        }
    }

    public function scopeOperacion($query,$operacion)
    {
        if ($operacion) {
         return $query->where('tipo_operacion',$operacion);
        }
    }
    public function scopeUbicacion($query,$departamento)
    {
        if ($departamento) {
         return $query->where('ubicacion_completa', 'like',  $departamento.'%');
        }
    }
    public function scopeDireccion($query,$direccion)
    {
        if ($direccion) {
         return $query->where('direccion', 'like',  $direccion.'%');
        }
    }

    public function scopeBanios($query,$cantidad)
    {
        if ($cantidad) {
         return $query->where('cant_baños', '>',  $cantidad);
        }
    }
    public function scopeDormitorio($query,$cantidad)
    {
        if ($cantidad) {
         return $query->where('cant_dormitorios', '>',  $cantidad);
        }
    }
    public function scopePrecio($query,$precio_desde,$precio_hasta)
    {
        if ($precio_desde==null || $precio_hasta==null) {
            # code...
        }else{
        return $query->whereBetween('precio', array($precio_desde, $precio_hasta));
    }
    }

        public function scopeDuenio($query,$duenio)
    {
        if ($duenio) {
         return $query->where('tipo_propietario',  $duenio);
        }
    }

    public function scopeAntiguedad($query,$antiguedad)
    {
        if ($antiguedad) {

            if ($antiguedad=="20mas") {
                return $query->where('antiguedad', '>=',  20);
            }else{
                return $query->where('antiguedad', '<=',  $antiguedad);
            }
         
        }
    }

    public function scopeCochera($query,$cochera)
    {
        if ($cochera) {
         return $query->where('cant_chocheras', '>=',  $cochera);
        }
    }

  
    public function scopePreciorden($query,$preciorden)
    {
        if ($preciorden) {
        if ($preciorden=="mayor-precio") {
         return $query->orderBy('precio','desc');
        }elseif ($preciorden=="menor-precio") {
           return $query->orderBy('precio','asc');
        }
        }



    }
    public function scopeInmobiliaria($query,$usuario)
    {
        if ($usuario) {
         return $query->where('user_id', $usuario)
                        ->where('codigo_publicacion', '<>',  NULL)
                        ->where('tipo_propietario', '=', 'Inmobiliaria');
        }
    }

    public function scopeObtenerPropiedades($query)
    {
         return $query->join( 'caracteristicas','caracteristicas.propiedad_id','=', 'propiedads.id');
    }

    public function scopeObtenerFavoritos($query,$user_id)
    {
         return $query->join( 'favorito__usuarios','favorito__usuarios.propiedad_id','=', 'propiedads.id')->where('favorito__usuarios.user_id','=',$user_id);
    }
    public function scopeObtenerHistorial($query,$user_id)
    {
         return $query->join( 'visto__usuarios','visto__usuarios.propiedad_id','=', 'propiedads.id')->where('visto__usuarios.user_id','=',$user_id);
    }
}

