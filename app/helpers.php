<?php 
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Visto_Usuario;
use Carbon\Carbon; 

function esFavorito($propiedad_id)//Este helpers agrega la clase activa en el boton favorito si el usuario lo agrego
{
	$user_id=Auth::user()->id;

	$buscarFavorito=DB::table('favorito__usuarios')->where('user_id',$user_id)->where('propiedad_id',$propiedad_id)->get();

      if (count($buscarFavorito)==0) {
      return "";
      }else{
      return "active";
      }
}



function agregarAVistos($propiedad_id)//Este helpers agrega una publicacion al historial del usuario
{
      $user_id=Auth::user()->id;
      $buscarHistorial=DB::table('visto__usuarios')->where('user_id',$user_id)->where('propiedad_id',$propiedad_id)->get();

      if (count($buscarHistorial)==0) {
      $visto= new Visto_Usuario;
      $visto->user_id=$user_id;
      $visto->propiedad_id=$propiedad_id;
      //$visto->fecha_visto=Carbon::now();
      $visto->save();
      }else{

      }
}

function determinarRol()//Este helpers termina el rol del user
{
	$user_id=Auth::user()->id;
	$role=DB::table('users')->join('roles','roles.id','=','users.role_id')->where('users.id',$user_id)->get();
	return $role[0]->name_rol;
}

function mostrarOpcionReducida() //Este helpers define las rutas a las cuales sun las url a la cual se ocultar las opciones del pan
{
switch (request()->path()) {
      case 'panel/estados':
            return false;
            break;
      case 'panel/favoritos':
            return false;
            break;
      case 'panel/vistos':
            return false;
            break;
      case 'panel/contactados':
            return false;
            break;
      
      default:
            return true;
            break;
}

}


function mostrarBusqueda()//Este helpers retrona una array para dibujar el boton de busqueda de acuerdo al propietario
{
      if (Auth::user()->role_id==3) {
            return $arrayName = array('url' => 'listado-inmobiliarias','title'=>'Buscar Inmobiliarias');
      }
      $tipo_propietario=Auth::user()->tipo_propietario; // preguntar por el role antes de hacer el swicth
      switch ($tipo_propietario) {
            case '':
                  return $arrayName = array('url' => 'listado-agentes','title'=>'Buscar Agentes');
                  break;
            case 'Agente':
                  return $arrayName = array('url' => 'listado-inmobiliarias','title'=>'Buscar Inmobiliarias');
                  break;
            case 'Inmobiliaria':
                  return $arrayName = array('url' => 'listado-agentes','title'=>'Buscar Agentes');
                  break;
            case 'Particular':
                  return $arrayName = array('url' => 'listado-agentes','title'=>'Buscar Agentes');
                  break;
      }
} 




