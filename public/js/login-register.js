$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("#login").submit(function(e) {
    e.preventDefault();
    $("#Sesion").attr('disabled',true);
    $("#Sesion").html(`<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>`);

  $.ajax({
    url: this.action,
    data:{email:$("#emailUser").val(),password:$("#passUser").val()},
    type: "POST",
    success: function(response){

      window.location.replace(response);

   /*   if (response==1) {
        //window.location.replace(redirect);
        console.log(response)
      }else{
        console.log(response)
        //location.reload();
      }*/
      },
       error : function(data) {
         if( data.status === 422 ) {
          $("#Sesion").attr('disabled',false);
          $("#Sesion").html(`Iniciar Sesión`);
            var errors = $.parseJSON(data.responseText);
          //  console.log(errors.message)
            if (errors.message=="The given data was invalid.") {
              $("#messageError").html('<small  class="form-text text-danger">Los datos proporcionados no son correctos.Verifique el email o la contraseña.</small>')
            }
          }
      }
    });

  });

   $("#register").submit(function(e) {
    e.preventDefault();
   $("#Registrar").attr('disabled',true);
    $("#Registrar").html(`<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>`);
    var role=2;
    var name=$("#registerName").val();
    var email=$("#registerEmail").val();
    var pass=$("#registerPassword").val();
    var pass_confirm=$("#registerPasswordConfirm").val();
    var agente=$("#agente").prop('checked');
    if (agente) {
      role=3;
    }
  $.ajax({
    url: this.action,
    data:{name:name,email:email,password:pass,password_confirmation:pass_confirm,role_id:role},
    type: "POST",
    success: function(response){

//console.log(response)
location.reload();
      },
       error : function(data) {

            for(var k in data.responseJSON.errors) {   
               if (k=='email') {
                $("#messageExistEmail").html('<small  class="form-text text-danger">El email ya ha sido registrado.</small>');
               }  

               if (k=='password') {
                $("#messageLongPass").html(data.responseJSON.errors[k][0]);
                if (data.responseJSON.errors[k][0]=='El campo confirmación de password no coincide.') {
                  $("#messageConfirmPass").html('<small  class="form-text text-danger">La confirmación de contraseña no coincide.</small>');
                }else{
                  $("#messageLongPass").html('<small  class="form-text text-danger">La contraseña debe tener al menos 8 caracteres.</small>');
                }
               }       
                }
            $("#Registrar").attr('disabled',false);    
            $("#Registrar").html(`Registrate`);
      }
    });

  }); 