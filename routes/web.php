<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
      $DepartamentosProvincias = DB::table('peru_departamentos')
            ->join('peru_provincias', 'peru_departamento_id', '=', 'peru_departamentos.id')
            ->select('*')
            ->get();
      $data = compact(['DepartamentosProvincias']);  
    return view('welcome',$data);
});

Route::get('/publicaciones', function () {
    return view('publications');
});

Route::get('/status-error', function () {
    return view('users.customers.error-mp-pago');
});
/*
Route::get('refresh', function() {// refresh y seed
    Artisan::call('migrate:refresh',['--force'=>true,'--seed'=>true]);
});


Route::get('seed', function() {
    Artisan::call('migrate',['--force'=>true,'--seed'=>true]);
});

Route::get('rollback', function() {
    Artisan::call('migrate:rollback',['--force'=>true]);
});

Route::get('migrate', function() {
    Artisan::call('migrate',['--force'=>true]);
});
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('password/change/', 'HomeController@changePassword')->name('changePassword');
Route::post('password/update/', 'HomeController@updatePassword')->name('updatePassword');
Route::get('email/change/', 'HomeController@changeEmail')->name('changeEmail');
Route::post('email/update/', 'HomeController@updateEmail')->name('updateEmail');


/*
|--------------------------------------------------------------------------
| Web Routes Search
|--------------------------------------------------------------------------
|
|
*/

Route::post('busqueda', 'BusquedaController@filtrar')->name('search-post');//Ruta auxiliar busqueda
//Route::get('busqueda-{tipo_propiedad?}-{filtroIngresado?}-{valorIngresado?}', 'BusquedaController@busquedaPropiedades')->name('busquedaPropiedades');
Route::get('busquedas-{operation?}-{location?}', 'BusquedaController@busquedaEspecifica')->name('busquedaEspecifica');
Route::get('search','BusquedaController@search')->name('search');


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
|
*/

Route::get('admin', 'AdministradorController@index')->name('dashboard');
Route::get('admin/requests', 'AdministradorController@requestProperties')->name('requests');
Route::get('admin/properties', 'AdministradorController@properties')->name('properties');
//Route::get('admin/advertisers', 'AdministradorController@advertisers')->name('advertisers');
Route::get('admin/advertisers/{advertisers?}', 'AdministradorController@advertisers')->name('advertisers');
//Route::get('admin/real-states', 'AdministradorController@realState')->name('real-estate');
Route::get('admin/publications', 'AdministradorController@publications')->name('publications');

Route::post('admin/accept', 'AdministradorController@acceptPublicactions')->name('publications-accept');
Route::post('admin/refused', 'AdministradorController@refusedPublications')->name('publications-refused');
Route::post('admin/delete', 'AdministradorController@deletePublications')->name('publications-delete');
Route::post('admin/delete-publication', 'AdministradorController@deletePublication')->name('publication-delete');


Route::post('admin/properties/newservice', 'AdministradorController@registerService')->name('services');
Route::post('admin/properties/newinstall', 'AdministradorController@registerInstalation')->name('instalations');
Route::post('admin/properties/newambience', 'AdministradorController@registerAmbience')->name('ambiences');

/*
|--------------------------------------------------------------------------
| Web Routes User
|--------------------------------------------------------------------------
|
| 
*/ 


Route::get('/publicaciones/precios/{tipoPropietario}', 'UsuarioController@paquetesDetalles')->name('paquetes');

Route::group(['middleware'=>'auth'], function () {
Route::GET('publicaciones/aviso', 'UsuarioController@nuevaPropiedad')->name('aviso');
Route::POST('publicaciones/aviso/solicitar', 'UsuarioController@solicitarPropiedad')->name('solicitarPublicacion');
Route::POST('/publicaciones/campos-dinamicos', 'UsuarioController@obtenerCampos')->name('camposEspecificos');

Route::POST('/publicaciones/campos-dinamicos', 'UsuarioController@obtenerCampos')->name('camposEspecificos');
Route::POST('publicaciones/aviso/ambientes', 'UsuarioController@agregarAmbiente')->name('agregarAmbiente');
Route::POST('publicaciones/aviso/servicios', 'UsuarioController@agregarServicio')->name('agregarServicio');
Route::POST('publicaciones/aviso/instalaciones', 'UsuarioController@agregarInstalacion')->name('agregarInstalacion');

Route::get('panel/{estados?}', 'UsuarioController@mostrarEstados')->name('estados');

Route::POST('publicaciones/mail', 'UsuarioController@enviarMail')->name('enviarMail'); 

Route::POST('publicaciones/provincias', 'UsuarioController@obtenerProvincias')->name('provincias');
Route::POST('publicaciones/districtos', 'UsuarioController@obtenerDistrictos')->name('districtos');


Route::POST('favoritos/', 'UsuarioController@agregarQuitarFavorito')->name('favoritos');

Route::get('changeInformation', 'UsuarioController@indexDatosPersonales')->name('changeInformation');

Route::post('datos-personales', 'UsuarioController@ActualizarDatosPersonales')->name('actualizarDatosPersonales');

Route::post('remover-historial-propiedad', 'UsuarioController@eliminarDelHistorial')->name('eliminarDelHistorial');

});

/*
|--------------------------------------------------------------------------
| Web Routes Agents
|--------------------------------------------------------------------------
|
*/
Route::get('listado-inmobiliarias', 'AgenteController@listadoInmobiliarias')->name('inmobiliariaResultados')->middleware('auth'); 

/*
|--------------------------------------------------------------------------
| Web Routes Real-State
|--------------------------------------------------------------------------
|
*/
Route::get('/publicaciones/registro/inmobiliaria/{modalidad}', 'InmobiliariaController@formulario')->name('indexI');
Route::POST('/registrarInmobiliaria', 'InmobiliariaController@registrar')->name('registrarInmobiliaria')->middleware('auth'); 
//Route::get('inmobiliaria-{user_id}', 'InmobiliariaController@obtenerPropiedades')->name('obtenerPropiedadesInmobiliaria'); 
Route::get('listado-agentes', 'InmobiliariaController@listadoAgentes')->name('agentesResultados'); 


/*
|--------------------------------------------------------------------------
| Web Routes Advertisers
|--------------------------------------------------------------------------
|
*/
Route::get('/publicaciones/registro/anunciante/{modalidad}', 'AnuncianteController@formulario')->name('indexA');
Route::POST('/registrarAnunciante', 'AnuncianteController@registrar')->name('registrarAnunciante');
Route::get('anunciante-{user_id}', 'AnuncianteController@obtenerPropiedades')->name('obtenerPropiedadesAnunciante')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Web Routes Pays
|--------------------------------------------------------------------------
|
*/

//Route::get('/home', 'UsuarioController@index')->name('detallesPago'); 

Route::get('/publicaciones/registro/pagos/{modalidad}', 'PagoController@generarPreferenciaMP')->name('preferenciaMercadoPago') ;
Route::get('/publicaciones/registro/pagos-debito', 'PagoController@generarPreferenciaMPSub')->name('preferenciaMercadoPagoSub');
Route::get('/publicaciones/registro/guardar-pago', 'PagoController@guardarPreferenciaMP')->name('guardarMercadoPagado')->middleware('auth');     



/*
|--------------------------------------------------------------------------
| Web Routes Prices
|--------------------------------------------------------------------------
|
* 
Route::get('/publicaciones/registro/pagos', 'PagoController@generarPreferenciaMP')->name('preferenciaMercadoPago');
Route::get('/publicaciones/registro/pagos-debito', 'PagoController@generarPreferenciaMPSub')->name('preferenciaMercadoPagoSub');
Route::get('/publicaciones/registro/guardar-pago', 'PagoController@guardarPreferenciaMP')->name('guardarMercadoPagado');    */